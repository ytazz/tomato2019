roslaunch my_dynamixel_tutorial controller_manager.launch
 roslaunch my_dynamixel_tutorial start_tilt_controller.launch
 ~/catkin_ws/src/tomato2019/scripts/dynamixel_init.sh

roslaunch camera_ui remote.launch
roslaunch camera_ui pointcloud_remote.launch

roslaunch kobuki_node minimal.launch
