#!/bin/sh

# send zero-position to each motor
rostopic pub -1 /tilt0_controller/command std_msgs/Float64 "data: 0.0"
rostopic pub -1 /tilt1_controller/command std_msgs/Float64 "data: 0.0"
rostopic pub -1 /tilt2_controller/command std_msgs/Float64 "data: 0.0"
rostopic pub -1 /tilt3_controller/command std_msgs/Float64 "data: 0.0"
rostopic pub -1 /tilt4_controller/command std_msgs/Float64 "data: 0.0"
rostopic pub -1 /tilt5_controller/command std_msgs/Float64 "data: 0.0"
rostopic pub -1 /tilt6_controller/command std_msgs/Float64 "data: 0.0"
rostopic pub -1 /tilt7_controller/command std_msgs/Float64 "data: 0.0"