#!/bin/sh

# update of apt
yes | sudo apt-get update
yes | sudo apt-get upgrade

# dynamixel
yes | sudo apt-get install ros-kinetic-dynamixel-motor

# turtlebot
yes | sudo apt-get install ros-kinetic-turtlebot
yes | sudo apt-get install ros-kinetic-turtlebot-apps

# urdf/xacro model visualizing package
yes | sudo apt-get install ros-kinetic-urdf-tutorial

# rviz visualization plugin created by jsk
yes | sudo apt-get install ros-kinetic-jsk-visualization

# ROS Abotix simulator
# yes | sudo apt-get install ros-kinetic-arbotix-*

# Qt
yes | sudo apt-get install \
  qt5-default \
  libqt5x11extras5-dev \
  qt5-style-plugins

# mocap setting
yes | sudo ufw allow 1510
yes | sudo ufw allow 1511
yes | sudo ufw reload

# camera
yes | sudo apt install ros-kinetic-openni2-launch
yes | sudo apt install ros-kinetic-perception-pcl
yes | sudo apt install ros-kinetic-pcl-ros
yes | sudo apt install ros-kinetic-uvc-camera 

# Eigen library
yes | sudo apt-get install libeigen3-dev
