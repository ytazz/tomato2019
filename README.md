# About this repository

トマトチャレンジ2019 Kチームのソースコード管理用プロジェクトです

# Rule

みんなでコーディングするためのルール

## ディレクトリ・ファイル構成

### ディレクトリ

**フォルダ名およびファイル名は英小文字** とし，
長くなる場合はアンダースコアでつなぐこと  

**各パッケージのルートディレクトリにはreadmeを作成** し，
そこにプログラムの説明を書く．  
readmeの拡張子は基本的に.mdとするが，.txtも可．  
Atom使っている同士は，
ctrl+shift+mでmarkdownプレビュー画面が見れます．

パッケージの作成コマンド

    $ catkin_create_pkg <package_name> [depend1] [depend2] [depend3]  

例えば,

    $ catkin_create_pkg my_package dynamixel_msgs roscpp rospy sensor_msgs std_msgs  

あるパッケージ１つのみをビルドしたい場合は，以下のコマンド．

    $ catkin_make --pkg [package_name]  

ディレクトリの構成は以下の通り．
変更あり．  

>tomato2019/  
>　├ doc/  
>　│　├ figure/  
>　│　│　├ \*.svg  
>　│　│　└ \*.eps  
>　│　├ \*.tex  
>　│　└ main.pdf  
>　├ scripts/  
>　│　└ install_package.sh  
>　├ src/  
>　│　├ sample/  
>　│　│　└ [package_name]/  
>　│　└ [package_name]/  
>　│　　　├ include/  
>　│　　　│　└ \*.h  
>　│　　　├ msg/  
>　│　　　│　└ \*.msg  
>　│　　　├ scripts/  
>　│　　　│　└ \*.py  
>　│　　　├ src/  
>　│　　　│　└ \*.cpp  
>　│　　　├ urdf/  
>　│　　　│　├ \*.urdf  
>　│　　　│　└ \*.xacro  
>　│　　　├ launch/  
>　│　　　├ CMakeLists.txt  
>　│　　　├ package.xml  
>　│　　　└ README.md  
>　├ .gitignore  
>　└ README.md  

apt-get等でインストールしたパッケージを使用する場合は，
他の人がビルドできなくなる可能性があるので，
`tomato2019/scripts/install_package.sh`に追記していってください．  
例)
```sh
#!/bin/sh
sudo apt-get install ros-kinetic-dynamixel-motor
```

**参考にしたい昨年以前のコードについてはsample以下に置くこと．**

**プログラムの詳細説明は各パッケージのreadmeに，
全体で共有したいgitや戦略等の説明はdoc以下のtexファイルに** お願いします．
docディレクトリは，main.pdf以外のpdfはgit管理から外しています．
texファイルを編集した最後にはmain.texをコンパイルしてmain.pdfを作成しておいてください．  
**texで作られる中間ファイルはpushしないこと**  
Ubuntuでtexファイルをコンパイルする際は以下のコマンド．  

    $ platex main.tex  
    $ dvipdfmx main.dvi


### ファイル
**ファイルの頭にそのファイルを書いた著者を記入** してください．
```cpp
/*
  Author: Hiroki Kuribayashi
*/
```

## コーディングの際の注意

クラスや関数の付近にはコメントを入れてください．
ヘッダとソースを分けているときはヘッダにのみコメントで大丈夫です．  
コメントは日本語でいいと思います．もちろん英語も可．
日本語の場合の **文字エンコーディングはUTF-8で統一** しましょう．

ハードコーディングはやめましょう．
.txtや.xmlやrosparamなどでパラメータとしてもたせること．  
各種パラメータを総括して保持するノードを作成予定．

# Others

## RViz

Rvizの設定ファイルを自分で作成するためには，  
Rvizを立ち上げて諸々設定した後に，メニューバーのFileからSave Configする．  

その後launchファイルのrvizノードのargs属性に保存した\*.rvizファイルを読み込むようにする．  