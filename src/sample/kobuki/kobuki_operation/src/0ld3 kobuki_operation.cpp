#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>
#include <kobuki_msgs/BumperEvent.h>

// int attack;
// double time = 0;
float x,z;
float old_x = 0.0, old_z = 0.0;

// void bumperCallback(const kobuki_msgs::BumperEventConstPtr &bumper)
// {
//   attack = bumper->state;
// }
//
// void timerCallback(const ros::TimerEvent&)
// {
//   time = time + 0.025;
// }

void joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  x = joy->axes[1];
  z = joy->axes[0];
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "kobuki_operation");
  ros::NodeHandle n;

  ros::Subscriber joy_sub = n.subscribe<sensor_msgs::Joy>("joy", 10, &joyCallback);
  // ros::Subscriber bumper_sub = n.subscribe<kobuki_msgs::BumperEvent>("/mobile_base/events/bumper", 1, &KobukiOperation::bumperCallback);

  ros::Publisher kobuki_pub = n.advertise<geometry_msgs::Twist>("/mobile_base/commands/velocity", 1);

  // ros::Timer timer = n.createTimer(ros::Duration(0.025), &KobukiOperation::timerCallback);

  geometry_msgs::Twist twist;

  float a_scale = 1.2;
  float l_scale = 0.15;

  while(ros::ok()){
    //if(-0.1 <= x - old_x <= 0.1)
    //{
      twist.linear.x = l_scale * x;
      old_x = x;
    //}

    //else if(x - old_x > 0.1)
    //{
      //twist.linear.x = l_scale * old_x;
      //old_x = old_x + 0.01;
    //}

    //else if(x - old_x < -0.1)
    //{
      //twist.linear.x = l_scale * old_x;
      //old_x = old_x - 0.01;
    //}

    //if(-0.1 <= z - old_z <= 0.1)
    //{
      twist.angular.z = a_scale * z;
      old_z = z;
    //}

    //else if(z - old_z > 0.1)
    //{
      //twist.angular.z = a_scale * old_z;
      //old_z = old_z + 0.01;
    //}

    //else if(z - old_z < -0.1)
    //{
      //twist.angular.z = a_scale * old_z;
      //old_z = old_z - 0.01;
    //}

    kobuki_pub.publish(twist);
    ROS_INFO("%f, %f", twist.linear.x, twist.angular.z);

    ros::spinOnce();
  }

  ros::spin();
}
