#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>
#include <kobuki_msgs/BumperEvent.h>

//int attack;
//double time = 0;
float x,z;

/*
void bumperCallback(const kobuki_msgs::BumperEventConstPtr &bumper)
{
  attack = bumper->state;
}

void timerCallback(const ros::TimerEvent&)
{
  time = time + 0.025;
}
*/

void joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  x = joy->axes[1];
  z = joy->axes[0];
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "kobuki_operation");
  ros::NodeHandle n;

  ros::Subscriber joy_sub = n.subscribe<sensor_msgs::Joy>("joy", 10, &joyCallback);
  //ros::Subscriber bumper_sub = n.subscribe<kobuki_msgs::BumperEvent>("/mobile_base/events/bumper", 1, &KobukiOperation::bumperCallback);

  ros::Publisher kobuki_pub = n.advertise<geometry_msgs::Twist>("/mobile_base/commands/velocity", 1);

  //ros::Timer timer = n.createTimer(ros::Duration(0.025), &KobukiOperation::timerCallback);

  geometry_msgs::Twist twist;

  float a_scale = 1.0;
  float l_scale = -0.15;

  while(ros::ok()){
    twist.linear.x = l_scale * x;
    twist.angular.z = a_scale * z;

    kobuki_pub.publish(twist);

    ros::spinOnce();
  }

  ros::spin();
}
