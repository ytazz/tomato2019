/*橋本俊治、よしのり、つーじー
*/

#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>
#include <kobuki_msgs/BumperEvent.h>

// int attack;
// double time = 0;
float x, z;
int temp, old_temp = 0, num = 0;
float target_linear, present_linear,target_angular,present_angular;

// void bumperCallback(const kobuki_msgs::BumperEventConstPtr &bumper)
// {
//   attack = bumper->state;
// }
//
// void timerCallback(const ros::TimerEvent&)
// {
//   time = time + 0.025;
// }

void joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  x = joy->axes[1];
  z = joy->axes[0];
  temp = joy->buttons[0];
}

void autokobuki(float& pl ,float& tl, geometry_msgs::Twist& tw)
{
  if(tl - pl > 0.001)
  {
    tw.linear.x = 0.1;
    tw.angular.z= 0.2;
    pl = pl + 0.05 * 0.0005;
  }
  else if(tl - pl <= 0.001)
  {
    tw.linear.x = 0.0;
    tw.angular.z = 0.0;
    pl = tl;
  }
}



int main(int argc, char** argv)
{
  ros::init(argc, argv, "kobuki_operation");
  ros::NodeHandle n;

  ros::Subscriber joy_sub = n.subscribe<sensor_msgs::Joy>("joy", 10, &joyCallback);
  // ros::Subscriber bumper_sub = n.subscribe<kobuki_msgs::BumperEvent>("/mobile_base/events/bumper", 1, &KobukiOperation::bumperCallback);

  ros::Publisher kobuki_pub = n.advertise<geometry_msgs::Twist>("/mobile_base/commands/velocity", 1);

  // ros::Timer timer = n.createTimer(ros::Duration(0.025), &KobukiOperation::timerCallback);

  geometry_msgs::Twist twist;

  float a_scale = 0.01;
  float l_scale = 0.15;

  while(ros::ok()){
    if(temp == 1 && old_temp == 0) num++;

    old_temp = temp;

    if(num % 2 == 0)
    {
      twist.linear.x = l_scale * x;
      twist.angular.z = a_scale * z;

      target_linear = 10.0, present_linear = 0.0;
      target_angular = 100.0, present_angular =0.0;
    }

    else
    {
      if(present_linear != target_linear){
        autokobuki(present_linear, target_linear ,twist);
      }

      else{
        if(target_angular - present_angular > 0.001)
        {
          twist.angular.z = 0.2;
          present_angular = present_angular + 0.6 * 0.0005;
        }

        else if(target_angular - present_angular <= 0.001)
        {
          twist.angular.z = 0.0;
          present_angular = target_angular;
        }
      }

      //twist.angular.z=0.0;
    }

    kobuki_pub.publish(twist);
    ROS_INFO("%f, %f, %f, %f", present_linear, target_linear, present_angular, target_angular);

    ros::spinOnce();
  }

  ros::spin();
}
