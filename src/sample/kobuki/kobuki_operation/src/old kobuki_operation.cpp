﻿// ===========================================
// kobukiをjoystickで操作するためのサンプルプログラム
// ===========================================

/* ヘッダファイルの読み込み */
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Twist.h>
#include <kobuki_msgs/BumperEvent.h>

/* クラス定義 */
class KobukiOperation
{
public:
    // 関数はこっち
    KobukiOperation(); // コンストラクタ(実行すると最初に呼ばれ、変数を初期化する役割)
    void joyCallback(const sensor_msgs::Joy::ConstPtr &joy); // joystickの信号を受け取る
    void kobukiCallback(const sensor_msgs::Imu); // kobukiの信号を受け取る
    void timerCallback(const ros::TimerEvent&);
    void bumperCallback(const kobuki_msgs::BumperEventConstPtr &bumper);
private:
    //変数はこっち
    ros::NodeHandle nh; // これは絶対いいる。おまじない。
    ros::Timer      timer;
    ros::Subscriber kobuki_sub, joy_sub, bumper_sub; // subscriberの変数宣言
    ros::Publisher  kobuki_pub; // publisherの変数宣言
    double          scaleLinear, scaleAngular; // 使いたい変数宣言(C言語と一緒の形式)
    double time, step_time,b;
    int mode ,attack;
};

/* クラス内の関数 */
KobukiOperation::KobukiOperation()
{

    // subscriber
    // kobuki_sub = nh.subscribe <sensor_msgs::Imu>("トピック名わすれた", 10, &KobukiOperation::kobukiCallback, this);　//kobukiからのセンサ情報
    joy_sub = nh.subscribe <sensor_msgs::Joy>("joy", 1, &KobukiOperation::joyCallback, this);
    bumper_sub       = nh.subscribe <kobuki_msgs::BumperEvent>("/mobile_base/events/bumper", 1, &KobukiOperation::bumperCallback, this);
    // publisher

    kobuki_pub = nh.advertise <geometry_msgs::Twist>("/mobile_base/commands/velocity", 1); //Kobuki実機
    // turtle_pub = nh.advertise <geometry_msgs::Twist>("/turtle1/cmd_vel", 1); // turtlesimで操作確認する時用
    timer            = nh.createTimer(ros::Duration(0.025), &KobukiOperation::timerCallback, this);

    // 変数の初期化
    scaleLinear  = 0.03;  //0.025   操作出来てる感　0.03
    scaleAngular = 0.3;   //0.26                 0.3
    mode=0;
    time =0;
    b=0;
    attack=0;
}


void KobukiOperation::bumperCallback(const kobuki_msgs::BumperEventConstPtr &bumper)
{

    attack = bumper->state;
}

void KobukiOperation::timerCallback(const ros::TimerEvent&) // 起動してからの時間のパラメータを生成
{
  //   geometry_msgs::Twist twist;
  // if(time <3.0){
  //   twist.linear.x  =3.141592/12 ;
  //   twist.angular.z = 3.141592/6;
  //   b=0.025*3.141592/12+b;
  //   std::cout<<b*180/3.141592<<std::endl;
  //   kobuki_pub.publish(twist);
  //   std::cout<<time<<std::endl;
  // }

    time = time + 0.025;
}

/* ジョイスティックの信号を受け取ったら呼ばれる関数 */
void KobukiOperation::joyCallback(const sensor_msgs::Joy::ConstPtr &joy)
{
    //変数の宣言(int a; とかと一緒)
    geometry_msgs::Twist twist;

    // joyの信号を使いたいときはこんな感じ
    switch (mode) {
      case 0:
      ROS_INFO_STREAM("Teleop");
      if(joy->buttons[8] == 1) {
          ROS_INFO_STREAM("mode Change");
          mode=1;
      }

      else if(attack==1){
        mode=2;
          }
      else {
        twist.linear.x  = scaleLinear * (joy->buttons[0] -joy->buttons[2]);
        twist.angular.z = scaleAngular *(joy->buttons[1] -joy->buttons[3]);
        kobuki_pub.publish(twist);
        }

      break;

      case 1:
      ROS_INFO_STREAM("AUTO");
      if(joy->buttons[8] == 1) {
          ROS_INFO_STREAM("mode Change");
          mode=0;
      }
      else if(attack==1){
        mode=2;
          }
      else {
        twist.linear.x  = 0.15 * (joy->buttons[0] -joy->buttons[2]);
        twist.angular.z = 0.15 *(joy->buttons[0] -joy->buttons[2]);
        kobuki_pub.publish(twist);
        }
        break;


    // joyの信号を使わないときはこんな感じ
    // twist.linear.x  = scaleLinear;
    // twist.angular.z = scaleAngular;

    case 2:
    // ROS_INFO_STREAM("90");
    //
    // if(time <6){
    //   // twist.linear.x  =3.141592/12 ;
    //   twist.angular.z = 3.141592/12;
    //   b=b+1;
    //   std::cout<<b<<std::endl;
    //     kobuki_pub.publish(twist);
    // }

    // else {
    //   mode=0;
    // }
        geometry_msgs::Twist twist;
        ROS_INFO_STREAM("Attack");

          twist.linear.x  = -0.05;
          twist.angular.z = 0;
          kobuki_pub.publish(twist);
          if(joy->buttons[8] == 1) {
              mode=0;
        }
    break;
}

}

/* main関数(そのままでよい) */
int main(int argc, char **argv)
{
    ros::init(argc, argv, "kobuki_operation");
    KobukiOperation kobuki_operation;
    ros::spin();
}
