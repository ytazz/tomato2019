<!-- Author: S. Hashimoto -->

# kobukiをキーボードで動かす（橋本俊治ver)
Author: S. Hashimoto

## 0. ワークスペースの準備
他と同じ

## 1. TurtleBot用のパッケージをインストール

    $ sudo apt-get install ros-kinetic-turtlebot
    $ sudo apt-get install ros-kinetic-turtlebot-apps

ただし，

    $ sudo apt-get install ros-indigo-turtlebot-viz

のkineticはなかったしおそらくやる必要もない

## 2. kobuki用パッケージをインストール
https://www.rt-shop.jp/blog/archives/368 参照  
このサイトの"Kobuki用パッケージの入手"の通りにすすめる

$ git clone https://github.com/yujinrobot/kobuki.gitの部分で注意  
福田さんのでは

    $ git clone -b indigo-devel https://github.com/yujinrobot/kobuki.git

とあるがindigoをkineticに変えてkobukiを入手することができない  
自分はhttps://github.com/yujinrobot/kobuki.gitをzipファイルで保存してcatkin_ws/srcに直接保存した  
なのでzipファイルで解凍するのもいいし

    $ git clone https://github.com/yujinrobot/kobuki.git

でもいいと思う

## 3. Kobukiのドライバのインストール
上記のサイト通りに行う

## 4． Kobukiの動作
    $roslaunch turtlebot_bringup minimal.launch
    $roslaunch turtlebot_teleop keyboard_teleop.launch

でkobuki が動くはず

## 5. gazebo
https://r17u.hatenablog.com/entry/2017/06/11/213942参照  
サイト通りにやっていく  
注意する点はgazeboの一番最初起動させるのはちょっと時間がかかるため気長に待つこと


<--ここでキーボードにuやiを入力しても動かねええってなって-->

https://demura.net/lecture/13054.htmlを参照  
次の部分に注目  
"さらに，動かない場合は，minimal.launchを起動している端末ごと終了させて，再度，新しい端末を開いて，minimal.launchを起動する．"
おそらくlaunchを起動するのに順番が必要らしい，，，
これで見事に動くはず！！！！！！！！！

    u;左旋回
    i;後ろ
    o;右旋回
    j;その場で左旋回
    k;その場で右旋回
    l
    m
    ,前進


## 6. kinectを使って映像表示
freenectをインストール

    $ sudo apt-get install ros-kinetic-freenect-launch

kinectをpcにつなぐ

    $ roslaunch freenect_launch freenect.launch
    $ rosrun rviz rviz
    $ rosrun rqt_image_view rqt_image_view

image_viewで
カラー画像は/camera/rgb/image_color，距離画像は/camera/depth/imageを選択  
（参考：http://robot.isc.chubu.ac.jp/?p=560）

## 7. 複数台ロボット通信
“ROS講座20 複数のPCでROS接続1 – Qiita” 参照  
サイトのとおりに進めていく  
今回はマスタをkobukiに乗せるほう，スレーブを自分のPCに設定した．  

それで最後のROS分散実行のところで
Kobukiのノーパソがroscore,ros bringup(kobukiとノーパソをつなぐコマンド)  
自分のPCでros_keybordのコマンドを打つと動くはず．  
Exportはターミナルを立ち上げるごとにしないとだめなのですごくめんどくさい  
だからbashrcに

    export ROS_IP=192.168.2.10
    export ROS_MASTER_URI=http://192.168.2.10:11311
    export ROS_IP=192.168.2.20

を通す
IPアドレスはPCごとで変更する

new
ROSロボットプログラミングバイブル参照
p191 ”映像の遠隔送信”を見ながら同じことを行ったほうが正確

## 8. joystick
辻ひろきのlandiskフォルダにkobuki_teleop_joyがあるのでそれを自分のsrcの下にコピーする．  
CMakelistも変える必要がある  
Landiskのトマトチャレンジ2018のフォルダの  

    \\LANDISK4\Public\共用(Shared)\t_トマトチャレンジ\2018\Kteam\software\package\kobuki_operation
    の中のCMakelist

をコピーし，kobuki_operationの下にそれを置く
kobuki_operationは自分で作る
この中にkobuki_operation.cppもおく．つまり

    catkin_ws
      - kobuki_operation
        - CMakeLists.txt
        - src
          - kobuki_operation.cppをおく

これであとは
マスタ側のKobukiのPCで

    $ roscore
    $ bringupなんちゃら

自分のPCで

    $ roslaunch kobuki_operation kobuki_operation

## 9. kinectの映像をRVizに表示させる方法
ROSロボットプログラミングバイブル参照
p189