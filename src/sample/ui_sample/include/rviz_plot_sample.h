/*
    author: H. Kuribayashi

    RvizPlotのサンプルコード
*/

#ifndef __RVIZ_PLOT_SAMPLE_H__
#define __RVIZ_PLOT_SAMPLE_H__

// ヘッダファイルの読み込み
// C言語系
#include <cmath>
#include <map>
#include <vector> // 動的配列
// ROS系
#include <geometry_msgs/Point.h>
#include <ros/ros.h>
#include <visualization_msgs/Marker.h> // Rvizに点や線を描画するメッセージ型
#include <visualization_msgs/MarkerArray.h> // 配列版
// 自分で定義したメッセージ型
#include <tomato_msgs/RvArrow.h>  // 矢印描画用
#include <tomato_msgs/RvCircle.h> // 円形描画用
#include <tomato_msgs/RvLine.h>   // 線描画用
#include <tomato_msgs/RvPoint.h>  // 点描画用
#include <tomato_msgs/RvSquare.h> // 四角形描画用
#include <tomato_msgs/RvStl.h>    // stlモデル描画用

// 描画クラス
class RvizPlotSample {
public:
  RvizPlotSample();
  ~RvizPlotSample();

  // 描画アイテムをpublish
  void pubPoint();
  void pubLine();
  void pubCircle();
  void pubSquare();
  void pubArrow();
  void pubStl();

  // 一定時間おきに形状を変更しpublish
  void timerCallback(const ros::TimerEvent &);

private:
  // ROSシステムの変数
  ros::NodeHandle nh;
  ros::Timer timer;
  // 描画アイテムをpublish
  // 点・線・円・四角でそれぞれ１つずつ
  ros::Publisher pub_point, pub_line, pub_circle;
  ros::Publisher pub_square, pub_arrow, pub_stl;

  // 時間
  double dt;   // timestep
  double time; // 経過時間
};

#endif // __RVIZ_PLOT_SAMPLE_H__