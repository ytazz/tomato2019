/*
    author: H. Kuribayashi

    RvizPlotのサンプルコード
*/

#include "rviz_plot_sample.h"

RvizPlotSample::RvizPlotSample() : dt(0.5) {
  // 送るトピックを指定
  // 点・線・円・四角でトピックとメッセージ型が異なる
  pub_point = nh.advertise<tomato_msgs::RvPoint>("rviz_plot/point", 10);
  pub_line = nh.advertise<tomato_msgs::RvLine>("rviz_plot/line", 10);
  pub_circle = nh.advertise<tomato_msgs::RvCircle>("rviz_plot/circle", 10);
  pub_square = nh.advertise<tomato_msgs::RvSquare>("rviz_plot/square", 10);
  pub_arrow = nh.advertise<tomato_msgs::RvArrow>("rviz_plot/arrow", 10);
  pub_stl = nh.advertise<tomato_msgs::RvStl>("rviz_plot/stl", 10);

  timer =
      nh.createTimer(ros::Duration(dt), &RvizPlotSample::timerCallback, this);
  time = 0.0;
}

RvizPlotSample::~RvizPlotSample() {}

void RvizPlotSample::pubPoint() {
  // ２種類の点群を同時に描画
  tomato_msgs::RvPoint rv_point;

  // 名前は唯一でなければならない(同じ名前を設定すると上書きされる)
  // １つ目の点群
  rv_point.name = "point1";
  if (time <= 5)
    rv_point.action = "add"; // 描画アイテムに追加するとき
  else
    rv_point.action = "delete"; // 描画アイテムから削除するとき
  rv_point.size = 0.1;
  rv_point.color.r = 1;
  rv_point.color.g = 0;
  rv_point.color.b = 0;
  rv_point.color.a = 1;

  geometry_msgs::Point point;
  for (int i = 0; i < 10; i++) {
    point.x = -1.0 + 0.2 * (rand() % 10);
    point.y = -1.0 + 0.2 * (rand() % 10);
    point.z = -1.0 + 0.2 * (rand() % 10);
    rv_point.position.push_back(point);
  }

  pub_point.publish(rv_point);

  // ２つ目の点群
  rv_point.name = "point2";
  if (time <= 5)
    rv_point.action = "add"; // 描画アイテムに追加するとき
  else
    rv_point.action = "delete"; // 描画アイテムから削除するとき
  rv_point.size = 0.1;
  rv_point.color.r = 0;
  rv_point.color.g = 1;
  rv_point.color.b = 1;
  rv_point.color.a = 1;

  rv_point.position.clear();
  for (int i = 0; i < 10; i++) {
    point.x = -1.0 + 0.2 * (rand() % 10);
    point.y = -1.0 + 0.2 * (rand() % 10);
    point.z = -1.0 + 0.2 * (rand() % 10);
    rv_point.position.push_back(point);
  }
  pub_point.publish(rv_point);
}

void RvizPlotSample::pubLine() {
  tomato_msgs::RvLine rv_line;

  rv_line.name = "line1";
  if (5 < time && time <= 10)
    rv_line.action = "add";
  else
    rv_line.action = "delete";
  rv_line.size = 0.02;
  rv_line.color.r = 0;
  rv_line.color.g = 1;
  rv_line.color.b = 0;
  rv_line.color.a = 1;

  geometry_msgs::Point line;
  for (int i = 0; i < 10; i++) {
    line.x = -1.0 + 0.2 * (rand() % 10);
    line.y = -1.0 + 0.2 * (rand() % 10);
    line.z = -1.0 + 0.2 * (rand() % 10);
    rv_line.position.push_back(line);
  }

  pub_line.publish(rv_line);
}

void RvizPlotSample::pubCircle() {
  tomato_msgs::RvCircle rv_circle;

  rv_circle.name = "circle1";
  if (10 < time && time <= 15)
    rv_circle.action = "add";
  else
    rv_circle.action = "delete";
  rv_circle.size = 0.02;
  rv_circle.color.r = 0;
  rv_circle.color.g = 0;
  rv_circle.color.b = 1;
  rv_circle.color.a = 1;

  geometry_msgs::Point circle;
  circle.x = -1.0 + 0.2 * (rand() % 10);
  circle.y = -1.0 + 0.2 * (rand() % 10);
  circle.z = -1.0 + 0.2 * (rand() % 10);
  rv_circle.position = circle;

  rv_circle.radius = 0.1 + 0.1 * (rand() % 10);

  pub_circle.publish(rv_circle);
}

void RvizPlotSample::pubSquare() {
  tomato_msgs::RvSquare rv_square;

  rv_square.name = "square1";
  if (15 < time && time <= 20)
    rv_square.action = "add";
  else
    rv_square.action = "delete";
  rv_square.size = 0.02;
  rv_square.color.r = 1;
  rv_square.color.g = 0;
  rv_square.color.b = 1;
  rv_square.color.a = 1;

  geometry_msgs::Point square;
  square.x = -1.0 + 0.2 * (rand() % 10);
  square.y = -1.0 + 0.2 * (rand() % 10);
  square.z = -1.0 + 0.2 * (rand() % 10);
  rv_square.position = square;

  rv_square.width = 0.1 + 0.1 * (rand() % 10);
  rv_square.height = 0.1 + 0.1 * (rand() % 10);

  pub_square.publish(rv_square);
}

void RvizPlotSample::pubArrow() {
  tomato_msgs::RvArrow rv_arrow;

  rv_arrow.name = "arrow1";
  if (20 < time && time <= 25)
    rv_arrow.action = "add";
  else
    rv_arrow.action = "delete";
  rv_arrow.size = 0.02;
  rv_arrow.color.r = 0;
  rv_arrow.color.g = 1;
  rv_arrow.color.b = 1;
  rv_arrow.color.a = 1;

  geometry_msgs::Point start, end;
  start.x = -1.0 + 0.2 * (rand() % 10);
  start.y = -1.0 + 0.2 * (rand() % 10);
  start.z = -1.0 + 0.2 * (rand() % 10);
  end.x = -1.0 + 0.2 * (rand() % 10);
  end.y = -1.0 + 0.2 * (rand() % 10);
  end.z = -1.0 + 0.2 * (rand() % 10);
  rv_arrow.start = start;
  rv_arrow.end = end;

  pub_arrow.publish(rv_arrow);
}

void RvizPlotSample::pubStl() {
  tomato_msgs::RvStl rv_stl;

  rv_stl.name = "turtlebot";
  if (25 < time && time <= 35)
    rv_stl.action = "add";
  else
    rv_stl.action = "delete";

  if (25 < time && time <= 30)
    rv_stl.position.x = 0.2 * (time - 25);
  else
    rv_stl.position.x = 0.0;
  rv_stl.position.y = 0.0;
  rv_stl.position.z = 0.0;

  if (30 < time && time <= 35)
    rv_stl.orientation = 3.14159 * 0.2 * (time - 30);
  else
    rv_stl.orientation = 0.0;

  pub_stl.publish(rv_stl);
}

void RvizPlotSample::timerCallback(const ros::TimerEvent &) {
  pubPoint();
  pubLine();
  pubCircle();
  pubSquare();
  pubArrow();
  pubStl();

  time += dt;
  if (time > 35)
    time = 0.0;
}

// main関数
// 定義したクラスのオブジェクト（ノード）を生成
int main(int argc, char **argv) {
  ros::init(argc, argv, "rviz_plot_sample");
  RvizPlotSample rviz_plot_sample;
  ros::spin();
}