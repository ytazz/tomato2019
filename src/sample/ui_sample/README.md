# About this package

## rviz_plot_sample

uiパッケージのrviz_plotのサンプルコード

ランダムな位置・形状で点→線→円→四角の順に描画します．

### API

独自のメッセージ型を作成しているので，
メッセージ型定義ファイルやサンプルコードを参考にパラメータを設定してください．  
基本的にはアイテム名や色・サイズ・位置などがパラメータです．

actionの項目については，  
描画対象に追加したい時 -> "add"  
描画対象から削除したい時 -> "delete"  
にしてください

addしていないのにdeleteしたり，
何回も連続でdeleteしても問題ありません．

### サンプルの起動方法

端末を立ち上げて，以下コマンドを打つ．

    $ roslaunch ui_sample rviz_plot_sample.launch

立ち上がったRvizに対して

    Add > MarkerArray > OK

    Global OptionsのFixed Frameをworldに設定

と操作してMarkerArrayを追加してあげれば，
0.5sおきにRvizPlotを使ってランダムに描画されます．