﻿#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <std_msgs/Int64.h>
#include <std_msgs/Float64.h>
#include <geometry_msgs/Twist.h>

int    area; // 緑色の面積を計算（画素）
int    x_sum[640]; // 各位置での緑色領域の面積を保存する配列
int    moment, sum; // 重心を求める際に必要な値
double cog; // 緑色領域の重心(Center of Gravity)のx座標値

class DetectGreen
{
public:
    DetectGreen();
    void timerCallback(const ros::TimerEvent&);
    void imageCallback(const sensor_msgs::ImageConstPtr &msg);

    int    area, sum;
    double cog, moment;
private:
    ros::NodeHandle nh;
    ros::Timer      timer;
    ros::Publisher  image_pub;
    ros::Subscriber sub;
};

DetectGreen::DetectGreen()
{
    image_pub = nh.advertise <geometry_msgs::Twist>("image_info", 1);
    timer     = nh.createTimer(ros::Duration(0.02), &DetectGreen::timerCallback, this);
    sub       = nh.subscribe("camera_raw", 1, &DetectGreen::imageCallback, this);
    area      = 0;
    cog       = 0;
    moment    = 0;
    sum       = 0;
}

void DetectGreen::timerCallback(const ros::TimerEvent&) // timerCallback関数が呼び出されるタイミング(0.02[s]ごと)で緑色領域情報をPublish
{
    geometry_msgs::Twist twist;
    twist.linear.x = area;
    twist.linear.y = cog;
    image_pub.publish(twist);
}

void DetectGreen::imageCallback(const sensor_msgs::ImageConstPtr &msg)
{
    cv_bridge::CvImagePtr cv_ptr;
    try {
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        cv::waitKey(30);
    }
    catch (cv_bridge::Exception &e) {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
    }

    // カメラ情報を処理する
    cv::Mat hsv_img, smooth_img;
    cv::Mat green_detect = cv::Mat(cv::Size(640, 480), CV_8UC1);

    green_detect = cv::Scalar(0);
    cv::medianBlur(cv_ptr->image, smooth_img, 7); // ノイズがあるので平滑化
    cv::cvtColor(smooth_img, hsv_img, CV_BGR2HSV);    // HSVに変換
    // int area=0; //面積を計算（画素）
    area   = 0;
    cog    = 0;
    moment = 0;
    sum    = 0;
    int i;
    for (i = 0; i < hsv_img.cols; i++) {    // 各位置での緑色領域の面積を保存する配列を初期化
        x_sum[i] = 0;
    }
    for (int y = 0; y < hsv_img.rows; y++) // 中心付近の緑色領域の面積計算
    {
        for (int x = 240; x < hsv_img.cols - 240; x++)
        {
            int a = hsv_img.step * y + x *hsv_img.elemSize();
            if (hsv_img.data[a] >= 39 && hsv_img.data[a] <= 50 && hsv_img.data[a + 1] >= 50 && hsv_img.data[a + 2] >= 50) // HSVでの緑色領域検出
            {
                area++;
            }
        }
    }
    for (int x = 0; x < hsv_img.cols; x++) // 画面内すべての緑色領域の面積計算
    {
        for (int y = 0; y < hsv_img.rows; y++)
        {
            int a = hsv_img.step * y + x *hsv_img.elemSize();
            if (hsv_img.data[a] >= 39 && hsv_img.data[a] <= 50 && hsv_img.data[a + 1] >= 50 && hsv_img.data[a + 2] >= 50) // HSVでの緑色領域検出
            {
                x_sum[x]++;
            }
        }
    }
    for (i = 0; i < hsv_img.cols; i++) // 重心計算
    {
        sum    = sum + x_sum[i];
        moment = moment + i * x_sum[i];
    }
    if (sum != 0)
    {
        cog = (double) moment / sum - 320;
    }
    else
    {
        cog = 0;
    }

    // 処理結果を表示する
    // cv::imshow("image_CameraSubscriber", cv_ptr->image);
    if (cog != 0)
    {
        int point = cog + 320;
        cv::circle(cv_ptr->image, cv::Point(point, 240), 50, cv::Scalar(0, 200, 0), -1, CV_AA); // 原画像に重心位置を表す円を表示
    }
    cv::imshow("GreenArea", cv_ptr->image);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "image_subscriber");
    ros::NodeHandle nh;
    cv::namedWindow("GreenArea");
    cv::startWindowThread();
    DetectGreen detectgreen;

    ros::spin();
    cv::destroyWindow("camera_view");

    return EXIT_SUCCESS;
}
