﻿#include <ros/ros.h>
#include <math.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Int64.h>
#include <std_msgs/Float64.h>
#include <sensor_msgs/Imu.h>
#include <kobuki_msgs/BumperEvent.h>
#include <kobuki_msgs/Sound.h>
#include <sensor_msgs/Joy.h>

#define pi         M_PI
#define dt         0.02
#define kobuki_vel 0.1814

class MoveKobuki
{
public:
    MoveKobuki();
    void cameraCallback(const geometry_msgs::TwistConstPtr &info);
    void imuCallback(const sensor_msgs::ImuConstPtr &imu);
    void bumperCallback(const kobuki_msgs::BumperEventConstPtr &bumper);
    void timerCallback(const ros::TimerEvent&);
    void pub_angular(double linear_velocity, double angular_velocity); // 直進速度、回転速度を決めPublish
    void mani(double theta1, double theta2, double theta3, double theta4, double theta5); // 角度をPublish

private:
    ros::NodeHandle nh;
    ros::Publisher  kobuki_pub, sound_pub;
    ros::Subscriber area_sub, cog_sub, red_sub, kobuki_sub, bumper_sub;
    ros::Publisher  pos_pub_1; // Rvizと実機にPublish
    ros::Subscriber joy_sub_; // Joyとオドメトリと関節角度のSubscribe
    ros::Timer      timer;
    double          time, time0, step_time; // 時間
    int             green, red; // 緑の領域の面積
    double          center; // 重心位置(-240から240)
    double          pos[1000];
    double          angular_vel, angular, linear_vel, real_angular; // Kobukiの角速度と角度と速度（正面0．右手方向が正）
    int             attack; // Kobukiのbumperが当たれば1。通常0。
    int             mode;
    int             count, max, max_pos;
    double          p_gain;
    int             red_border; // 鉢に接近したかのしきい値（画素の面積）
    double          acceleration, velocity, position, distance;
    double          pre_acceleration, pre_velocity, pre_position;
    int             odom;
    double          end_angular, t[6];
};

MoveKobuki::MoveKobuki()
{
    kobuki_pub       = nh.advertise <geometry_msgs::Twist>("/mobile_base/commands/velocity", 1);
    pos_pub_1        = nh.advertise <std_msgs::Float64>("/joint1_controller/command", 1);
    kobuki_sub       = nh.subscribe <sensor_msgs::Imu>("/mobile_base/sensors/imu_data", 1, &MoveKobuki::imuCallback, this);
    bumper_sub       = nh.subscribe <kobuki_msgs::BumperEvent>("/mobile_base/events/bumper", 1, &MoveKobuki::bumperCallback, this);
    area_sub         = nh.subscribe <geometry_msgs::Twist>("/image_info", 1, &MoveKobuki::cameraCallback, this);
    timer            = nh.createTimer(ros::Duration(dt), &MoveKobuki::timerCallback, this);
    sound_pub        = nh.advertise <kobuki_msgs::Sound>("/mobile_base/commands/sound", 1);
    angular_vel      = 0.25; // [rad/s]
    linear_vel       = 0.05;
    real_angular     = 0;
    angular          = 0;
    mode             = 0;
    count            = 0;
    max              = 0;
    center           = 0;
    p_gain           = 2;
    max_pos          = 0;
    acceleration     = 0;
    velocity         = 0;
    position         = 0;
    pre_acceleration = 0;
    pre_velocity     = 0;
    pre_position     = 0;
    t[1]             = pi / 2;
    t[2]             = 0;
    t[3]             = 0;
    t[4]             = 0;
    t[5]             = 0;
    for (int j = 0; j < 1000; j++) {
        pos[j] = 0;
    }
    red_border = 120000;
    attack     = 0;
    distance   = 0;
}

void MoveKobuki::pub_angular(double linear_velocity, double angular_velocity)
{
    geometry_msgs::Twist twist;
    twist.linear.x  = linear_velocity;
    twist.angular.z = angular_velocity;
    // angular = angular + angular_velocity*dt*180/pi;
    kobuki_pub.publish(twist);
}

void MoveKobuki::cameraCallback(const geometry_msgs::TwistConstPtr &info)
{
    green  = info->linear.x;
    center = info->linear.y;
    red    = info->linear.z;
}

void MoveKobuki::imuCallback(const sensor_msgs::ImuConstPtr &imu)
{
    real_angular = imu->orientation.z * 180 * 90 / 128;
}

void MoveKobuki::bumperCallback(const kobuki_msgs::BumperEventConstPtr &bumper)
{
    attack = bumper->state;
}

void MoveKobuki::mani(double theta1, double theta2, double theta3, double theta4, double theta5) // 角度をPublish
{
    std_msgs::Float64 joint1;
    joint1.data = theta1;
    pos_pub_1.publish(joint1);
}

void MoveKobuki::timerCallback(const ros::TimerEvent&)
{
    switch (mode) {
        case 0: // 初期位置まで移動
            if (real_angular > -50) {
                pub_angular(0, -angular_vel);
            } else {
                mode = 1;
            }
            break;
        case 1: // 回転しながら緑が一番多いところを検出
            if (real_angular <= 50) {
                pub_angular(0, angular_vel);
                pos[count] = real_angular;
                if (green >= max) {
                    max     = green;
                    max_pos = count;
                }
                count++;
            } else {
                if (pos[max_pos] != 0) {
                    distance = 1.4 / cos(pos[max_pos] * pi / 180);
                } else {
                    distance = 1.4;
                }
                mode = 2;
            }
            break;
        case 2: // 緑が一番多いところに正面を合わせる
            if (real_angular > pos[max_pos]) {
                pub_angular(0, -angular_vel);
            } else {
                mode     = 3;
                position = 0;
                odom     = 0;
            }
            break;
        case 3:
            if (position < distance) {
                pub_angular(0.2, -p_gain * (center / 320) * angular_vel);
                ROS_INFO_STREAM("Approach!");
                position += kobuki_vel * dt;
            }
            if (position >= distance || attack == 1) {
                if (odom == 0 || odom == 50 || odom == 100 || odom == 150) {
                    kobuki_msgs::Sound sound;
                    sound.ON;
                    sound_pub.publish(sound);
                }
                if (odom == 200) {
                    mode = 8;
                    // mode=4;
                    odom = 0;
                }
                ROS_INFO_STREAM("Wait...");
                pub_angular(0, 0);
                odom++;
            }
            break;
        case 4:
            ROS_INFO_STREAM("Attack");
            pub_angular(0.1, 0);
            if (attack == 1) {
                mode = 5;
            }
            break;
        case 5:
            ROS_INFO_STREAM("Back!");
            pub_angular(-0.1, 0);
            if (odom == 100) {
                mode = 6;
            }
            odom++;
            break;
        case 6:
            pub_angular(0, 0);
            end_angular = real_angular;
            mode        = 7;
            break;
        case 7:
            if ((real_angular + end_angular) < 90) {
                ROS_INFO_STREAM("Rolling");
                pub_angular(0, angular_vel);
            } else {
                ROS_INFO_STREAM("Complete!!!!");
                pub_angular(0, 0);
            }
            break;
        case 8:
            std_msgs::Float64 joint1;
            joint1.data = pi / 2;
            pos_pub_1.publish(joint1);
            odom++;
            if (odom >= 200) {
                mode = 4;
                odom = 0;
            }
    }
    // ROS_INFO_STREAM(real_angular+end_angular);
    // ROS_INFO_STREAM(position);
    ROS_INFO_STREAM(distance);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "move_kobuki");
    MoveKobuki movekobuki;
    ros::spin();
}
