﻿#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <sensor_msgs/JointState.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/Float64.h>
#include <math.h>
#include <geometry_msgs/Twist.h>
#include "dynamixel_msgs/MotorStateList.h"

#define pi                 M_PI
#define L1                 0.2
#define L2                 0.25
#define JOINT1_MIDDLE_VAL  3500
#define JOINT2_MIDDLE_VAL  2432
#define JOINT3_MIDDLE_VAL  363
#define JOINT5_MIDDLE_VAL  520
#define JOINT5_MIN_VAL     520
#define JOINT5_MAX_VAL     610
#define RX28_CHANGE_TO_DEG (300.0 / 1024.0)
#define MX64_CHANGE_TO_DEG (360.0 / 4096.0)
#define CHANGE_TO_RAD      (pi / 180.0)

class TeleopRobot
{
public:
    TeleopRobot();
    void joyCallback(const sensor_msgs::Joy::ConstPtr &joy);
    void timerCallback(const ros::TimerEvent&);
    void kobukiCallback(const sensor_msgs::Imu);
    void pub_angular(double theta1, double theta2, double theta3, double theta4, double theta5);
    void move_position(double goal_angular1, double goal_angular2, double goal_angular3);
    void jointCallback(const dynamixel_msgs::MotorStateList::ConstPtr &motorstates);

private:
    ros::NodeHandle nh;
    ros::Timer      timer;
    ros::Publisher  pos_pub_, pos_pub_1, pos_pub_2, pos_pub_3, pos_pub_4, pos_pub_5; // Rvizと実機にPublish
    ros::Subscriber joy_sub_, kobuki_sub, pos_sub_; // Joyとオドメトリと関節角度のSubscribe
    int             a, b[6], i;
    double          L, t[6], now[6], to[6];
    double          time, time0, step_time; // 時間
    double          angular_velocity; // 自動移動時の関節角速度
    double          x, z; // 目標手先位置
    double          x_scale_hand, z_scale_hand, grip_scale; // 手先位置の移動速度
    double          l_scale_kobuki, a_scale_kobuki; // Kobukiの移動速度
    double          memory[6];
    double          to_t[6]; // 範囲制限用
    ros::Publisher  twist_pub_;
    int             id; // モータのID
    double          real_pos[6];
};

TeleopRobot::TeleopRobot()
{
    joy_sub_         = nh.subscribe <sensor_msgs::Joy>("joy", 10, &TeleopRobot::joyCallback, this);
    pos_sub_         = nh.subscribe("/motor_states/pan_tilt_port", 10, &TeleopRobot::jointCallback, this);
    pos_pub_         = nh.advertise <sensor_msgs::JointState>("/joint_states_source", 1);
    pos_pub_1        = nh.advertise <std_msgs::Float64>("/joint1_controller/command", 1);
    pos_pub_2        = nh.advertise <std_msgs::Float64>("/joint2_controller/command", 1);
    pos_pub_3        = nh.advertise <std_msgs::Float64>("/joint3_controller/command", 1);
    pos_pub_4        = nh.advertise <std_msgs::Float64>("/joint4_controller/command", 1);
    pos_pub_5        = nh.advertise <std_msgs::Float64>("/joint5_controller/command", 1);
    timer            = nh.createTimer(ros::Duration(0.025), &TeleopRobot::timerCallback, this);
    twist_pub_       = nh.advertise <geometry_msgs::Twist>("/mobile_base/commands/velocity", 1);
    a                = 0;
    b[1]             = 0;
    b[2]             = 0;
    b[3]             = 0;
    t[1]             = 0;
    t[2]             = 0;
    t[3]             = 0;
    t[4]             = 0;
    t[5]             = 0;
    time             = 0;
    step_time        = 0;
    a_scale_kobuki   = 0.5;
    l_scale_kobuki   = 0.1;
    x_scale_hand     = 0.0001;
    z_scale_hand     = 0.0001;
    grip_scale       = 0.001;
    angular_velocity = 0.0007;
}

void TeleopRobot::timerCallback(const ros::TimerEvent&) // 起動してからの時間のパラメータを生成
{
    time = time + 0.025;
}

void TeleopRobot::pub_angular(double theta1, double theta2, double theta3, double theta4, double theta5) // 角度をPublish
{
    if (time != step_time) {
        std_msgs::Float64 joint1;
        std_msgs::Float64 joint2;
        std_msgs::Float64 joint3;
        std_msgs::Float64 joint4;
        std_msgs::Float64 joint5;

        joint1.data = theta1;
        joint2.data = theta2;
        joint3.data = theta3;
        joint4.data = theta4;
        joint5.data = theta5;
        pos_pub_1.publish(joint1);
        pos_pub_2.publish(joint2);
        pos_pub_3.publish(joint3);
        pos_pub_4.publish(joint4);
        pos_pub_5.publish(joint5);

        step_time = time;
    }
}

void TeleopRobot::jointCallback(const dynamixel_msgs::MotorStateList::ConstPtr &motorstates) // 現在の関節角度をシミュレーション(Rviz)に反映
{
    sensor_msgs::JointState joint;
    joint.velocity.resize(3);
    joint.name.resize(3);
    joint.position.resize(3);
    joint.name[0]     = "Joint0";
    joint.name[1]     = "Joint1";
    joint.name[2]     = "Joint2";
    real_pos[0]       = -((double) (motorstates->motor_states[0].position) - JOINT1_MIDDLE_VAL) * MX64_CHANGE_TO_DEG * CHANGE_TO_RAD; // 回転軸
    real_pos[1]       = ((double) (motorstates->motor_states[1].position) - JOINT2_MIDDLE_VAL) * MX64_CHANGE_TO_DEG * CHANGE_TO_RAD; // 前後軸
    real_pos[2]       = -((double) (motorstates->motor_states[2].position) - JOINT3_MIDDLE_VAL) * RX28_CHANGE_TO_DEG * CHANGE_TO_RAD; // 鉛直軸
    joint.position[0] = real_pos[0];
    joint.position[1] = real_pos[1];
    joint.position[2] = real_pos[1] + real_pos[2];
    pos_pub_.publish(joint);
}

void TeleopRobot::move_position(double goal_angular1, double goal_angular2, double goal_angular3)   // 任意の位置まで移動
{
    // 目標関節角度設定
    to[1] = goal_angular1;
    to[2] = goal_angular2;
    to[3] = goal_angular3;

    // 目標関節角度に移動
    for (i = 1; i <= 3; i++) {
        if (b[i] == 0) { // b[i]=0->まだ達していない,b[i]=1->到達完了
            if (t[i] > to[i]) {
                t[i] = t[i] - angular_velocity;
                if (time - time0 > 5) {   // 3秒以内に動けなければ強制終了
                    b[i] = 1;
                }
                if (real_pos[i - 1] <= to[i]) {
                    b[i] = 1;
                }
            } else if (t[i] < to[i]) {
                t[i] = t[i] + angular_velocity;
                if (time - time0 > 5) {
                    b[i] = 1;
                }
                if (t[i] >= to[i]) {
                    b[i] = 1;
                }
            } else {
                b[i] = 1;
            }
        }
    }
    t[4] = t[3];
}

void TeleopRobot::joyCallback(const sensor_msgs::Joy::ConstPtr &joy)
{
    switch (a) {
        case 0:     // 最初の待機
            ROS_INFO_STREAM("Press L2+R2 button");
            if (joy->buttons[8] == 1 && joy->buttons[9] == 1) {
                a = 1;
            }
            break;
        case 1:
            if (t[1] < pi / 2) {
                t[1] += angular_velocity;
            }
            if (t[1] >= pi / 2) {
                a     = 2;
                time0 = time;
                t[1]  = pi / 2;
            }
            pub_angular(t[1], t[2], t[3], t[4], t[5]);
            break;
        case 2:     // 自動的に初期位置に動く(1秒待つ)
            ROS_INFO_STREAM("Move Initial Position. Please Wait...");
            move_position(pi / 2, -pi / 6, pi / 6);
            if (b[1] == 1 && b[2] == 1 && b[3] == 1) {
                b[1] = 0;
                b[2] = 0;
                b[3] = 0;
                x    = L1 * sin(-pi / 12) + L2 *cos(pi / 12);
                z    = L1 * cos(-pi / 12) + L2 *sin(pi / 12);
                t[1] = pi / 2;
                t[2] = -pi / 12;
                t[3] = pi / 12;
                t[4] = t[3];
                t[5] = 0;
                a    = 3;
            }
            pub_angular(t[1], t[2], t[3], t[4], t[5]);
            break;
        case 3:     // 初期位置に移動完了
            ROS_INFO_STREAM("Complete");
            a = 4;
            ROS_INFO_STREAM("Manipulator Mode");
            break;
        case 4:     // 逆運動学で動かす
            // 手先移動距離
            x += x_scale_hand * (joy->buttons[5] - joy->buttons[7]);
            z += z_scale_hand * (joy->buttons[4] - joy->buttons[6]);
            // 逆運動学計算
            // to_t[2] = pi/2 - acos((pow(L1,2)-pow(L2,2)+pow(x,2)+pow(z,2))/(2*L1*sqrt(pow(x,2)+pow(z,2))))-acos(sqrt(pow(x,2)/(pow(x,2)+pow(z,2))));
            // to_t[3] = -(pi/2 + t[2] -acos((pow(L1,2)+pow(L2,2)-pow(x,2)-pow(z,2))/(2*L1*L2)));
            // if(to_t[2]>=-pi/6 && to_t[2]<=pi/6){
            // if(to_t[3]>=-pi/6 && to_t[3]<=pi/6){
            // }else{
            //    t[3]=to_t[3];
            // }
            // }else{
            //    t[2]=to_t[2];
            // }
            // ※逆運動学は可動範囲の設定が煩雑になるので、今回は各軸制御とした。

            // 各軸操作
            t[2] += 0.0005 * (joy->buttons[5] - joy->buttons[7]);
            t[3] += 0.0005 * (joy->buttons[4] - joy->buttons[6]);
            if (t[2] > pi / 6) {
                t[2] = pi / 6;
            }
            if (t[2] < -pi / 6) {
                t[2] = -pi / 6;
            }
            if (t[3] > pi / 6) {
                t[3] = pi / 6;
            }
            if (t[3] < -pi / 6) {
                t[3] = -pi / 6;
            }
            t[4]  = t[3];
            t[5] += grip_scale * (joy->buttons[13] - joy->buttons[14]);
            if (t[5] < (JOINT5_MIN_VAL - JOINT5_MIDDLE_VAL) * RX28_CHANGE_TO_DEG * CHANGE_TO_RAD)
            {
                t[5] = 0;
            }
            if (t[5] > (JOINT5_MAX_VAL - JOINT5_MIDDLE_VAL) * RX28_CHANGE_TO_DEG * CHANGE_TO_RAD)
            {
                t[5] = (JOINT5_MAX_VAL - JOINT5_MIDDLE_VAL) * RX28_CHANGE_TO_DEG * CHANGE_TO_RAD;
            }
            // ROS_INFO_STREAM(t[5]);
            // フリフリ
            // t[1]=pi/2+pi/12*(joy->buttons[11]-joy->buttons[10]);
            pub_angular(t[1], t[2], t[3], t[4], t[5]);
            if (joy->buttons[3] == 1) { // SERECTが押されたらkobuki用に切り替える
                a = 7;
                ROS_INFO_STREAM("Kobuki Mode");
            }
            if (joy->buttons[1] == 1) {       // 左ぐりぐりが押されたらカゴ位置に自動的に移動
                a = 5;
                for (i = 1; i <= 3; i++) {
                    memory[i] = t[i];
                    b[1]      = 0;
                    b[2]      = 0;
                    b[3]      = 0;
                }
                time0 = time;
            }
            break;
        case 5:     // カゴ位置に自動的に移動
            ROS_INFO_STREAM("Move Basket. Please Wait...");
            move_position(pi, -pi / 6, memory[3]);
            t[4] = t[3];
            if (b[1] == 1 && b[2] == 1 && b[3] == 1) {
                t[5] += grip_scale * (joy->buttons[13] - joy->buttons[14]);
                if (t[5] < (JOINT5_MIN_VAL - JOINT5_MIDDLE_VAL) * RX28_CHANGE_TO_DEG * CHANGE_TO_RAD)
                {
                    t[5] = 0;
                }
                if (t[5] > (JOINT5_MAX_VAL - JOINT5_MIDDLE_VAL) * RX28_CHANGE_TO_DEG * CHANGE_TO_RAD)
                {
                    t[5] = (JOINT5_MAX_VAL - JOINT5_MIDDLE_VAL) * RX28_CHANGE_TO_DEG * CHANGE_TO_RAD;
                }
                if (joy->buttons[2] == 1) {       // 右ぐりぐりが押されたらカゴ位置から収穫時の姿勢に復帰
                    a     = 6;
                    b[1]  = 0;
                    b[2]  = 0;
                    b[3]  = 0;
                    time0 = time;
                    t[5]  = 0;
                }
            }
            pub_angular(t[1], t[2], t[3], t[4], t[5]);
            break;
        case 6:     // カゴ位置から収穫時の姿勢に復帰
            ROS_INFO_STREAM("Return Position. Please Wait...");
            move_position(memory[1], memory[2], memory[3]);
            if (b[1] == 1 && b[2] == 1 && b[3] == 1) {
                a    = 3;
                b[1] = 0;
                b[2] = 0;
                b[3] = 0;
                t[5] = 0;
            }
            pub_angular(t[1], t[2], t[3], t[4], t[5]);
            break;
        case 7:     // kobuki用
            if (joy->buttons[0] == 1) { // SERECTが押されたらkobuki用に切り替える
                a = 4;
                ROS_INFO_STREAM("Manipulator Mode");
            }
            geometry_msgs::Twist twist;
            twist.angular.z = a_scale_kobuki * 0.22 * (joy->buttons[11] - joy->buttons[10]) + a_scale_kobuki * 0.5 * (joy->axes[13] - joy->axes[12]);
            twist.linear.x  = l_scale_kobuki * 0.6 * (joy->buttons[10] - joy->buttons[11]) + l_scale_kobuki * 0.5 * (joy->axes[1] + joy->axes[3]);
            twist_pub_.publish(twist);
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "joy_teleop");
    TeleopRobot teleoprobot;
    ros::spin();
}
