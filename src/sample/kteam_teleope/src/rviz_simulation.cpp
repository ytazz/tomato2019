﻿#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <math.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/JointState.h>
#include <sensor_msgs/Joy.h>
#include <std_msgs/Float64.h>

#define pi M_PI
#define DEG_TO_RAD (pi / 180.0)

class RvizSimulation {
public:
  RvizSimulation();
  void joyCallback(const sensor_msgs::Joy::ConstPtr &joy);
  void timerCallback(const ros::TimerEvent &);
  void pubAngular();
  void calcVirtualAngle();

private:
  ros::NodeHandle nh;
  ros::Timer timer;
  ros::Publisher pos_pub;  // RvizにPublish
  ros::Subscriber joy_sub; // JoyのSubscribe
  const double numJoint;
  double theta1, theta2, theta3, theta4, theta5;
  const double scaleX;
  const double scaleY;
  const double scaleZ;
};

RvizSimulation::RvizSimulation()
    : numJoint(7), scaleX(0.01), scaleY(0.01), scaleZ(0.01) {
  joy_sub = nh.subscribe<sensor_msgs::Joy>("joy", 1,
                                           &RvizSimulation::joyCallback, this);
  pos_pub = nh.advertise<sensor_msgs::JointState>("/joint_states_source", 1);
  timer =
      nh.createTimer(ros::Duration(0.01), &RvizSimulation::timerCallback, this);

  theta1 = 0.0;
  theta2 = 0.4;
  theta3 = -0.8;
  theta4 = 0.4;
  theta5 = 0.0;
}

void calcInverseKinematics() {}

void RvizSimulation::timerCallback(
    const ros::TimerEvent &) // 起動してからの時間のパラメータを生成
{
  sensor_msgs::JointState jointstate;
  jointstate.velocity.resize(7);
  jointstate.name.resize(7);
  jointstate.position.resize(7);

  jointstate.name[0] = "Joint1";
  jointstate.name[1] = "Joint1_2";
  jointstate.name[2] = "Joint1_3";
  jointstate.name[3] = "Joint2";
  jointstate.name[4] = "Joint3";
  jointstate.name[5] = "Joint4";
  jointstate.name[6] = "Joint6";

  jointstate.position[0] = theta1;
  jointstate.position[1] = theta1;
  jointstate.position[2] = theta1;
  jointstate.position[3] = theta2;
  jointstate.position[4] = theta3;
  jointstate.position[5] = theta4;
  jointstate.position[6] = theta5;
  pos_pub.publish(jointstate);
}

void RvizSimulation::joyCallback(
    const sensor_msgs::Joy::ConstPtr
        &joy) // 現在の関節角度をシミュレーション(Rviz)に反映
{
  theta2 += scaleX * joy->buttons[0];
  theta3 += scaleX * joy->buttons[1];
  theta4 += scaleX * joy->buttons[2];
  calcInverseKinematics();
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "joy_teleop");
  RvizSimulation rvizsimulation;
  ros::spin();
}
