#include "ros/ros.h"
/**
* turtlebot_arm.cpp – move turtlebot arm according to predefined gestures
*/

#include "std_msgs/Float64.h"

#include <sstream>

int main(int argc, char **argv)
{

ros::init(argc, argv, "kteam_teleope");

ros::NodeHandle n;

//publish command message to joints/servos of arm

ros::Publisher joint2_pub = n.advertise<std_msgs::Float64>("/joint2_controller/command", 1000);
ros::Publisher joint3_pub = n.advertise<std_msgs::Float64>("/joint3_controller/command", 1000);
ros::Publisher joint4_pub = n.advertise<std_msgs::Float64>("/joint4_controller/command", 1000);

std_msgs::Float64 pos2;
std_msgs::Float64 pos3;
std_msgs::Float64 pos4;

//Initial gesture of robot arm

pos2.data = 0.0;
pos3.data = 0.0;
pos4.data = 0.0;


joint2_pub.publish(pos2);
joint3_pub.publish(pos3);
joint4_pub.publish(pos4);


ros::Rate loop_rate(0.5);

while (ros::ok())
{
//gesture 1

pos2.data = 0.0;
pos3.data = 0.0;
pos4.data = 0.0;

joint2_pub.publish(pos2);
joint3_pub.publish(pos3);
joint4_pub.publish(pos4);


loop_rate.sleep();

//gesture 2

pos2.data = 0.9;
pos3.data = 1.8;
pos4.data = 1.0;


joint2_pub.publish(pos2);
joint3_pub.publish(pos3);
joint4_pub.publish(pos4);


loop_rate.sleep();

//gesture 3

pos2.data = 0.9;
pos3.data = 1.8;
pos4.data = 1.0;


joint2_pub.publish(pos2);
joint3_pub.publish(pos3);
joint4_pub.publish(pos4);

loop_rate.sleep();

//gesture 4

pos2.data = 0.0;
pos3.data = 0.0;
pos4.data = 0.0;


joint2_pub.publish(pos2);
joint3_pub.publish(pos3);
joint4_pub.publish(pos4);


loop_rate.sleep();

//gesture 5

pos2.data = 0.0;
pos3.data = 0.0;
pos4.data = 0.0;


joint2_pub.publish(pos2);
joint3_pub.publish(pos3);
joint4_pub.publish(pos4);


loop_rate.sleep();

//gesture 6
pos2.data = 0.3;
pos3.data = 2.0;
pos4.data = -0.2;

joint2_pub.publish(pos2);
joint3_pub.publish(pos3);
joint4_pub.publish(pos4);


loop_rate.sleep();

//gesture 7

pos2.data = 0.3;
pos3.data = 2.0;
pos4.data = -0.2;


joint2_pub.publish(pos2);
joint3_pub.publish(pos3);
joint4_pub.publish(pos4);

loop_rate.sleep();


//gesture 8

pos2.data = 0.4;
pos3.data = 0.0;
pos4.data = -0.2;

joint2_pub.publish(pos2);
joint3_pub.publish(pos3);
joint4_pub.publish(pos4);


loop_rate.sleep();

//gesture 9

pos2.data = 0.0;
pos3.data = 0.0;
pos4.data = 0.0;

joint2_pub.publish(pos2);
joint3_pub.publish(pos3);
joint4_pub.publish(pos4);


loop_rate.sleep();

//Initial gesture of robot arm
0.0;
pos2.data = 0.0;
pos3.data = 0.0;
pos4.data = 0.0;

joint2_pub.publish(pos2);
joint3_pub.publish(pos3);
joint4_pub.publish(pos4);


loop_rate.sleep();

ros::spinOnce();
}

return 0;
}
