﻿// 0726 kuribayashi: fixed 'sensor value bug'

#include "ros/ros.h"
#include "std_msgs/Float64.h"
#include "sensor_msgs/JointState.h"
#include "dynamixel_msgs/MotorStateList.h"
#include <math.h>
#include <sstream>


#define pi                 M_PI
#define JOINT1_MIDDLE_VAL  2048
#define JOINT2_MIDDLE_VAL  512
#define JOINT3_MIDDLE_VAL  512
#define JOINT4_MIDDLE_VAL  2048
#define RX28_CHANGE_TO_DEG (300.0 / 1024.0)
#define MX28_CHANGE_TO_DEG (360.0 / 4096.0)
#define MX64_CHANGE_TO_DEG (360.0 / 4096.0)
#define CHANGE_TO_RAD      (pi / 180.0)

const double loopRate  = 100;
const int    numJoints = 4;
const int    delta     = 10;

sensor_msgs::JointState jointstate;
// char                    renew = 0;
int                     nowAngle[numJoints], prevAngle[numJoints], pprevAngle[numJoints]; // prev = 1 step before, pprev = 2 step before

void reflect_jointstate(const dynamixel_msgs::MotorStateList::ConstPtr &motorstates);

// the diffrence of angle at before and after steps is correct => true
// the diffrence of angle at before and after steps include large noise => false
bool judgeCorrectValue(dynamixel_msgs::MotorStateList::ConstPtr sensor, int idJoint)
{
    bool sensorFlag = 0;

    // set 1, 2 step before joint angles
    pprevAngle[idJoint] = prevAngle[idJoint];
    prevAngle[idJoint]  = nowAngle[idJoint];

    // set current joint angle
    nowAngle[idJoint] = sensor->motor_states[idJoint].position;

    // judge variation <= delta
    if ((abs(nowAngle[idJoint] - prevAngle[idJoint]) <= delta) && (abs(prevAngle[idJoint] - pprevAngle[idJoint]) <= delta)) {
        sensorFlag = 1;
    }

    // std::cout<< sensorFlag << std::endl;
    return sensorFlag;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "reflect_rviz");
    ros::NodeHandle nh;
    ros::Publisher  joint_states_pub = nh.advertise <sensor_msgs::JointState>("/joint_states_source", 10);
    ros::Subscriber sub              = nh.subscribe("/motor_states/pan_tilt_port", 10, reflect_jointstate);
    ros::Rate       loop_rate(loopRate);

    for (int i = 0; i < numJoints; i++) {
        nowAngle[i]   = 0;
        prevAngle[i]  = 0;
        pprevAngle[i] = 0;
    }

    jointstate.name.push_back("Joint1");
    jointstate.name.push_back("Joint1_2");
    jointstate.name.push_back("Joint1_3");
    jointstate.name.push_back("Joint2");
    jointstate.name.push_back("Joint3");
    jointstate.name.push_back("Joint4");

    jointstate.position.push_back(0.0);
    jointstate.position.push_back(0.0);
    jointstate.position.push_back(0.0);
    jointstate.position.push_back(0.0);
    jointstate.position.push_back(0.0);
    jointstate.position.push_back(0.0);


    // jointstate.name[0] = "Joint1";
    // jointstate.name[1] = "Joint2";
    // jointstate.name[2] = "Joint3";
    // jointstate.name[3] = "Joint4";
    //
    // jointstate.position[0]=0;
    // jointstate.position[1]=0;
    // jointstate.position[2]=0;
    // jointstate.position[3]=0;

    while (ros::ok())
    {
        // ?
        // while (ros::ok() && renew == 0) {
        //     ros::spinOnce();
        //     loop_rate.sleep();
        // }

        jointstate.header.stamp = ros::Time::now();

        joint_states_pub.publish(jointstate);
        // renew = 0;


        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}

void reflect_jointstate(const dynamixel_msgs::MotorStateList::ConstPtr &motorstates)
{
    if (judgeCorrectValue(motorstates, 0)) {
        jointstate.position[0] = ((double) (motorstates->motor_states[0].position) - JOINT1_MIDDLE_VAL) * MX64_CHANGE_TO_DEG * CHANGE_TO_RAD;
        jointstate.position[1] = -2 * ((double) (motorstates->motor_states[0].position) - JOINT1_MIDDLE_VAL) * MX64_CHANGE_TO_DEG * CHANGE_TO_RAD;
        jointstate.position[2] = ((double) (motorstates->motor_states[0].position) - JOINT1_MIDDLE_VAL) * MX64_CHANGE_TO_DEG * CHANGE_TO_RAD;
    }
    if (judgeCorrectValue(motorstates, 1)) {
      jointstate.position[3] = 5*((double) (motorstates->motor_states[1].position) - JOINT2_MIDDLE_VAL) * RX28_CHANGE_TO_DEG * CHANGE_TO_RAD/3;
      if (judgeCorrectValue(motorstates, 2)) {
        jointstate.position[4] =
        (((double) (motorstates->motor_states[2].position) - JOINT3_MIDDLE_VAL) * RX28_CHANGE_TO_DEG * CHANGE_TO_RAD) - 5*(((double) (motorstates->motor_states[1].position) - JOINT2_MIDDLE_VAL) * RX28_CHANGE_TO_DEG * CHANGE_TO_RAD);
      }
    }
    if (judgeCorrectValue(motorstates, 3)) {
        jointstate.position[5] = ((double) (motorstates->motor_states[3].position) - JOINT4_MIDDLE_VAL) * MX28_CHANGE_TO_DEG * CHANGE_TO_RAD;
    }

    // std::cout <<"[ " << pprevAngle[3]<<" , " << prevAngle[3]<<" , " << nowAngle[3]<<" ]" <<std::endl;
    // std::cout<<jointstate.position[3]<<std::endl;
    // renew = 1;
}
