#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <std_msgs/Float64.h>
#include "sensor_msgs/JointState.h"
#include "dynamixel_msgs/MotorStateList.h"
#include <math.h>
#include <geometry_msgs/Twist.h>

#define pi                 M_PI
#define JOINT1_MIDDLE_VAL  2048
#define JOINT2_MIDDLE_VAL  512
#define JOINT3_MIDDLE_VAL  512
#define JOINT4_MIDDLE_VAL  2048
#define JOINT5_MIDDLE_VAL  512
#define RX28_CHANGE_TO_DEG (300.0 / 1024.0)
#define MX28_CHANGE_TO_DEG (360.0 / 4096.0)
#define MX64_CHANGE_TO_DEG (360.0 / 4096.0)
#define AX12_CHANGE_TO_DEG (300.0 / 1024.0)
#define CHANGE_TO_RAD      (pi / 180.0)

class TeleopRobot
{

public:
  TeleopRobot();
  void joyCallback(const sensor_msgs::Joy::ConstPtr &joy);
  void jointCallback(const dynamixel_msgs::MotorStateList::ConstPtr &motorstates);
  void timerCallback(const ros::TimerEvent&);
  void pub_angular();

private:
  ros::NodeHandle nh;
  ros::Timer      timer;
  ros::Publisher  joint1_pub,joint2_pub,joint3_pub,joint4_pub,joint5_pub,joint6_pub;
  ros::Subscriber joy_sub;
  ros::Subscriber kobuki_sub; // subscriberの変数宣言
  ros::Publisher  kobuki_pub; // publisherの変数宣言
  double          scaleLinear, scaleAngular;
  double a;
  double time, step_time;
  double real_pos[6] ;
  int mode ;
  //変数の宣言(int a; とかと一緒)
  std_msgs::Float64 joint1;
  std_msgs::Float64 joint2;
  std_msgs::Float64 joint3;
  std_msgs::Float64 joint4;
  std_msgs::Float64 joint5;
  std_msgs::Float64 joint6;
  sensor_msgs::JointState joint;
  double theta1, theta2, theta3, theta4,z,theta6,theta5;
};

TeleopRobot::TeleopRobot(){
  joy_sub           = nh.subscribe <sensor_msgs::Joy>("joy", 10, &TeleopRobot::joyCallback, this);
  // pos_sub_          = nh.subscribe ("/motor_states/pan_tilt_port", 10, &TeleopRobot::jointCallback, this);
  joint1_pub        = nh.advertise <std_msgs::Float64>("/joint1_controller/command", 1);
  joint2_pub        = nh.advertise <std_msgs::Float64>("/joint2_controller/command", 1);
  joint3_pub        = nh.advertise <std_msgs::Float64>("/joint3_controller/command", 1);
  joint4_pub        = nh.advertise <std_msgs::Float64>("/joint4_controller/command", 1);
  joint5_pub        = nh.advertise <std_msgs::Float64>("/joint5_controller/command", 1);
  joint6_pub        = nh.advertise <std_msgs::Float64>("/joint6_controller/command", 1);
  // pos_pub_          = nh.advertise <sensor_msgs::JointState>("/joint_states_source", 1);
  timer            = nh.createTimer(ros::Duration(0.025), &TeleopRobot::timerCallback, this);


  a = 0.003;
  joint1.data=0;
  joint2.data=0;
  joint3.data=0;
  joint4.data=0;
  joint5.data=0;
  joint6.data=0;
  // joint5.data=0;
  mode=0;
  time             = 0;
  step_time        = 0;
  theta1=0;
  theta2=0;
  theta3=0;
  theta4=0;
  theta5=0;
  theta6=0;
  // theta5=0;
  z=0;
  // subscriber
  // kobuki_sub = nh.subscribe <sensor_msgs::Imu>("トピック名わすれた", 10, &KobukiOperation::kobukiCallback, this);　//kobukiからのセンサ情報


  // publisher

  kobuki_pub = nh.advertise <geometry_msgs::Twist>("/mobile_base/commands/velocity", 1); //Kobuki実機
  // turtle_pub = nh.advertise <geometry_msgs::Twist>("/turtle1/cmd_vel", 1); // turtlesimで操作確認する時用

  // 変数の初期化
  scaleLinear  = 0.03;  //0.025   操作出来てる感　0.03
  scaleAngular = 0.3;   //0.26                 0.3
}
//

void TeleopRobot::timerCallback(const ros::TimerEvent&) // 起動してからの時間のパラメータを生成
{
    // time = time + 0.025;
    pub_angular();

}

void TeleopRobot::pub_angular() // 角度をPublish
{
    // if (time != step_time) {
        std_msgs::Float64 joint1;
        std_msgs::Float64 joint2;
        std_msgs::Float64 joint3;
        std_msgs::Float64 joint4;
        std_msgs::Float64 joint5;
        std_msgs::Float64 joint6;

        joint1.data = theta1;
        joint2.data = theta2;
        joint3.data = theta3;
        joint4.data = theta4;
        joint5.data = theta5;
        joint6.data = theta6;

        joint1_pub.publish(joint1);
        joint2_pub.publish(joint2);
        joint3_pub.publish(joint3);
        joint4_pub.publish(joint4);
        joint5_pub.publish(joint5);
        joint6_pub.publish(joint6);


        // step_time = time;
    // }
}




void TeleopRobot::joyCallback(const sensor_msgs::Joy::ConstPtr &joy) //実機
{

switch(mode){
  case 0:
  ROS_INFO_STREAM("AUTO");
  ROS_INFO_STREAM("Manipulator mode press AUTO, Kobuki mode press CLEAR");
  if(joy->buttons[8] == 1) {
      ROS_INFO_STREAM("Manipulator mode Change");
      mode=1;
  }
  else if(joy->buttons[9] == 1) {
   ROS_INFO_STREAM(" Kobuki Mode");
      mode=3;
    }




  break;


case 1:
  ROS_INFO_STREAM("joy_Teleop");
  ROS_INFO_STREAM("Change mode press AUTO");
  if (joy->buttons[8] == 1) {
      ROS_INFO_STREAM("Change mode");
      mode=0;
  }
  else if(joy->buttons[9] == 1){
  mode=2;
  }
  else {
      //
      // z += 100*a*(joy->buttons[0] - joy->buttons[1]);
      // if(z>=200){
      //   z=200;
      // }
      // else if(z<=-200){
      //   z=-200;
      // }
      // theta1 = asin(z/400);


  theta1 += a*(-joy->buttons[0] + 2*joy->buttons[0]*joy->buttons[4]);

  if(theta1 >=pi/6){
    theta1 = pi/6;
  }
  else if(theta1 <=-pi/6){
    theta1 = -pi/6;
  }


  theta2 += a*(joy->buttons[1] - 2*joy->buttons[1]*joy->buttons[4]);
  if(theta2 >=pi/20*3){
    theta2 = pi/20*3;
  }
  else if(theta2 <=-pi/20*3){
    theta2 = -pi/20*3;
  }

  theta3 += a*(joy->buttons[2] - 2*joy->buttons[2]*joy->buttons[4]);
  if(theta3 >=pi/4*30/38){
    theta3 = pi/4*30/38;
  }
  else if(theta3 <=-pi/2*30/38){
    theta3 = -pi/2*30/38;
  }

  theta4 += a*(joy->buttons[3] - 2*joy->buttons[3]*joy->buttons[4]);
  if(theta4 >=pi/2){
    theta1 = pi/2;
  }
  else if(theta4 <=-pi/2){
    theta4 = -pi/2;
  }

  theta6 += a*(joy->buttons[0] - 2*joy->buttons[0]*joy->buttons[4]);

  if(theta6 >=pi/6){
    theta6 = pi/6;
  }
  else if(theta6 <=-pi/6){
    theta6 = -pi/6;
  }





//
// twist.angular.z = 0.15 *(joy->axes[0] -joy->buttons[2]);
  // pub_angular(theta1, theta2, theta3, theta4);
}
break;


case 2:
  ROS_INFO_STREAM("syuukaku");
  if (joy->buttons[8] == 1&& joy->buttons[9] == 1) {
      ROS_INFO_STREAM("Change mode");
      mode=1;
  }
  else{
    // theta5 = 5/3;
    joint5_pub.publish(joint5);
  }



case 3:
  ROS_INFO_STREAM("Kobuki");
  ROS_INFO_STREAM("Change mode press AUTO");
  ROS_INFO_STREAM("button1,3=x ,button2,4=z");
  if (joy->buttons[8] == 1) {
      ROS_INFO_STREAM("Change mode");
      mode=0;
  }
 else{
  geometry_msgs::Twist twist;
  twist.linear.x  = scaleLinear * (joy->buttons[0] -joy->buttons[2]);
  twist.angular.z = scaleAngular *(joy->buttons[1] -joy->buttons[3]);
  kobuki_pub.publish(twist);
}
break ;
}
   // // // model joy操作
   // joint.velocity.resize(6);
   // joint.name.resize(6);
   // joint.position.resize(6);
   // joint.name[0]     = "Joint1";
   // joint.name[1]     = "Joint1_2";
   // joint.name[2]     = "Joint1_3";
   // joint.name[3]     = "Joint2";
   // joint.name[4]     = "Joint3";
   // joint.name[5]     = "Joint4";
   //
   //
   // joint.position[0] += -0.005*(joy->buttons[0] - joy->buttons[1]);
   // joint.position[1] = -2*joint.position[0] ;
   // // std::cout<<joint.position[1]<<std::endl;
   // joint.position[2] = joint.position[0] ;
   // joint.position[3] += 0.005*(joy->buttons[2] - joy->buttons[3]);
   // joint.position[4] +=  0.005*(joy->buttons[4] - joy->buttons[5]) - 0.005*(joy->buttons[2] - joy->buttons[3]);
   // joint.position[5] += 0.005*(joy->buttons[6] -joy->buttons[7]);
   //

   //
   //     pos_pub_.publish(joint); //joint情報をmodelにpublish
 }





// void TeleopRobot::jointCallback(const dynamixel_msgs::MotorStateList::ConstPtr &motorstates) // 現在の関節角度をシミュレーション(Rviz)に反映
// {
//   sensor_msgs::JointState joint;
//   joint.velocity.resize(6);
//   joint.name.resize(6);
//   joint.position.resize(6);
//   joint.name[0]     = "Joint1";
//   joint.name[1]     = "Joint1_2";
//   joint.name[2]     = "Joint1_3";
//   joint.name[3]     = "Joint2";
//   joint.name[4]     = "Joint3";
//   joint.name[5]     = "Joint4";
//
//
//
//   real_pos[0] = ((double) (motorstates->motor_states[0].position) - JOINT1_MIDDLE_VAL) * MX64_CHANGE_TO_DEG * CHANGE_TO_RAD;
//   real_pos[1] = ((double) (motorstates->motor_states[1].position) - JOINT2_MIDDLE_VAL) * RX28_CHANGE_TO_DEG * CHANGE_TO_RAD;
//   real_pos[2] = ((double) (motorstates->motor_states[2].position) - JOINT3_MIDDLE_VAL) * RX28_CHANGE_TO_DEG * CHANGE_TO_RAD;
//   real_pos[3] = ((double) (motorstates->motor_states[3].position) - JOINT4_MIDDLE_VAL) * MX28_CHANGE_TO_DEG * CHANGE_TO_RAD;
//
//   joint.position[0] = real_pos[0];
//   joint.position[1] = -2*real_pos[0];
//   // std::cout<<joint.position[1]<<std::endl;
//   joint.position[2] = real_pos[0];
//   joint.position[3] = real_pos[1];
//   joint.position[4] = real_pos[2]-real_pos[1];
//   joint.position[5] = real_pos[3];
//
//
//   pos_pub_.publish(joint);
// }


int main(int argc, char **argv)
{
    ros::init(argc, argv, "kteam_teleope");
    TeleopRobot teleoprobot;
    ros::spin();
}
