/*
    author: H. Kuribayashi

    KobukiPlannerのサンプルコード
*/

#ifndef __KOBUKI_PLANNER_SAMPLE_H__
#define __KOBUKI_PLANNER_SAMPLE_H__

// ヘッダファイルの読み込み
// ROS系
#include <ros/ros.h>
// 自分で定義したメッセージ型
#include <tomato_msgs/KobukiPhase.h> // Kobuki軌道計画用

class KobukiPlannerSample {
public:
  KobukiPlannerSample();
  ~KobukiPlannerSample();

  void pubPhase();

  // 一定時間おきにphaseをpublish
  void timerCallback(const ros::TimerEvent &);

private:
  // ROSシステムの変数
  ros::NodeHandle nh;
  ros::Timer timer;

  // phaseや初期位置をpublish
  ros::Publisher pub;

  // 時間
  const double dt; // timestep
  double time;     // 経過時間
};

#endif // __KOBUKI_PLANNER_SAMPLE_H__