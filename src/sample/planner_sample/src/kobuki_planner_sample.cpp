#include "kobuki_planner_sample.h"

KobukiPlannerSample::KobukiPlannerSample() : dt(0.01) {
  pub = nh.advertise<tomato_msgs::KobukiPhase>("kobuki/phase", 1);

  timer = nh.createTimer(ros::Duration(dt), &KobukiPlannerSample::timerCallback,
                         this);
  time = 0.0;
}

KobukiPlannerSample::~KobukiPlannerSample() {}

void KobukiPlannerSample::pubPhase() {
  tomato_msgs::KobukiPhase phase;
  phase.phase = tomato_msgs::KobukiPhase::APPROACH;
  phase.waypoint.linear.x = 1.0;
  phase.waypoint.linear.y = -1.0;
  phase.waypoint.linear.z = 0.0;
  phase.waypoint.angular.x = 0.0;
  phase.waypoint.angular.y = 0.0;
  phase.waypoint.angular.z = 0.0;

  pub.publish(phase);
}

void KobukiPlannerSample::timerCallback(const ros::TimerEvent &) {
  if (time > 5)
    pubPhase();
  std::cout << time << '\n';

  time += dt;
}

// main関数
// 定義したクラスのオブジェクト（ノード）を生成
int main(int argc, char **argv) {
  ros::init(argc, argv, "kobuki_planner_sample");
  KobukiPlannerSample kobuki_planner_sample;
  ros::spin();
}