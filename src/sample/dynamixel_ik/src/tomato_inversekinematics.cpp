/*橋本ダブル、ツージー
*/
#include <fstream>
#include <iostream>
#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <dynamixel_msgs/JointState.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

//stdは名前空間で、標準的な関数や型が含まれる
using namespace std;

#define pi 3.141592
// ＊＊＊正確なリンク長の追記が必要＊＊＊
const double l1 = 0.0825; // [m]
const double l2 = 0.094; // [m]
const double l3 = 0.1; // [m]
double current_angle1 = 0.0;
double current_angle2 = 0.0;
double current_pose_x = 0.0;
double current_pose_z = 0.0;
// タイムステップの設定
const double timestep = 0.01; // 制御のタイムステップ[s]



//-------------------------------------------------------------------------------------------------------------------
// 逆行列計算プログラム
//　float64 data　
void Inverse(double x,double z,std_msgs::Float64& theta1, std_msgs::Float64& theta2, std_msgs::Float64& theta3)
{
	double r1,r2;

	r1 = (x*x + z*z + l1*l1 - l2*l2 + l3*l3 - 2*x*l3)/(2*l1);
	theta1.data = atan2(z,(x-l3)) - atan2(sqrt((x-l3)*(x-l3) + z*z - r1*r1),r1);
	r2 = (z - l1*sin(theta1.data))/l2;
	theta2.data = atan2(cos(theta1.data),sin(theta1.data))- atan2(sqrt(1-r2*r2),r2);
	theta3.data = - (theta1.data + theta2.data);
  theta1.data = pi/2 - theta1.data;//変数分けが必要。。。。。。。。。。
	printf("target%f,%f,%f\n",theta1.data,theta2.data,theta3.data);
}
//---------------------------------------------------------------------------------------------------------------
//順運動学プログラム-------------------------------------------------------------------------------------------------
void Forward(double& x, double& z, double theta1, double theta2)
{
	x = l1*cos(theta1) + l2*cos(theta1 + theta2) + l3 ;
	z = l1*sin(theta1) + l2*sin(theta1 + theta2);
}
//----------------------------------------------------------------------------------------------------------------
//Callback関数の作成------------------------------------------------------------------------------------------------
void messageCallback1(const dynamixel_msgs::JointState& dynamixel)
{
	current_angle1 = dynamixel.current_pos;
	current_angle1 = pi/2 - current_angle1; //変数分けが必要。。。。。。。。。。
}
void messageCallback2(const dynamixel_msgs::JointState& dynamixel)
{
  current_angle2 = dynamixel.current_pos;
}
//----------------------------------------------------------------------------------------------------------------



int main(int argc, char** argv){

	//ノード作成
	ros::init(argc, argv, "mani_operation");
	// ros::init(argc, argv, "subscriber_node");
  ros::NodeHandle n;
	// ros::NodeHandle nh;

  //piblisher作成
	ros::Publisher theta1_pub = n.advertise<std_msgs::Float64>("/tilt2_controller/command", 10);
	ros::Publisher theta2_pub = n.advertise<std_msgs::Float64>("/tilt3_controller/command", 10);
	ros::Publisher theta3_pub = n.advertise<std_msgs::Float64>("/tilt4_controller/command", 10);
	//Subscriberの作成
	ros::Subscriber present_theta1_sub = n.subscribe("/tilt2_controller/state",1000,&messageCallback1);
	ros::Subscriber present_theta2_sub = n.subscribe("/tilt3_controller/state",1000,&messageCallback2);

	double target_pose_x;
	double target_pose_z;

	// target_pose_x = 0.1;
	// target_pose_z = 0.1;

	cout << "手先の目標位置xを入力してください\n";
	cin >> target_pose_x;
	cout << "手先の目標位置zを入力してください\n";
	cin >> target_pose_z;

  std_msgs::Float64 theta1,theta2,theta3;
	// printf("1");

   while(ros::ok()){


   Forward(current_pose_x,current_pose_z,current_angle1,current_angle2);
	 // printf("%f, %f", current_pose_x, current_pose_z);
   Inverse(target_pose_x,target_pose_z,theta1,theta2,theta3);
   // printf("%f,%f,%f\n",theta1,theta2,theta3);
	 printf("current%f,%f\n",current_angle1,current_angle2);

	 theta1_pub.publish(theta1);
	 theta2_pub.publish(theta2);
	 theta3_pub.publish(theta3);
	 // printf("2");

   if((-0.005 < target_pose_x-current_pose_x < 0.005) && (-0.005 < target_pose_z - current_pose_z < 0.005))
   {
		 cout << "手先の目標位置xを入力してください\n";
		 cin >> target_pose_x;
		 cout << "手先の目標位置zを入力してください\n";
		 cin >> target_pose_z;
		 // printf("3");
   }


	 //最速で回すros::spinOnce()
   ros::spinOnce();
   }
}
//--------------------------------------------------------------------------------
