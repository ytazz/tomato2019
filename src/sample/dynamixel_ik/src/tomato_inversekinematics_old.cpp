#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

//stdは名前空間で、標準的な関数や型が含まれる
using namespace std;

#define pi 3.141592
// ＊＊＊正確なリンク長の追記が必要＊＊＊
const double l1 = 0.0825; // [m]
const double l2 = 0.094; // [m]
const double l3 = 0.1; // [m]

// タイムステップの設定
const double timestep = 0.01; // 制御のタイムステップ[s]



//--------------------------------------------------------------------------------
// 逆行列計算プログラム
//　float64 data　
void Inverse(double x,double z,double& theta1, double& theta2, double& theta3)
{
	double r1,r2;

	r1 = (x*x + z*z + l1*l1 - l2*l2 + l3*l3 - 2*x*l3)/(2*l1);
	theta1 = atan2(z,(x-l3)) - atan2(sqrt((x-l3)*(x-l3) + z*z - r1*r1),r1);
	r2 = (z - l1*sin(theta1))/l2;
	theta2 = atan2(cos(theta1),sin(theta1))- atan2(sqrt(1-r2*r2),r2);
	theta3 = - (theta1 + theta2);
  theta1 = pi/2 - theta1;
}


int main(){
	double target_pose_x;
	double target_pose_z;

	cout << "手先の目標位置xを入力してください\n";
	cin >> target_pose_x;

	cout << "手先の目標位置zを入力してください\n";
	cin >> target_pose_z;


	//ノード作成
	// ros::init(argc, argv, "mani_operation");
  // ros::NodeHandle n;

  //piblisher作成
	// ros::Publisher theta1_pub = n.advertise<std_msgs::Float64>("/tilt2_controller/command", 1);
	// ros::Publisher theta2_pub = n.advertise<std_msgs::Float64>("/tilt3_controller/command", 1);
	// ros::Publisher theta3_pub = n.advertise<std_msgs::Float64>("/tilt4_controller/command", 1);


 double theta1,theta2,theta3;
 // while(ros::ok()){

 Inverse(target_pose_x,target_pose_z,theta1,theta2,theta3);
 printf("%f,%f,%f\n",theta1,theta2,theta3);

 // theta1_pub.publish(theta1);
 // theta2_pub.publish(theta2);
 // theta3_pub.publish(theta3);
 // //1回だけコールバック関数を呼び出すものがros::spinOnce()
   // ros::spinOnce();
// }
}
//--------------------------------------------------------------------------------
