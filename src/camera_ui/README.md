# About this package

remoteで  

    $ roslaunch camera_ui remote.launch 
    $ roslaunch camera_ui pointcloud_remote.launch


localで  

    $ roslaunch camera_ui local.launch 
    $ roslaunch camera_ui pointcloud_local.launch

pointcloud_localのターミナルで`1`を入力すると点群が表示される  

# TODO
- キャプチャ取るのをサービスにする
- キャプチャにrgbのせる
- キャプチャ複数保存
- キャリブレーション
