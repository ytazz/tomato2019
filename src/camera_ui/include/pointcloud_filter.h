/*
  Author: TAKEUCHI Yukako
*/

#ifndef __POINTCLOUD_FILTER_H__
#define __POINTCLOUD_FILTER_H__

//ROS
#include <ros/ros.h>
//PCL
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
//original
#include <camera_ui/CaptureCloud.h> //srv
#include <std_srvs/Empty.h>

//フィルタ用変数 rosparamで変更可
std::string pr_field_name = "z";
double pr_leaf_size = 0.1; //default voxelのサイズ
double pr_limit_min = 0.0; //default 表示するpointsの最小距離
double pr_limit_max = 3.5; //default 表示するpointsの最大距離

class PointCloudFilter
{
public:
  PointCloudFilter();
  ~PointCloudFilter();

  void filterCallback(const sensor_msgs::PointCloud2::ConstPtr &input_cloud); //input_cloudにフィルタをかけてoutput_cloudに変換
  bool filterService(camera_ui::CaptureCloud::Request &req, camera_ui::CaptureCloud::Response &res);

private:
  ros::Subscriber sub;
  ros::Publisher pub;
  ros::ServiceServer service;
  ros::NodeHandle nh;
  sensor_msgs::PointCloud2 output_cloud;
};

#endif // __POINTCLOUD_FILTER_H__