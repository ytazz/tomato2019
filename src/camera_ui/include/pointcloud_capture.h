/*
  Author: TAKEUCHI Yukako
*/

#ifndef __POINTCLOUD_CAPTURE_H__
#define __POINTCLOUD_CAPTURE_H__

//ROS
#include <ros/ros.h>
#include <std_msgs/Int8.h>
//PCL
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
//original
#include <camera_ui/CaptureCloud.h>  //srv

class PointCloudCapture
{
public:
  PointCloudCapture();
  ~PointCloudCapture();

  void captureCallback(const std_msgs::Int8::ConstPtr &input_sw);           //pointcloudをキャプチャ
  void cloudCallback(const sensor_msgs::PointCloud2::ConstPtr &input_cloud); //pointcloudを受け取るだけ そのうちサービスに変更

private:
  ros::Subscriber sub_sw;
  ros::Subscriber sub_cloud; //そのうちサービスに変更したい
  ros::Publisher pub_cloud;
  //ros::ServiceClient cliant_cloud;  //点群を要求
  ros::NodeHandle nh;

  sensor_msgs::PointCloud2 tmp_cloud;
  sensor_msgs::PointCloud2 output_capture;
  sensor_msgs::PointCloud2 capture_array[10];
  int sw = 0;
  int capture_num = 0;
};

#endif // __POINTCLOUD_CAPTURE_H__