//参考URL http://robot.isc.chubu.ac.jp/?p=560

#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
//#include <tomato_xtion/Coordinate_rectangle.h>
#include <geometry_msgs/Vector3.h>
#include <std_msgs/Int16.h>
//#include <tomato_xtion/Coordinate_xyz.h>

#define _USE_MATH_DEFINES
#include <math.h>

#define MAX_DEPTH 5.5
#define IMAGE_WIDTH 640     //画面のpixel幅
#define IMAGE_HIGHT 480     //画面のpixel高さ
#define HORIZONTAL_ANGLE 31 //画角　中心から
#define VERTICAL_ANGLE 23   //画角　中心から
#define CAMERA_ANGLE 0      //カメラ固定角度　kobukiに対して

//出力座標calibration用ゲイン　上から順番に調整する
#define CAMERA_X_GAIN 1.0
#define CAMERA_Y_GAIN 1.0
#define CAMERA_Z_GAIN 1.0
#define KOBUKI_Y_GAIN 1.0
#define KOBUKI_X_GAIN 1.0
#define KOBUKI_Z_GAIN 1.0

class Depth_estimater
{
public:
    Depth_estimater();
    ~Depth_estimater();
    //void coordinateCallback(const tomato_xtion::Coordinate_rectangle::ConstPtr &image_xy);
    void tomatoxyCallback(const geometry_msgs::Vector3ConstPtr &image_xy);
    void rgbImageCallback(const sensor_msgs::ImageConstPtr &msg);
    void depthImageCallback(const sensor_msgs::ImageConstPtr &msg);
    void switchCallback(const std_msgs::Int16ConstPtr &sw);
    void swmanualCallback(const geometry_msgs::Vector3ConstPtr &swmanual);

private:
    ros::NodeHandle nh;
    ros::Subscriber sub_coo, sub_tomatoxy, sub_rgb, sub_depth, sub_switch, sub_swmanual;
    ros::Publisher pub_xyz, pub_ope;
    int image_x = IMAGE_WIDTH / 2;
    int image_y = IMAGE_HIGHT / 2;
    //image(画面)座標
    int image_x1 = image_x - 1;
    int image_x2 = image_x + 1;
    int image_y1 = image_y - 1;
    int image_y2 = image_y + 1;

    int manual_switch = 0;
    int image_x_manual = -1;
    int image_y_manual = -1;

    geometry_msgs::Vector3 target_xyz_kobuki;
    geometry_msgs::Vector3 target_xyz_attesou;
};

Depth_estimater::Depth_estimater()
{

    //sub_coo = nh.subscribe<tomato_xtion::Coordinate_rectangle>("/coordinate_topic", 1, &Depth_estimater::coordinateCallback, this);
    sub_tomatoxy = nh.subscribe<geometry_msgs::Vector3>("/tomato_xy", 1, &Depth_estimater::tomatoxyCallback, this);
    sub_rgb = nh.subscribe<sensor_msgs::Image>("/camera/rgb/image_raw", 1, &Depth_estimater::rgbImageCallback, this);
    sub_depth = nh.subscribe<sensor_msgs::Image>("/camera/depth/image", 1, &Depth_estimater::depthImageCallback, this);
    sub_switch = nh.subscribe<std_msgs::Int16>("/switch_auto_kobuki", 1, &Depth_estimater::switchCallback, this);
    sub_swmanual = nh.subscribe<geometry_msgs::Vector3>("/image_xy_switch", 1, &Depth_estimater::swmanualCallback, this);
    //sub_coo 対象物の座標を受け取る　coordinate_publisher用
    //sub_tomatoxy 対象物の座標を受け取る　detect_tomato用
    //sub_swmanual 入力があれば座標指定をマニュアルモードに

    pub_xyz = nh.advertise<geometry_msgs::Vector3>("/target_xyz_topic", 1);
    pub_ope = nh.advertise<geometry_msgs::Vector3>("/xyz_for_operator", 1);

    //pub_xyz kobuki自律移動用座標出力
    //pub_ope オペレータ用座標出力

    target_xyz_kobuki.x = 1.0;
    target_xyz_kobuki.y = 0.0;
    target_xyz_kobuki.z = 0.0;
    target_xyz_attesou.x = 1.0;
    target_xyz_attesou.y = 0.0;
    target_xyz_attesou.z = 0.0;
}

Depth_estimater::~Depth_estimater()
{
}

//トピックから座標をとる関数　coordinate_publisher用
/*void Depth_estimater::coordinateCallback(const tomato_xtion::Coordinate_rectangle::ConstPtr &image_xy)
{

    image_x = image_xy->rec_x1;
    image_x1 = image_x - 1;
    image_x2 = image_x + 1;
    image_y = image_xy->rec_y1;
    image_y1 = image_y - 1;
    ;
    image_y2 = image_y + 1;
}*/

//トピックから座標をとる関数　detect_tomato用
void Depth_estimater::tomatoxyCallback(const geometry_msgs::Vector3ConstPtr &image_xy)
{

    image_x = image_xy->x;
    image_x1 = image_x - 1;
    image_x2 = image_x + 1;
    image_y = image_xy->y;
    image_y1 = image_y - 1;
    image_y2 = image_y + 1;
}

//キー入力あったら座標をpub
void Depth_estimater::switchCallback(const std_msgs::Int16ConstPtr &sw)
{

    if (sw->data == 1)
        pub_xyz.publish(target_xyz_attesou);
}

void Depth_estimater::swmanualCallback(const geometry_msgs::Vector3ConstPtr &swmanual)
{

    if (swmanual->z == 1)
    {
        manual_switch = 1;
    }
    if (swmanual->z == -1)
    {
        manual_switch = 0;
    }

    switch ((int)swmanual->x)
    {
    case 1:
        image_x_manual += 1;
        break;
    case -1:
        image_x_manual -= 1;
        break;
    default:
        break;
    }

    switch ((int)swmanual->y)
    {
    case 1:
        image_y_manual -= 1;
        break;
    case -1:
        image_y_manual += 1;
        break;
    default:
        break;
    }
}

//rgb画像を出力する関数
void Depth_estimater::rgbImageCallback(const sensor_msgs::ImageConstPtr &msg)
{

    int i, j;
    cv_bridge::CvImagePtr cv_ptr;
    //cv_ptrは画像を扱うための変数

    if (manual_switch == 1)
    {
        image_x = image_x_manual;
        image_y = image_y_manual;
    }
    //マニュアルで座標入力があればそっちを使う

    try
    {
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        //toCopy()でROSからOpenCVの形式に変換　購読した画像をBGR8形式にしてcv_ptrに代入
    }
    catch (cv_bridge::Exception &ex)
    {
        ROS_ERROR("error");
        exit(-1);
        //購読できなかったらエラー
    }

    cv::Mat &mat = cv_ptr->image;
    //cv_ptrのimageをmatに代入

    std::string str = cv::format("(%d,%d)", image_x, image_y);

    cv::circle(cv_ptr->image, cv::Point(image_x, image_y), 1, cv::Scalar(0, 255, 255), 3, 4);
    //cv::putText(cv_ptr->image, str, cv::Point(image_x-40, image_y-15), cv::FONT_HERSHEY_SIMPLEX, 0.6, cv::Scalar(0, 255, 255), 1, 4);
    cv::imshow("RGB image", cv_ptr->image);
    //画像出力
    cv::waitKey(10);
    //waitKey()　キーが押されるまで待機　今回はキー入力必要ないが画像を表示する場合には必要
}

//depth画像の出力、targetの座標計算、publiushする関数
void Depth_estimater::depthImageCallback(const sensor_msgs::ImageConstPtr &msg)
{

    int i, j, k;
    double sum = 0.0;
    double ave = 0.0;
    double target_x_camera = 0.0;
    double target_y_camera = 0.0;
    double target_z_camera = 0.0;
    //camera座標
    double target_x_kobuki = 0.0;
    double target_y_kobuki = 0.0;
    double target_z_kobuki = 0.0;
    //kobuki座標

    if (manual_switch == 1)
    {
        image_x = image_x_manual;
        image_y = image_y_manual;
        image_x1 = image_x_manual - 1;
        image_x2 = image_x_manual + 1;
        image_y1 = image_y_manual - 1;
        image_y2 = image_y_manual + 1;
    }
    //マニュアルで座標入力があればそっちを使う

    cv_bridge::CvImagePtr cv_ptr;

    try
    {
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_32FC1);
        //購読した画像をTYPE_32FC1形式にしてcv_ptrに代入 TYPE_32FC1は1チャンネル浮動小数点数(1つの画素に32ビット)
    }
    catch (cv_bridge::Exception &ex)
    {
        ROS_ERROR("error");
        exit(-1);
    }

    cv::Mat depth(cv_ptr->image.rows, cv_ptr->image.cols, CV_32FC1);
    cv::Mat img(cv_ptr->image.rows, cv_ptr->image.cols, CV_8UC1);
    //指定した大きさの配列確保　CV_8UC1はBGR8と同じらしい？

    //targetまでの距離値の合計を計算　0以下は除外
    for (i = 0; i < cv_ptr->image.rows; i++)
    {
        float *Dimage = cv_ptr->image.ptr<float>(i);
        float *Iimage = depth.ptr<float>(i);
        char *Ivimage = img.ptr<char>(i);
        for (j = 0; j < cv_ptr->image.cols; j++)
        {

            if (Dimage[j] > 0.0)
            {
                Iimage[j] = Dimage[j];
                Ivimage[j] = (char)(255 * (Dimage[j] / MAX_DEPTH));
                //距離値を取得できる最大距離で割り、255を掛ける　最大距離は調整必要？
                //画像出力の方に関わる
            }
            else
            {
            }

            if (i >= image_y1 && i <= image_y2)
            {
                if (j >= image_x1 && j <= image_x2)
                {
                    if (Dimage[j] > 0.0)
                    {
                        sum += Dimage[j];
                    }
                }
            }
            //1点だとなんだかうまくいかないので3x3マスで平均
        }
    }

    ave = sum / ((image_x2 - image_x1 + 1) * (image_y2 - image_y1 + 1));

    target_x_camera = CAMERA_X_GAIN * ave;
    target_y_camera = CAMERA_Y_GAIN * ((IMAGE_WIDTH / 2 - (double)image_x) / (IMAGE_WIDTH / 2)) * target_x_camera * tan(HORIZONTAL_ANGLE * M_PI / 180);
    target_z_camera = CAMERA_Z_GAIN * ((IMAGE_HIGHT / 2 - (double)image_y) / (IMAGE_HIGHT / 2)) * target_x_camera * tan(VERTICAL_ANGLE * M_PI / 180);
    //camara座標

    target_y_kobuki = KOBUKI_Y_GAIN * target_y_camera;
    target_x_kobuki = KOBUKI_X_GAIN * (target_x_camera * cos(CAMERA_ANGLE * M_PI / 180) + target_z_camera * sin(CAMERA_ANGLE * M_PI / 180));
    target_z_kobuki = KOBUKI_Z_GAIN * (-target_x_camera * sin(CAMERA_ANGLE * M_PI / 180) + target_z_camera * cos(CAMERA_ANGLE * M_PI / 180));
    //kobuki座標

    target_xyz_kobuki.x = target_x_kobuki;
    target_xyz_kobuki.y = target_y_kobuki;
    target_xyz_kobuki.z = target_z_kobuki;

    pub_ope.publish(target_xyz_kobuki);
    //オペレータ用座標

    //トマト範囲　初期位置から
    if (1.5 < target_x_kobuki && target_x_kobuki < 2.8 && -1.2 < target_y_kobuki && target_y_kobuki < 1.2)
    {
        target_xyz_attesou.x = target_x_kobuki;
        target_xyz_attesou.y = target_y_kobuki;
        target_xyz_attesou.z = target_z_kobuki;
    }

    //ROS_INFO("attesou(x,y,z) : (%f, %f, %f) [m]", target_xyz_attesou.x, target_xyz_attesou.y, target_xyz_attesou.z);
    //ROS_INFO
    ROS_INFO("image(x,y) (%d, %d) [px]: camera(x,y,z) : (%f, %f, %f) [m]", image_x, image_y, target_x_camera, target_y_camera, target_z_camera);
    //ROS_INFO("kobuki(x,y,z) : (%f, %f, %f) [m]", target_x_kobuki, target_y_kobuki, target_z_kobuki); //対象物の座標出力
    //cv::imshow("DEPTH image", img);     //画像出力
    cv::waitKey(100);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "get_coordinate_and_depth_estimate");

    Depth_estimater depth_estimater;

    ros::spin();
    return 0;
}
