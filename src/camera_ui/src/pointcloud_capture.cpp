/*
  Author: TAKEUCHI Yukako
*/

#include "pointcloud_capture.h"

PointCloudCapture::PointCloudCapture()
{
    sub_sw = nh.subscribe<std_msgs::Int8>("capture_switch", 1, &PointCloudCapture::captureCallback, this);
    sub_cloud = nh.subscribe<sensor_msgs::PointCloud2>("depth_filtered", 1, &PointCloudCapture::cloudCallback, this);
    pub_cloud = nh.advertise<sensor_msgs::PointCloud2>("capture", 1);
    //cliant_cloud = nh.serviceClient<camera_ui::CaptureCloud>("/output_service");
}

PointCloudCapture::~PointCloudCapture() {}

void PointCloudCapture::cloudCallback(const sensor_msgs::PointCloud2::ConstPtr &input_cloud)
{
    tmp_cloud = *input_cloud;
}

void PointCloudCapture::captureCallback(const std_msgs::Int8::ConstPtr &input_sw)
{
    sw = input_sw->data;
    //capture_array[sw] = output_capture;
    //ROS_INFO("%d \n", sw);
    if (sw == 0)
    {
        capture_num += 1;
        if (capture_num > 9)
        {
            capture_num = 1;
        }

        //camera_ui::CaptureCloud srv_cloud;
        //if(cliant_cloud.call(srv_cloud)){
        //    ROS_INFO("receive");
        //}
        output_capture = tmp_cloud;
        //output_capture = srv_cloud.response.cloud;
        pub_cloud.publish(output_capture);

        capture_array[capture_num] = output_capture;
        ROS_INFO("Saved to capture%d \n", capture_num);
    }
    else if (sw > 0)
    {
        pub_cloud.publish(capture_array[sw]);
        ROS_INFO("Display capture%d \n", sw);
    }else{
        //pass;
    }

    //TODO 複数保存
    //TODO 座標変換
    //TODO kobuki_pos
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "pointcloud_capture");
    PointCloudCapture pointcloud_capture;
    ros::spin();
}