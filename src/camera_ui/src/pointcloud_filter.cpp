/*
  Author: TAKEUCHI Yukako
*/

#include "pointcloud_filter.h"

PointCloudFilter::PointCloudFilter()
{
    sub = nh.subscribe<sensor_msgs::PointCloud2>("input", 1, &PointCloudFilter::filterCallback, this);
    pub = nh.advertise<sensor_msgs::PointCloud2>("output", 1);
    service = nh.advertiseService("output_service", &PointCloudFilter::filterService, this);
}

PointCloudFilter::~PointCloudFilter() {}

void PointCloudFilter::filterCallback(const sensor_msgs::PointCloud2::ConstPtr &input_cloud)
{

    //std::cout << pr_leaf_size << std::endl;
    //std::cout << pr_limit_min << std::endl;
    //std::cout << pr_limit_max << std::endl;
    //std::cout << pr_field_name << std::endl;

    // Container for original & filtered data
    pcl::PCLPointCloud2 *cloud = new pcl::PCLPointCloud2;
    pcl::PCLPointCloud2::ConstPtr cloudPtr(cloud);
    pcl::PCLPointCloud2 cloud_filtered;

    // Convert to PCL data type
    pcl_conversions::toPCL(*input_cloud, *cloud);

    // Perform the actual filtering
    pcl::VoxelGrid<pcl::PCLPointCloud2> sor;
    sor.setInputCloud(cloudPtr);
    sor.setFilterFieldName(pr_field_name);
    //pr_field_name = sor.getFilterFieldName();
    sor.setFilterLimitsNegative(false);
    sor.setLeafSize(pr_leaf_size, pr_leaf_size, pr_leaf_size);
    sor.setFilterLimits(pr_limit_min, pr_limit_max);
    sor.filter(cloud_filtered);

    // Convert to ROS data type
    pcl_conversions::fromPCL(cloud_filtered, output_cloud);

    // Publish the data
    pub.publish(output_cloud);
}

bool PointCloudFilter::filterService(camera_ui::CaptureCloud::Request &req, camera_ui::CaptureCloud::Response &res)
{
    res.cloud = output_cloud;
    return true;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "pointcloud_filter");

    //rosparamの受け取り
    ros::NodeHandle pn("~"); //クラス内で定義するとparamをrelativeにできなさそう 困った
    pn.getParam("field_name", pr_field_name);
    pn.getParam("/filter/leaf_size", pr_leaf_size);
    pn.getParam("limit_min", pr_limit_min);
    pn.getParam("limit_max", pr_limit_max);

    PointCloudFilter pointcloud_filter;
    ros::spin();
}