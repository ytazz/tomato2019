#!/usr/bin/env python
import rospy
from std_msgs.msg import Int8
import sys, select, termios, tty

# Author: TAKEUCHI Yukako

msg = """
------------------------------------------------------
Push key 0-9
------------------------------------------------------
"""

tenkeyBindings = {
	'0': 0,
	'1': 1,
	'2': 2,
	'3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9
}

LOOP_RATE = 50 #Hz

def getKey():
	tty.setraw(sys.stdin.fileno())
	select.select([sys.stdin], [], [], 0)
	key = sys.stdin.read(1)
	termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
	return key

if __name__=="__main__":
    settings = termios.tcgetattr(sys.stdin)
    rospy.init_node('tenkey')
    rate = rospy.Rate(LOOP_RATE)

    select_num = -1

    pub_num = rospy.Publisher('/tenkey_num', Int8, queue_size = 1)


    try:
        print msg
        while(1):
            key = getKey()
            if (key == '\x03'):
                print "Goodbye..."
                break
            elif key in tenkeyBindings.keys():
                select_num = tenkeyBindings[key]
            else:
                pass

            if(select_num >= 0):
                pub_num.publish(select_num)
                print "tenkey :",
                print select_num
                select_num = -1

    except Exception as e:
         print e

    finally:
        pub_num.publish(-1)
        
