#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3
from std_msgs.msg import Empty
from std_msgs.msg import Float64
from std_msgs.msg import Int16
import sys, select, termios, tty

msg = """
kobuki               | manipulator
-------------------- |--------------------------------
auto mode start: 1   | cutting   : f  | loosen    : g
auto mode stop : 0   |
-------------------- | up        : x  | down      : c
reset odometry : 9   | forward   : q  | back      : a
-------------------- |
Moving around:       | joint2 <- : s  | joint2 -> : d
        8            | joint3 <- : w  | joint3 -> : e
   4    5    6       |
        2            | stop      : z  | CTRL-C to quit
-------------------- |--------------------------------
image depth control
------------------------------------------------------
turn on / off automatic mode : o / p
move point
                        i
                     j     l
                        k
------------------------------------------------------
"""
moveBindings = {
	'8':(1,  0,  0,  0),
	'4':(0,  1,  0,  0),
	'6':(0, -1,  0,  0),
	'2':(-1, 0,  0,  0),
    '1':(0,  0,  1,  0),
    '0':(0,  0, -1,  0),
    '9':(0,  0,  0,  1)
}

jointBindings = {
    'f':(1,  0,  0,  0,  0),
    'g':(-1, 0,  0,  0,  0),
    'd':(0,  1,  0,  0,  0),
    's':(0, -1,  0,  0,  0),
    'e':(0,  0,  1,  0,  0),
    'w':(0,  0, -1,  0,  0),
    'x':(0,  0,  0,  1,  0),
    'c':(0,  0,  0, -1,  0),
    'q':(0,  0,  0,  0,  1),
    'a':(0,  0,  0,  0, -1),
    'z':(0,  0,  0,  0,  0)
}

imageBindings = {
    'j':(-1, 0,  0),
    'l':(1,  0,  0),
    'i':(0,  1,  0),
    'k':(0, -1,  0),
    'o':(0,  0,  1),
    'p':(0,  0, -1)
}

LOOP_RATE = 50 #Hz
PI = 3.141592

MX64AR_RESOLUTION = 0.088 #degree
MX28AR_RESOLUTION = 0.088 #degree
AX12A_RESOLUTION = 0.29

JOINT_ONE_MAX_SPEED = 1.0
JOINT_TWO_MAX_SPEED = 1.0
JOINT_THREE_MAX_SPEED = 1.0
JOINT_FOUR_MAX_SPEED = 1.0

JOINT_ONE_MIN_ANGLE_INT = 0
JOINT_ONE_MAX_ANGLE_INT = 1023
JOINT_ONE_INIT_ANGLE_INT = 0

JOINT_TWO_MIN_ANGLE_INT = 210
JOINT_TWO_MAX_ANGLE_INT = 706
JOINT_TWO_INIT_ANGLE_INT = 706

JOINT_THREE_MIN_ANGLE_INT = 1370
JOINT_THREE_MAX_ANGLE_INT = 2101
JOINT_THREE_INIT_ANGLE_INT = 2100

JOINT_ONE_MAX_ANGLE = (JOINT_ONE_MAX_ANGLE_INT - JOINT_ONE_INIT_ANGLE_INT) * AX12A_RESOLUTION * (PI/180)
JOINT_ONE_MIN_ANGLE = (JOINT_ONE_MIN_ANGLE_INT - JOINT_ONE_INIT_ANGLE_INT) * AX12A_RESOLUTION * (PI/180)
JOINT_TWO_MAX_ANGLE = (JOINT_TWO_MAX_ANGLE_INT - JOINT_TWO_INIT_ANGLE_INT) * MX28AR_RESOLUTION * (PI/180)
JOINT_TWO_MIN_ANGLE = (JOINT_TWO_MIN_ANGLE_INT - JOINT_TWO_INIT_ANGLE_INT) * MX28AR_RESOLUTION * (PI/180)
JOINT_THREE_MAX_ANGLE = (JOINT_THREE_MAX_ANGLE_INT - JOINT_THREE_INIT_ANGLE_INT) * MX28AR_RESOLUTION * (PI/180)
JOINT_THREE_MIN_ANGLE = (JOINT_THREE_MIN_ANGLE_INT - JOINT_THREE_INIT_ANGLE_INT) * MX28AR_RESOLUTION * (PI/180)


def getKey():
	tty.setraw(sys.stdin.fileno())
	select.select([sys.stdin], [], [], 0)
	key = sys.stdin.read(1)
	termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
	return key

if __name__=="__main__":
    settings = termios.tcgetattr(sys.stdin)
    rospy.init_node('keyboard_controller')
    rate = rospy.Rate(LOOP_RATE)

    joint1_goal_pos = Float64()
    joint2_goal_pos = Float64()
    joint3_goal_pos = Float64()
    joint4_goal_pos = Float64()

    joint1_goal_pos = (0 - JOINT_ONE_INIT_ANGLE_INT) * AX12A_RESOLUTION * (PI/180)
    joint2_goal_pos = (0 - JOINT_TWO_INIT_ANGLE_INT) * MX28AR_RESOLUTION * (PI/180)
    joint3_goal_pos = (0 - JOINT_THREE_INIT_ANGLE_INT) * MX28AR_RESOLUTION * (PI/180)
    joint4_goal_pos = 0

    image_xy_switch = Vector3()

    image_xy_switch.x = 0
    image_xy_switch.y = 0
    image_xy_switch.z = 0

    joint1 = 0
    joint2 = 0
    joint3 = 0
    joint4 = 0
    x_manipulator = 0
    x_kobuki = 0
    th = 0
    switch = -1
    reset_kobuki_key = 0
    reset_odometry = Empty()

    speed = 0.1
    turn = 0.5

    pub_kobuki = rospy.Publisher('/mobile_base/commands/velocity', Twist, queue_size = 1)
    pub_kobuki_reset = rospy.Publisher('/mobile_base/commands/reset_odometry', Empty, queue_size=1)
    pub_switch = rospy.Publisher('/switch_auto_kobuki', Int16, queue_size = 1)
    pub_image_xy_switch = rospy.Publisher('/image_xy_switch', Vector3, queue_size=1)
    # pub_manipulator = rospy.Publisher('/manipulator_xy', Vector3, queue_size = 1)
    joint1_pub = rospy.Publisher('/joint1_controller/command', Float64, queue_size=1000)
    joint2_pub = rospy.Publisher('/joint2_controller/command', Float64, queue_size=1000)
    joint3_pub = rospy.Publisher('/joint3_controller/command', Float64, queue_size=1000)
    joint4_pub = rospy.Publisher('/joint4_controller/command', Float64, queue_size=1000)


    try:
        print msg
        while(1):
            key = getKey()
            if key in jointBindings.keys():
                joint1 = jointBindings[key][0]
                joint2 = jointBindings[key][1]
                joint3 = jointBindings[key][2]
                joint4 = jointBindings[key][3]
                x_manipulator = jointBindings[key][4]
                x_kobuki = 0
                th = 0
                switch = -1
                reset_kobuki_key = 0
                image_xy_switch.x = 0
                image_xy_switch.y = 0
                image_xy_switch.z = 0
            elif key in moveBindings.keys():
                joint1 = 0
                joint2 = 0
                joint3 = 0
                joint4 = 0
                x_manipulator = 0
                x_kobuki = moveBindings[key][0]
                th = moveBindings[key][1]
                switch = moveBindings[key][2]
                reset_kobuki_key = moveBindings[key][3]
                image_xy_switch.x = 0
                image_xy_switch.y = 0
                image_xy_switch.z = 0
            elif key in imageBindings.keys():
                joint1 = 0
                joint2 = 0
                joint3 = 0
                joint4 = 0
                x_manipulator = 0
                x_kobuki = 0
                th = 0
                switch = 0
                reset_kobuki_key = 0
                image_xy_switch.x = imageBindings[key][0]
                image_xy_switch.y = imageBindings[key][1]
                image_xy_switch.z = imageBindings[key][2]
            else:
                joint1 = 0
                joint2 = 0
                joint3 = 0
                joint4 = 0
                x_manipulator = 0
                x_kobuki = 0
                th = 0
                switch = -1
                reset_kobuki_key = 0
                image_xy_switch.x = 0
                image_xy_switch.y = 0
                image_xy_switch.z = 0
                if (key == '\x03'):
                    break
                pass

            joint1_goal_pos += joint1*0.1
            joint2_goal_pos += joint2*0.1 + x_manipulator * 0.1
            joint3_goal_pos += joint3*0.1 - x_manipulator * 0.1
            joint4_goal_pos += joint4*0.5

            if (joint1_goal_pos > JOINT_ONE_MAX_ANGLE):
                joint1_goal_pos = JOINT_ONE_MAX_ANGLE
            elif (joint1_goal_pos < JOINT_ONE_MIN_ANGLE):
                joint1_goal_pos = JOINT_ONE_MIN_ANGLE
            joint1_pub.publish(joint1_goal_pos)

            if (joint2_goal_pos > JOINT_TWO_MAX_ANGLE):
                joint2_goal_pos = JOINT_TWO_MAX_ANGLE
            elif (joint2_goal_pos < JOINT_TWO_MIN_ANGLE):
                joint2_goal_pos = JOINT_TWO_MIN_ANGLE
            joint2_pub.publish(joint2_goal_pos)

            if (joint3_goal_pos > JOINT_THREE_MAX_ANGLE):
                joint3_goal_pos = JOINT_THREE_MAX_ANGLE
            elif (joint3_goal_pos < JOINT_THREE_MIN_ANGLE):
                joint3_goal_pos = JOINT_THREE_MIN_ANGLE
            joint3_pub.publish(joint3_goal_pos)

            if (key == 'z'):
                joint4_goal_pos = 0.0
            elif (joint4_goal_pos > 2.0):
                joint4_goal_pos = 2.0
            elif (joint4_goal_pos < -2.0):
                joint4_goal_pos = -2.0
            joint4_pub.publish(joint4_goal_pos)

            twist = Twist()
            twist.linear.x = x_kobuki*speed; twist.linear.y = 0; twist.linear.z = 0;
            twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = th*turn
            pub_kobuki.publish(twist)

            if(switch == -1):
                pub_switch.publish(0)
            elif(switch == 1):
                pub_switch.publish(1)

            if (reset_kobuki_key==1):
                pub_kobuki_reset.publish(reset_odometry)
                reset_kobuki_key = 0

            pub_image_xy_switch.publish(image_xy_switch)

            # vector3 = Vector3()
            # vector3.x = x_manipulator; vector3.y = 0; vector3.z = 0
            # pub_manipulator.publish(vector3)

    except Exception as e:
         print e

    finally:
        twist = Twist()
        twist.linear.x = 0; twist.linear.y = 0; twist.linear.z = 0
        twist.angular.x = 0; twist.angular.y = 0; twist.angular.z = 0
        pub_kobuki.publish(twist)

        pub_switch.publish(0)
        
