# About this package

## rviz_plot

Rviz上に点や線を簡単に描画できるクラス  
計画した軌道やトマトエリアの表示などに使ってください

以下のアイテムの描画に対応しています

    - 点  
    - 線  
    - 円  
    - 四角

以下のアイテムについては実装予定なのでしばしお待ちを

    - 矢印  
    - テキスト  

サンプルコードを用意しているので，参考にしてください．  
*tomato2019/src/sample/ui_sample*   
詳しくはui_sampleパッケージのREADMEにて解説しています．

使いにくい，こうして欲しい等あれば栗林まで．