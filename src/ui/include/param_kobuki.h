/*
    author: H. Kuribayashi

    Kobukiのパラメータ
    ハードウェア性能・大きさ等
*/

#ifndef __KOBUKI_PARAM_H__
#define __KOBUKI_PARAM_H__

#define LINEAR_VEL_MIN = 0.03
#define LINEAR_VEL_MAX = 0.3
#define ANGULAR_VEL_MIN = 0.3
#define ANGULAR_VEL_MAX = 2.0

#endif // __KOBUKI_PARAM_H__