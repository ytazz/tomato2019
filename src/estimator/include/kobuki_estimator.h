/*
    author: H. Kuribayashi

    State Estimator for Kobuki Robot

    state variable xi consists of
      xi_x    : position x of center of the robot
      xi_y    : position y of center of the robot
      xi_theta: orientation
      xi_v    : linear velocity
      xi_omega: angular velocity
*/

#ifndef __KOBUKI_ESTIMATOR_H__
#define __KOBUKI_ESTIMATOR_H__

// ヘッダファイルの読み込み
// C言語系
#include <cmath>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/LU>
#include <string>
// ROS系
#include <geometry_msgs/Pose.h>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <tf/transform_broadcaster.h>
#include <tomato_msgs/KobukiState.h>

typedef Eigen::Vector3d vec3_t;
typedef Eigen::Matrix3d mat3_t;
typedef Eigen::Matrix4d mat4_t;

class KobukiEstimator {
public:
  KobukiEstimator();
  ~KobukiEstimator();

  void reset(const geometry_msgs::Pose &pose);
  void set(const sensor_msgs::Imu &imu);
  void set(const geometry_msgs::Pose &pose);
  tomato_msgs::KobukiState get();

private:
  double origin_x, origin_y, origin_z;
  double origin_roll, origin_pitch, origin_yaw;
  tomato_msgs::KobukiState kobuki_state, kobuki_state_;

  mat4_t world_T_base, world_T_kobuki, world_T_planter;
  mat4_t base_T_kobuki, base_T_planter;
};

#endif // __KOBUKI_ESTIMATOR_H__