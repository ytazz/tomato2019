/*
    author: H. Kuribayashi

    State Estimator for Manipulator
*/

#ifndef __MANIP_ESTIMATOR_H__
#define __MANIP_ESTIMATOR_H__

// ヘッダファイルの読み込み
// C言語系
#include <cmath>
#include <string>
// ROS系
#include <dynamixel_msgs/MotorStateList.h>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float64.h>
// 自分で定義したメッセージ型
#include <tomato_msgs/ManipState.h>

#define NUMBER_OF_JOINT 8

#define JOINT0_MIN_VAL 0
#define JOINT1_MIN_VAL 2250
#define JOINT2_MIN_VAL 3148
#define JOINT3_MIN_VAL 0
#define JOINT4_MIN_VAL 0
#define JOINT5_MIN_VAL 25
#define JOINT6_MIN_VAL 511
#define JOINT7_MIN_VAL 304

#define JOINT0_MIDDLE_VAL 511
#define JOINT1_MIDDLE_VAL 3213
#define JOINT2_MIDDLE_VAL 3148
#define JOINT3_MIDDLE_VAL 1752
#define JOINT4_MIDDLE_VAL 1030
#define JOINT5_MIDDLE_VAL 25
#define JOINT6_MIDDLE_VAL 803
#define JOINT7_MIDDLE_VAL 304

#define JOINT0_MAX_VAL 1023
#define JOINT1_MAX_VAL 4095
#define JOINT2_MAX_VAL 4087
#define JOINT3_MAX_VAL 1752
#define JOINT4_MAX_VAL 2070
#define JOINT5_MAX_VAL 1023
#define JOINT6_MAX_VAL 803
#define JOINT7_MAX_VAL 506

#define JOINT0_DIRECTION 1
#define JOINT1_DIRECTION 1
#define JOINT2_DIRECTION 1
#define JOINT3_DIRECTION -1
#define JOINT4_DIRECTION 1
#define JOINT5_DIRECTION 1
#define JOINT6_DIRECTION 1
#define JOINT7_DIRECTION 1

#define AX12A_COUNT_TO_DEG (300.0 / 1024.0)
#define MX28AR_COUNT_TO_DEG (360.0 / 4096.0)
#define MX64AR_COUNT_TO_DEG (360.0 / 4096.0)

#define DEG_TO_RAD (M_PI / 180.0)
#define DEG_TO_LINEAR (0.5 / (360.0 * 100))

#define THRESHOLD 0.01

class ManipEstimator {
public:
  ManipEstimator();
  ~ManipEstimator();

  void set(const dynamixel_msgs::MotorStateList::ConstPtr &motor_);
  tomato_msgs::ManipState get();

  bool isCorrect(double pos, int id);

private:
  ros::NodeHandle nh;
  ros::Publisher pub_angle[NUMBER_OF_JOINT];
  ros::Publisher pub_torque[NUMBER_OF_JOINT];

  tomato_msgs::ManipState manip_state;

  double qold[NUMBER_OF_JOINT]; // joint angles at previous step
  double qlst[NUMBER_OF_JOINT]; // joint angles at 2step before
};

#endif // __MANIP_ESTIMATOR_H__
