/*
    author: H. Kuribayashi

    State Estimator for Planter
*/

#ifndef __PLANTER_ESTIMATOR_H__
#define __PLANTER_ESTIMATOR_H__

// ヘッダファイルの読み込み
// C言語系
#include <cmath>
#include <string>
// ROS系
#include <geometry_msgs/Pose.h>
#include <ros/ros.h>
#include <tomato_msgs/PlanterState.h>

class PlanterEstimator {
public:
  PlanterEstimator();
  ~PlanterEstimator();

  void set(const geometry_msgs::Pose &pose);
  tomato_msgs::PlanterState get();

private:
  tomato_msgs::PlanterState planter_state;
};

#endif // __PLANTER_ESTIMATOR_H__