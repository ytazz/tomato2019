/*
    author: H. Kuribayashi

    State Estimator for Robots
*/

#ifndef __STATE_ESTIMATOR_H__
#define __STATE_ESTIMATOR_H__

// ヘッダファイルの読み込み
// C言語系
#include <cmath>
#include <string>
#include <stdio.h>
#include <iostream>
// ROS系
#include "kobuki_estimator.h"
// #include "manip_estimator.h"
#include "planter_estimator.h"
#include <dynamixel_msgs/JointState.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64.h>
#include <tomato_msgs/ManipState.h>

#define NUMBER_OF_JOINT 8
#define BALL_COUNT 50
#define BALL_DIST 0.414

class StateEstimator {
public:
  StateEstimator();
  ~StateEstimator();

  // void motorCallback(const dynamixel_msgs::MotorStateList::ConstPtr &state);
  void joint0Callback(const dynamixel_msgs::JointState::ConstPtr &state);
  void joint1Callback(const dynamixel_msgs::JointState::ConstPtr &state);
  void joint2Callback(const dynamixel_msgs::JointState::ConstPtr &state);
  void joint3Callback(const dynamixel_msgs::JointState::ConstPtr &state);
  void joint4Callback(const dynamixel_msgs::JointState::ConstPtr &state);
  void joint5Callback(const dynamixel_msgs::JointState::ConstPtr &state);
  void joint6Callback(const dynamixel_msgs::JointState::ConstPtr &state);
  void joint7Callback(const dynamixel_msgs::JointState::ConstPtr &state);
  void odomCallback(const sensor_msgs::Imu::ConstPtr &imu);
  void mocapOriginCallback(const geometry_msgs::PoseStamped::ConstPtr &stamp);
  void mocapKobukiCallback(const geometry_msgs::PoseStamped::ConstPtr &stamp);
  void mocapPlanterCallback(const geometry_msgs::PoseStamped::ConstPtr &stamp);

private:
  PlanterEstimator planter_estimator;
  KobukiEstimator kobuki_estimator;
  // ManipEstimator manip_estimator;

  ros::Subscriber sub_joint0, sub_joint1, sub_joint2, sub_joint3, sub_joint4;
  ros::Subscriber sub_joint5, sub_joint6, sub_joint7;

  ros::NodeHandle nh;

  ros::Publisher pub_planter, pub_kobuki, pub_manip;
  ros::Subscriber sub_motor, sub_odom;
  ros::Subscriber sub_mocap_origin, sub_mocap_kobuki, sub_mocap_planter;

  tomato_msgs::ManipState manip_state;

  int count;
};

#endif // __STATE_ESTIMATOR_H__