#include "kobuki_estimator.h"

using namespace Eigen;

KobukiEstimator::KobukiEstimator() {
  world_T_base = MatrixXd::Identity(4, 4);
  world_T_kobuki = MatrixXd::Identity(4, 4);
  world_T_planter = MatrixXd::Identity(4, 4);
  base_T_kobuki = MatrixXd::Identity(4, 4);
  base_T_planter = MatrixXd::Identity(4, 4);
}

KobukiEstimator::~KobukiEstimator() {}

void KobukiEstimator::reset(const geometry_msgs::Pose &pose) {
  origin_x = pose.position.x;
  origin_y = pose.position.y;
  origin_z = pose.position.z;

  tf::Quaternion q(pose.orientation.x, pose.orientation.y, pose.orientation.z,
                   pose.orientation.w);
  tf::Matrix3x3 m(q);
  m.getRPY(origin_roll, origin_pitch, origin_yaw);

  world_T_base(0, 0) = m.getRow(0).getX();
  world_T_base(0, 1) = m.getRow(0).getY();
  world_T_base(0, 2) = m.getRow(0).getZ();
  world_T_base(1, 0) = m.getRow(1).getX();
  world_T_base(1, 1) = m.getRow(1).getY();
  world_T_base(1, 2) = m.getRow(1).getZ();
  world_T_base(2, 0) = m.getRow(2).getX();
  world_T_base(2, 1) = m.getRow(2).getY();
  world_T_base(2, 2) = m.getRow(2).getZ();
  world_T_base(0, 3) = origin_x;
  world_T_base(1, 3) = origin_y;
  world_T_base(2, 3) = origin_z;
}

void KobukiEstimator::set(const sensor_msgs::Imu &imu) {}

void KobukiEstimator::set(const geometry_msgs::Pose &pose) {
  kobuki_state_ = kobuki_state;

  kobuki_state.position.linear.x = pose.position.x - origin_x;
  kobuki_state.position.linear.y = pose.position.y - origin_y;
  // kobuki_state.position.linear.z = pose.position.z-origin_z;
  kobuki_state.position.linear.z = 0.0;

  tf::Quaternion q(pose.orientation.x, pose.orientation.y, pose.orientation.z,
                   pose.orientation.w);
  tf::Matrix3x3 m(q);
  world_T_kobuki(0, 0) = m.getRow(0).getX();
  world_T_kobuki(0, 1) = m.getRow(0).getY();
  world_T_kobuki(0, 2) = m.getRow(0).getZ();
  world_T_kobuki(1, 0) = m.getRow(1).getX();
  world_T_kobuki(1, 1) = m.getRow(1).getY();
  world_T_kobuki(1, 2) = m.getRow(1).getZ();
  world_T_kobuki(2, 0) = m.getRow(2).getX();
  world_T_kobuki(2, 1) = m.getRow(2).getY();
  world_T_kobuki(2, 2) = m.getRow(2).getZ();
  world_T_kobuki(0, 3) = pose.position.x;
  world_T_kobuki(1, 3) = pose.position.y;
  world_T_kobuki(2, 3) = pose.position.z;

  base_T_kobuki = world_T_base.inverse() * world_T_kobuki;
  mat3_t rot = base_T_kobuki.block(0, 0, 3, 3);
  Eigen::Vector3d euler = rot.eulerAngles(0, 1, 2);

  double roll = 0, pitch = 0, yaw = 0;
  m.getRPY(roll, pitch, yaw);
  kobuki_state.position.angular.x = roll - origin_roll;
  kobuki_state.position.angular.y = pitch - origin_pitch;
  kobuki_state.position.angular.z = yaw - origin_yaw;

  kobuki_state.velocity.linear.x = 0.0;
  kobuki_state.velocity.linear.y = 0.0;
  kobuki_state.velocity.linear.z = 0.0;
  kobuki_state.velocity.angular.x = 0.0;
  kobuki_state.velocity.angular.y = 0.0;
  kobuki_state.velocity.angular.z = 0.0;
}

tomato_msgs::KobukiState KobukiEstimator::get() { return kobuki_state; }