#include "manip_estimator.h"

ManipEstimator::ManipEstimator() {
  pub_angle[0] = nh.advertise<std_msgs::Float32>("/joint0/angle", 1);
  pub_angle[1] = nh.advertise<std_msgs::Float32>("/joint1/angle", 1);
  pub_angle[2] = nh.advertise<std_msgs::Float32>("/joint2/angle", 1);
  pub_angle[3] = nh.advertise<std_msgs::Float32>("/joint3/angle", 1);
  pub_angle[4] = nh.advertise<std_msgs::Float32>("/joint4/angle", 1);
  pub_angle[5] = nh.advertise<std_msgs::Float32>("/joint5/angle", 1);
  pub_angle[6] = nh.advertise<std_msgs::Float32>("/joint6/angle", 1);
  pub_angle[7] = nh.advertise<std_msgs::Float32>("/joint7/angle", 1);

  for (int i = 0; i < NUMBER_OF_JOINT; i++) {
    std_msgs::Float32 pos;
    pos.data = 0.0;
    pub_angle[i].publish(pos);

    qold[i] = 0.0;
    qlst[i] = 0.0;
  }

  pub_torque[0] = nh.advertise<std_msgs::Float32>("/joint0/torque", 1);
  pub_torque[1] = nh.advertise<std_msgs::Float32>("/joint1/torque", 1);
  pub_torque[2] = nh.advertise<std_msgs::Float32>("/joint2/torque", 1);
  pub_torque[3] = nh.advertise<std_msgs::Float32>("/joint3/torque", 1);
  pub_torque[4] = nh.advertise<std_msgs::Float32>("/joint4/torque", 1);
  pub_torque[5] = nh.advertise<std_msgs::Float32>("/joint5/torque", 1);
  pub_torque[6] = nh.advertise<std_msgs::Float32>("/joint6/torque", 1);
  pub_torque[7] = nh.advertise<std_msgs::Float32>("/joint7/torque", 1);

  for (int i = 0; i < NUMBER_OF_JOINT; i++) {
    std_msgs::Float32 tau;
    tau.data = 0.0;
    pub_torque[i].publish(tau);
  }

  manip_state.joint_position.resize(NUMBER_OF_JOINT + 4);
}

ManipEstimator::~ManipEstimator() {}

void ManipEstimator::set(
    const dynamixel_msgs::MotorStateList::ConstPtr &motor_) {

  dynamixel_msgs::MotorStateList motor = *motor_;

  // if(motor.motor_states[0].position<JOINT0_MIN_VAL)
  //   motor.motor_states[0].position=JOINT0_MIN_VAL;
  // if(motor.motor_states[0].position>JOINT0_MAX_VAL)
  //   motor.motor_states[0].position=JOINT0_MAX_VAL;
  // if(motor.motor_states[1].position<JOINT1_MIN_VAL)
  //   motor.motor_states[1].position=JOINT1_MIN_VAL;
  // if(motor.motor_states[1].position>JOINT1_MAX_VAL)
  //   motor.motor_states[1].position=JOINT1_MAX_VAL;
  // if(motor.motor_states[2].position<JOINT2_MIN_VAL)
  //   motor.motor_states[2].position=JOINT2_MIN_VAL;
  // if(motor.motor_states[2].position>JOINT2_MAX_VAL)
  //   motor.motor_states[2].position=JOINT2_MAX_VAL;
  // if(motor.motor_states[3].position<JOINT3_MIN_VAL)
  //   motor.motor_states[3].position=JOINT3_MIN_VAL;
  // if(motor.motor_states[3].position>JOINT3_MAX_VAL)
  //   motor.motor_states[3].position=JOINT3_MAX_VAL;
  // if(motor.motor_states[4].position<JOINT4_MIN_VAL)
  //   motor.motor_states[4].position=JOINT4_MIN_VAL;
  // if(motor.motor_states[4].position>JOINT4_MAX_VAL)
  //   motor.motor_states[4].position=JOINT4_MAX_VAL;
  // if(motor.motor_states[5].position<JOINT5_MIN_VAL)
  //   motor.motor_states[5].position=JOINT5_MIN_VAL;
  // if(motor.motor_states[5].position>JOINT5_MAX_VAL)
  //   motor.motor_states[5].position=JOINT5_MAX_VAL;
  // if(motor.motor_states[6].position<JOINT6_MIN_VAL)
  //   motor.motor_states[6].position=JOINT6_MIN_VAL;
  // if(motor.motor_states[6].position>JOINT6_MAX_VAL)
  //   motor.motor_states[6].position=JOINT6_MAX_VAL;
  // if(motor.motor_states[7].position<JOINT7_MIN_VAL)
  //   motor.motor_states[7].position=JOINT7_MIN_VAL;
  // if(motor.motor_states[7].position>JOINT7_MAX_VAL)
  //   motor.motor_states[7].position=JOINT7_MAX_VAL;

  manip_state.joint_position[0] =
      JOINT0_DIRECTION *
      ((double)(motor.motor_states[0].position) - JOINT0_MIDDLE_VAL) *
      MX64AR_COUNT_TO_DEG * DEG_TO_LINEAR;
  manip_state.joint_position[1] =
      JOINT1_DIRECTION *
      ((double)(motor.motor_states[1].position) - JOINT1_MIDDLE_VAL) *
      MX28AR_COUNT_TO_DEG * DEG_TO_RAD;
  manip_state.joint_position[2] =
      JOINT2_DIRECTION *
      ((double)(motor.motor_states[2].position) - JOINT2_MIDDLE_VAL) *
      MX64AR_COUNT_TO_DEG * DEG_TO_RAD;
  manip_state.joint_position[3] =
      JOINT3_DIRECTION *
      ((double)(motor.motor_states[3].position) - JOINT3_MIDDLE_VAL) *
      MX64AR_COUNT_TO_DEG * DEG_TO_RAD;
  manip_state.joint_position[4] =
      JOINT4_DIRECTION *
      ((double)(motor.motor_states[4].position) - JOINT4_MIDDLE_VAL) *
      MX28AR_COUNT_TO_DEG * DEG_TO_RAD;
  manip_state.joint_position[5] =
      JOINT5_DIRECTION *
      ((double)(motor.motor_states[5].position) - JOINT5_MIDDLE_VAL) *
      MX28AR_COUNT_TO_DEG * DEG_TO_RAD;
  manip_state.joint_position[6] =
      JOINT6_DIRECTION *
      ((double)(motor.motor_states[6].position) - JOINT6_MIDDLE_VAL) *
      AX12A_COUNT_TO_DEG * DEG_TO_RAD;
  manip_state.joint_position[7] = +manip_state.joint_position[6];
  manip_state.joint_position[8] = -manip_state.joint_position[6];
  manip_state.joint_position[9] =
      JOINT7_DIRECTION *
      ((double)(motor.motor_states[7].position) - JOINT7_MIDDLE_VAL) *
      AX12A_COUNT_TO_DEG * DEG_TO_RAD;
  manip_state.joint_position[10] = +manip_state.joint_position[9];
  manip_state.joint_position[11] = -manip_state.joint_position[9];

  // display current joint angle
  for (int i = 0; i < NUMBER_OF_JOINT; i++) {
    std_msgs::Float32 pos;
    pos.data = manip_state.joint_position[i];
    if (isCorrect(pos.data, i))
      pub_angle[i].publish(pos);
  }

  // display current joint torque
  // ratio of applied torque over maximum torque
  for (int i = 0; i < NUMBER_OF_JOINT; i++) {
    std_msgs::Float32 tau;
    tau.data = motor.motor_states[i].load;
    pub_torque[i].publish(tau);
  }

  std::cout << (motor.motor_states[2].position) << '\n';
}

tomato_msgs::ManipState ManipEstimator::get() { return manip_state; }

bool ManipEstimator::isCorrect(double pos, int id) {
  bool flag = false;

  if ((fabs(pos - qold[id]) <= THRESHOLD) &&
      (fabs(qold[id] - qlst[id]) <= THRESHOLD))
    flag = true;

  qlst[id] = qold[id];
  qold[id] = pos;

  return flag;
}
