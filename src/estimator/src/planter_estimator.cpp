#include "planter_estimator.h"

PlanterEstimator::PlanterEstimator() {}

PlanterEstimator::~PlanterEstimator() {}

void PlanterEstimator::set(const geometry_msgs::Pose &pose) {
  planter_state.position.x = pose.position.x;
  planter_state.position.y = pose.position.y;
  // planter_state.position.z = pose.position.z;
  planter_state.position.z = 0.0;

  //消す予定(テスト用)
  planter_state.circle_radius = 1;
}

tomato_msgs::PlanterState PlanterEstimator::get() { return planter_state; }