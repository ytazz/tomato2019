#include "state_estimator.h"

StateEstimator::StateEstimator() {
  // sub_motor = nh.subscribe<dynamixel_msgs::MotorStateList>(
  //     "/motor_states/pan_tilt_port", 10, &StateEstimator::motorCallback,
  //     this);
  sub_joint0 = nh.subscribe<dynamixel_msgs::JointState>(
      "/tilt0_controller/state", 5, &StateEstimator::joint0Callback, this);
  sub_joint1 = nh.subscribe<dynamixel_msgs::JointState>(
      "/tilt1_controller/state", 5, &StateEstimator::joint1Callback, this);
  sub_joint2 = nh.subscribe<dynamixel_msgs::JointState>(
      "/tilt2_controller/state", 5, &StateEstimator::joint2Callback, this);
  sub_joint3 = nh.subscribe<dynamixel_msgs::JointState>(
      "/tilt3_controller/state", 5, &StateEstimator::joint3Callback, this);
  sub_joint4 = nh.subscribe<dynamixel_msgs::JointState>(
      "/tilt4_controller/state", 5, &StateEstimator::joint4Callback, this);
  sub_joint5 = nh.subscribe<dynamixel_msgs::JointState>(
      "/tilt5_controller/state", 5, &StateEstimator::joint5Callback, this);
  sub_joint6 = nh.subscribe<dynamixel_msgs::JointState>(
      "/tilt6_controller/state", 5, &StateEstimator::joint6Callback, this);
  sub_joint7 = nh.subscribe<dynamixel_msgs::JointState>(
      "/tilt7_controller/state", 5, &StateEstimator::joint7Callback, this);

  sub_mocap_origin = nh.subscribe<geometry_msgs::PoseStamped>(
      "/mocap_node/reset", 10, &StateEstimator::mocapOriginCallback, this);
  sub_mocap_planter = nh.subscribe<geometry_msgs::PoseStamped>(
      "/mocap_node/Planter", 10, &StateEstimator::mocapPlanterCallback, this);
  sub_mocap_kobuki = nh.subscribe<geometry_msgs::PoseStamped>(
      "/mocap_node/Kobuki", 10, &StateEstimator::mocapKobukiCallback, this);

  pub_planter =
      nh.advertise<tomato_msgs::PlanterState>("/planter/pose/current", 1);
  pub_kobuki =
      nh.advertise<tomato_msgs::KobukiState>("/kobuki/pose/current", 1);
  pub_manip = nh.advertise<tomato_msgs::ManipState>("/manip/pose/current", 1);

  manip_state.joint_position.resize(NUMBER_OF_JOINT + 4);

  count = 0;
}

StateEstimator::~StateEstimator() {}

// void StateEstimator::motorCallback(
//     const dynamixel_msgs::MotorStateList::ConstPtr &state) {
//   manip_estimator.set(state);
//   pub_manip.publish(manip_estimator.get());
// }

void StateEstimator::joint0Callback(
    const dynamixel_msgs::JointState::ConstPtr &state) {
  double diff = state->current_pos - manip_state.joint_position[0];
  // if (diff > 1)
  //   count++;
  // if (diff < -1)
  //   count--;
  // printf("%1.3lf,\t%d\n", diff, count);

  manip_state.joint_position[0] = (double)count * BALL_DIST / BALL_COUNT;
  pub_manip.publish(manip_state);
}

void StateEstimator::joint1Callback(
    const dynamixel_msgs::JointState::ConstPtr &state) {
double diff = state->current_pos - manip_state.joint_position[1];
if(fabs(diff)<1){
  manip_state.joint_position[1] = state->current_pos;
}
// std::cout << "joint1: "<<manip_state.joint_position[1] <<diff<< '\n';

  pub_manip.publish(manip_state);
}

void StateEstimator::joint2Callback(
    const dynamixel_msgs::JointState::ConstPtr &state) {
double diff = state->current_pos - manip_state.joint_position[2];
if(fabs(diff)<1){
  manip_state.joint_position[2] = state->current_pos;
}

  pub_manip.publish(manip_state);
}

void StateEstimator::joint3Callback(
    const dynamixel_msgs::JointState::ConstPtr &state) {
double diff = state->current_pos - manip_state.joint_position[3];
if(fabs(diff)<1){
  manip_state.joint_position[3] = state->current_pos;
}

  pub_manip.publish(manip_state);
}

void StateEstimator::joint4Callback(
    const dynamixel_msgs::JointState::ConstPtr &state) {
double diff = state->current_pos - manip_state.joint_position[4];
if(fabs(diff)<1){
  manip_state.joint_position[4] = state->current_pos;
}

  pub_manip.publish(manip_state);
}

void StateEstimator::joint5Callback(
    const dynamixel_msgs::JointState::ConstPtr &state) {
double diff = state->current_pos - manip_state.joint_position[5];
if(fabs(diff)<1){
  manip_state.joint_position[5] = state->current_pos;
}

  pub_manip.publish(manip_state);
}

void StateEstimator::joint6Callback(
    const dynamixel_msgs::JointState::ConstPtr &state) {
double diff = state->current_pos - manip_state.joint_position[6];
if(fabs(diff)<1){
  manip_state.joint_position[6] = state->current_pos;

  manip_state.joint_position[7] = +manip_state.joint_position[6];
  manip_state.joint_position[8] = -manip_state.joint_position[6];
}
  pub_manip.publish(manip_state);
}

void StateEstimator::joint7Callback(
    const dynamixel_msgs::JointState::ConstPtr &state) {
double diff = state->current_pos - manip_state.joint_position[9];
if(fabs(diff)<1){
  manip_state.joint_position[9] = state->current_pos;

  manip_state.joint_position[10] = +manip_state.joint_position[9];
  manip_state.joint_position[11] = -manip_state.joint_position[9];
}
  pub_manip.publish(manip_state);
}

void StateEstimator::mocapOriginCallback(
    const geometry_msgs::PoseStamped::ConstPtr &stamp) {
  kobuki_estimator.reset(stamp->pose);
}

void StateEstimator::mocapKobukiCallback(
    const geometry_msgs::PoseStamped::ConstPtr &stamp) {
  kobuki_estimator.set(stamp->pose);
  pub_kobuki.publish(kobuki_estimator.get());
}

void StateEstimator::mocapPlanterCallback(
    const geometry_msgs::PoseStamped::ConstPtr &stamp) {
  planter_estimator.set(stamp->pose);
  pub_planter.publish(planter_estimator.get());
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "state_estimator");
  StateEstimator state_estimator;

  ros::spin();
}