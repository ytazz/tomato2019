/*
    author: H. Kuribayashi

    main controller
*/

#ifndef __MAIN_CONTROLLER_H__
#define __MAIN_CONTROLLER_H__

// ヘッダファイルの読み込み
// C言語系
#include <cmath>
#include <string>
// ROS系
#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <std_msgs/Float32.h>
#include <std_msgs/UInt8.h>
// 自分で定義したメッセージ型
#include "joy_setting.h"             //
#include <tomato_msgs/KobukiPhase.h> //
#include <tomato_msgs/KobukiState.h> // Kobuki軌道計画用
#include <tomato_msgs/KobukiValue.h> // state service
#include <tomato_msgs/Phase.h>       //

#define LINEAR_VEL_MAX 0.2
#define LINEAR_ACCEL 0.001
#define ANGULAR_VEL_MAX 1.0
#define ANGULAR_ACCEL 0.001

class MainController {
public:
  MainController();
  ~MainController();

  void timerCallback(const ros::TimerEvent &);
  void joyCallback(const sensor_msgs::Joy::ConstPtr &joy);
  void phaseCallback(const std_msgs::UInt8::ConstPtr &phase);

  void pubText();

  bool pubKobukiPhase();
  bool pubManipPhase();

private:
  ros::NodeHandle nh;
  ros::Timer timer;
  ros::Publisher pub_text, pub_menu, pub_kobuki;
  ros::Subscriber sub_joy, sub_phase;
  ros::ServiceClient cln_phase;

  std_msgs::UInt8 phase;
  tomato_msgs::KobukiPhase kobuki_phase;

  geometry_msgs::Twist vel;

  const double dt;
};

#endif // __MAIN_CONTROLLER_H__