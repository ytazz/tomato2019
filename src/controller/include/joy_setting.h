/*
    author: H. Kuribayashi

    joyの割り振り
*/

#ifndef __JOY_SETTING_H__
#define __JOY_SETTING_H__

#define KOBUKI_LINEAR_AXES 3
#define KOBUKI_ANGULAR_AXES_CW 6
#define KOBUKI_ANGULAR_AXES_CCW 7

#endif // __JOY_SETTING_H__