//--- Author: Hiroki Tsuji ---//

#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <std_msgs/Float64.h>
#include <geometry_msgs/Twist.h>
#include <dynamixel_msgs/JointState.h>
#include <std_msgs/UInt8.h>

int mode = 0;
double joint[8] = {0.0};

void PhaseCallback(const std_msgs::UInt8 phase)
{
  mode = phase.data;
}

void joyCallback(const sensor_msgs::Joy::ConstPtr &joy)
{
  if(mode == 8){
    joint[0]  = 3.0 * joy->axes[5];

    joint[1] += 0.002 * joy->axes[4];
    if(joint[1] > 1.352) joint[1] = 1.352;
    else if(joint[1] < -1.476) joint[1] = -1.476;

    joint[2] += 0.002 * (joy->buttons[0] - joy->buttons[1]);
    if(joint[2] > 1.354) joint[2] = 1.354;
    else if(joint[2] < 0.0) joint[2] = 0.0;

    joint[3] += 0.002 * (joy->buttons[2] - joy->buttons[3]);
    if(joint[3] > 0.0) joint[3] = 0.0;
    else if(joint[3] < -2.682) joint[3] = -2.682;

    joint[4] += 0.002 * joy->axes[1];
    if(joint[4] > 1.594) joint[4] = 1.594;
    else if(joint[4] < -1.580) joint[4] = -1.580;

    joint[5] += 0.002 * joy->axes[3];
    if(joint[5] > 1.400) joint[5] = 1.400;
    else if(joint[5] < -0.130) joint[5] = -0.130;

    joint[6] += 0.005 * (joy->buttons[4] - joy->buttons[5]);
    if(joint[6] > 0.0) joint[6] = 0.0;
    else if(joint[6] < -1.0) joint[6] = -1.0;

    joint[7] += 0.005 * (joy->buttons[6] - joy->buttons[7]);
    if(joint[7] > 1.0) joint[7] = 1.0;
    else if(joint[7] < 0.0) joint[7] = 0.0;
  }
}

int main(int argc, char **argv){
  ros::init(argc, argv, "axes_control");
  ros::NodeHandle n;

  ros::Subscriber phase_sub = n.subscribe<std_msgs::UInt8>("control/phase", 1, &PhaseCallback);
  ros::Subscriber joy_sub = n.subscribe<sensor_msgs::Joy>("joy", 1, &joyCallback);

  ros::Publisher joint0_pub = n.advertise<std_msgs::Float64>("/tilt0_controller/command", 1);
  ros::Publisher joint1_pub = n.advertise<std_msgs::Float64>("/tilt1_controller/command", 1);
  ros::Publisher joint2_pub = n.advertise<std_msgs::Float64>("/tilt2_controller/command", 1);
  ros::Publisher joint3_pub = n.advertise<std_msgs::Float64>("/tilt3_controller/command", 1);
  ros::Publisher joint4_pub = n.advertise<std_msgs::Float64>("/tilt4_controller/command", 1);
  ros::Publisher joint5_pub = n.advertise<std_msgs::Float64>("/tilt5_controller/command", 1);
  ros::Publisher joint6_pub = n.advertise<std_msgs::Float64>("/tilt6_controller/command", 1);
  ros::Publisher joint7_pub = n.advertise<std_msgs::Float64>("/tilt7_controller/command", 1);

  std_msgs::Float64 joint0;
  std_msgs::Float64 joint1;
  std_msgs::Float64 joint2;
  std_msgs::Float64 joint3;
  std_msgs::Float64 joint4;
  std_msgs::Float64 joint5;
  std_msgs::Float64 joint6;
  std_msgs::Float64 joint7;

  joint0.data = 0.0;
  joint1.data = 0.0;
  joint2.data = 0.0;
  joint3.data = 0.0;
  joint4.data = 0.0;
  joint5.data = 0.0;
  joint6.data = 0.0;
  joint7.data = 0.0;

  ros::Rate loop_rate(50);

  while(ros::ok()){
    joint0.data = joint[0];
    joint1.data = joint[1];
    joint2.data = joint[2];
    joint3.data = joint[3];
    joint4.data = joint[4];
    joint5.data = joint[5];
    joint6.data = joint[6];
    joint7.data = joint[7];

    joint0_pub.publish(joint0);
    joint1_pub.publish(joint1);
    joint2_pub.publish(joint2);
    joint3_pub.publish(joint3);
    joint4_pub.publish(joint4);
    joint5_pub.publish(joint5);
    joint6_pub.publish(joint6);
    joint7_pub.publish(joint7);

    ros::spinOnce();
    loop_rate.sleep();
  }
}




