/*
    author: H. Kuribayashi

    main controller
*/

#include "main_controller.h"

MainController::MainController() : dt(0.05) {
  // timer =
  //     nh.createTimer(ros::Duration(dt), &MainController::timerCallback,
  //     this);
  sub_joy = nh.subscribe<sensor_msgs::Joy>("/joy", 10,
                                           &MainController::joyCallback, this);
  sub_phase = nh.subscribe<std_msgs::UInt8>(
      "/control/phase", 10, &MainController::phaseCallback, this);

  pub_kobuki =
      nh.advertise<geometry_msgs::Twist>("/mobile_base/commands/velocity", 1);

  cln_phase = nh.serviceClient<tomato_msgs::KobukiPhase>("/kobuki/phase");

  vel.linear.x = 0.0;
  vel.linear.y = 0.0;
  vel.linear.z = 0.0;
  vel.angular.x = 0.0;
  vel.angular.y = 0.0;
  vel.angular.z = 0.0;
}

MainController::~MainController() {}

void MainController::phaseCallback(const std_msgs::UInt8::ConstPtr &phase) {
  this->phase = *phase;
}

void MainController::joyCallback(const sensor_msgs::Joy::ConstPtr &joy) {
  geometry_msgs::Twist vel_ref;

  switch (this->phase.data) {
  case tomato_msgs::Phase::REMOTE_KOBUKI:
    vel_ref.linear.x = LINEAR_VEL_MAX * joy->axes[KOBUKI_LINEAR_AXES];
    vel_ref.angular.z =
        ANGULAR_VEL_MAX * (joy->buttons[KOBUKI_ANGULAR_AXES_CW]) -
        ANGULAR_VEL_MAX * (joy->buttons[KOBUKI_ANGULAR_AXES_CCW]);

    if (vel_ref.linear.x > vel.linear.x)
      vel.linear.x += LINEAR_ACCEL;
    else if (vel_ref.linear.x < vel.linear.x)
      vel.linear.x -= LINEAR_ACCEL;

    if (fabs(vel_ref.linear.x) <= 0.01)
      vel.linear.x = 0.0;

    if (vel_ref.angular.z > vel.angular.z)
      vel.angular.z += ANGULAR_ACCEL;
    else if (vel_ref.angular.z < vel.angular.z)
      vel.angular.z -= ANGULAR_ACCEL;

    if (fabs(vel_ref.angular.z) <= 0.01)
      vel.angular.z = 0.0;

    // printf("linear  : %lf\n", vel.linear.x);
    // printf("angular : %lf\n", vel.angular.z);
    pub_kobuki.publish(vel);
    break;
  case tomato_msgs::Phase::REMOTE_MANIP_AXIS:
    break;
  case tomato_msgs::Phase::REMOTE_MANIP_IK:
    break;
  default:
    break;
  }
}

bool MainController::pubKobukiPhase() {
  // kobuki_phase.request.phase = tomato_msgs::Phase::READY;
  // cln_phase.call(kobuki_phase);

  // geometry_msgs::Twist vel;
  // vel.linear.x = 0.0;
  // vel.linear.y = 0.0;
  // vel.linear.z = 0.0;
  // vel.angular.x = 0.0;
  // vel.angular.y = 0.0;
  // vel.angular.z = 0.0;
  //
  // kobuki_phase.request.phase = tomato_msgs::Phase::REMOTE;
  // kobuki_phase.request.velocity = vel;
  // cln_phase.call(kobuki_phase);
}

bool MainController::pubManipPhase() {}

void MainController::timerCallback(const ros::TimerEvent &) {
  // pubKobukiPhase();
  // pubManipPhase();
}

// main関数
// 定義したクラスのオブジェクト（ノード）を生成
int main(int argc, char **argv) {
  ros::init(argc, argv, "main_controller");
  MainController main_controller;
  ros::spin();
}