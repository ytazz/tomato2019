#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64
from dynamixel_msgs.msg import JointState
from std_msgs.msg import UInt8


class ControllerClass(object):
    def __init__(self):
        self.joy_sub = rospy.Subscriber('joy', Joy, self.JoyCallback)
	self.phase_sub = rospy.Subscriber('control/phase', UInt8, self.PhaseCallback)
        self.tilt1_pub = rospy.Publisher('/tilt1_controller/command', Float64, queue_size=1)
        self.tilt2_pub = rospy.Publisher('/tilt2_controller/command', Float64, queue_size=1)
        self.tilt3_pub = rospy.Publisher('/tilt3_controller/command', Float64, queue_size=1)
        self.tilt4_pub = rospy.Publisher('/tilt4_controller/command', Float64, queue_size=1)
        self.tilt5_pub = rospy.Publisher('/tilt5_controller/command', Float64, queue_size=1)
        self.tilt6_pub = rospy.Publisher('/tilt6_controller/command', Float64, queue_size=1)
        self.tilt7_pub = rospy.Publisher('/tilt7_controller/command', Float64, queue_size=1)

	self.mode = UInt8()
        self.tilt1_pos = Float64()
        self.tilt2_pos = Float64()
        self.tilt3_pos = Float64()
        self.tilt4_pos = Float64()
        self.tilt5_pos = Float64()
        self.tilt6_pos = Float64()
        self.tilt7_pos = Float64()

	self.mode = 0
        self.tilt1_pos = 0.0
        self.tilt2_pos = 0.0
        self.tilt3_pos = 0.0
        self.tilt4_pos = 0.0
        self.tilt5_pos = 0.0
        self.tilt6_pos = 0.0
        self.tilt7_pos = 0.0

    def PhaseCallback(self, phase):
	self.mode = phase.data

    def JoyCallback(self, data):
	if self.mode == 8 :
        	self.tilt1_pos += 0.002 * data.axes[4]
        	self.tilt2_pos += 0.002 * (data.buttons[0] - data.buttons[1])
        	self.tilt3_pos += 0.002 * (data.buttons[2] - data.buttons[3])
        	self.tilt4_pos += 0.002 * data.axes[1]
        	self.tilt5_pos += 0.002 * data.axes[3]
        	self.tilt6_pos += 0.002 * (data.buttons[4] - data.buttons[5])
        	self.tilt7_pos += 0.002 * (data.buttons[6] - data.buttons[7])

    def tilt1_publish(self):
        self.tilt1_pub.publish(self.tilt1_pos)

    def tilt2_publish(self):
        self.tilt2_pub.publish(self.tilt2_pos)

    def tilt3_publish(self):
        self.tilt3_pub.publish(self.tilt3_pos)

    def tilt4_publish(self):
        self.tilt4_pub.publish(self.tilt4_pos)

    def tilt5_publish(self):
        self.tilt5_pub.publish(self.tilt5_pos)

    def tilt6_publish(self):
        self.tilt6_pub.publish(self.tilt6_pos)

    def tilt7_publish(self):
        self.tilt7_pub.publish(self.tilt7_pos)


if __name__ == '__main__':
    rospy.init_node('axes_control', anonymous=True)
    controller_class = ControllerClass()
    rate = rospy.Rate(50)
    while not rospy.is_shutdown():
        controller_class.tilt1_publish()
        controller_class.tilt2_publish()
        controller_class.tilt3_publish()
        controller_class.tilt4_publish()
        controller_class.tilt5_publish()
        controller_class.tilt6_publish()
        controller_class.tilt7_publish()
        rate.sleep()
