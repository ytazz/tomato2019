#ifndef __CONTROL_PANEL_H__
#define __CONTROL_PANEL_H__

#ifndef Q_MOC_RUN
#include <ros/ros.h>
#include <rviz/panel.h>
#endif

#include <QButtonGroup>
#include <QCheckBox>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMouseEvent>
#include <QPainter>
#include <QRadioButton>
#include <QSizePolicy>
#include <QTimer>
#include <QToolButton>
#include <QVBoxLayout>
#include <QWidget>
#include <iostream>
#include <sstream>
#include <string>

#include <std_msgs/UInt8.h>
#include <std_msgs/Empty.h>
#include <tomato_msgs/Device.h>
#include <tomato_msgs/Phase.h>

namespace control_plugin {

class ControlPanel : public rviz::Panel {
  Q_OBJECT
public:
  ControlPanel(QWidget *parent = 0);
  ~ControlPanel();

  virtual void load(const rviz::Config &config);
  virtual void save(rviz::Config config) const;

public Q_SLOTS:
  void clickedWait();
  void clickedMapping();
  void clickedReady();
  void clickedCircle();
  void clickedApproach();
  void clickedHarvest();
  void clickedRemote();
  void clickedKobuki();
  void clickedManipAxis();
  void clickedManipIK();

  void clickedDeviceJoy();
  void clickedDeviceMarker();

public:
  void buttonEnabled(QToolButton *button, bool flag);
  void buttonSelected(QToolButton *button, bool flag);
  void buttonSelected(QToolButton *button, bool flag, std::string color);

  ros::NodeHandle nh;
  ros::Publisher pub_phase, pub_device,pub_time;
  ros::Subscriber sub_phase;

  QLabel *title_label[3];

  QLabel *phase_label;
  QToolButton *phase_button[10];

  QLabel *device_label;
  QToolButton *device_button[2];
};

} // control_plugin

#endif // __MAP_PARAM_H__