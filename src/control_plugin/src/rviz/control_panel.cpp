#include "control_panel.h"

#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>

namespace control_plugin {
ControlPanel::ControlPanel(QWidget *parent) : rviz::Panel(parent) {
  QGridLayout *layout = new QGridLayout;

  // Current Phase Pane
  title_label[0] = new QLabel;
  title_label[0]->setText(tr("Current Phase"));
  phase_label = new QLabel;
  phase_label->setText(tr("None"));
  phase_label->setFixedSize(150, 50);
  layout->addWidget(title_label[0], 0, 0, Qt::AlignTop);
  layout->addWidget(phase_label, 0, 1, Qt::AlignLeft);
  phase_label->setStyleSheet(
      "color:black;"
      "border-color:black; border-style:solid; border-width:2px;");

  // Phase Select Pane
  title_label[1] = new QLabel;
  title_label[1]->setText(tr("Phase"));
  layout->addWidget(title_label[1], 1, 0, Qt::AlignLeft);

  for (int i = 0; i < 10; i++) {
    phase_button[i] = new QToolButton;
    phase_button[i]->setFixedSize(100, 50);
    if (i < 5)
      layout->addWidget(phase_button[i], i + 2, 0, Qt::AlignCenter);
    else
      layout->addWidget(phase_button[i], i + 2 - 5, 1, Qt::AlignCenter);
  }
  phase_button[tomato_msgs::Phase::WAIT]->setText("Wait");
  phase_button[tomato_msgs::Phase::MAPPING]->setText("Mapping");
  phase_button[tomato_msgs::Phase::READY]->setText("Ready");
  phase_button[tomato_msgs::Phase::CIRCLE]->setText("Circle");
  phase_button[tomato_msgs::Phase::APPROACH]->setText("Approach");
  phase_button[tomato_msgs::Phase::HARVEST]->setText("Harvest");
  phase_button[tomato_msgs::Phase::REMOTE]->setText("Remote");
  phase_button[tomato_msgs::Phase::REMOTE_KOBUKI]->setText("Kobuki");
  phase_button[tomato_msgs::Phase::REMOTE_MANIP_AXIS]->setText("Manip Axis");
  phase_button[tomato_msgs::Phase::REMOTE_MANIP_IK]->setText("Manip IK");

  // Device Select Pane
  // title_label[2] = new QLabel;
  // title_label[2]->setText(tr("Device"));
  // layout->addWidget(title_label[2], 7, 0, Qt::AlignTop);
  //
  // for (int i = 0; i < 2; i++) {
  //   device_button[i] = new QToolButton;
  //   device_button[i]->setFixedSize(100, 50);
  // }
  // layout->addWidget(device_button[0], 8, 0, Qt::AlignCenter);
  // layout->addWidget(device_button[1], 8, 1, Qt::AlignCenter);
  // device_button[tomato_msgs::Device::JOY]->setText("Joy");
  // device_button[tomato_msgs::Device::MARKER]->setText("Marker");

  // for Stretch
  QVBoxLayout *stretch = new QVBoxLayout;
  stretch->addStretch();
  layout->addLayout(stretch, 7, 0);
  setLayout(layout);

  connect(phase_button[tomato_msgs::Phase::WAIT], SIGNAL(clicked()), this,
          SLOT(clickedWait()));
  connect(phase_button[tomato_msgs::Phase::MAPPING], SIGNAL(clicked()), this,
          SLOT(clickedMapping()));
  connect(phase_button[tomato_msgs::Phase::READY], SIGNAL(clicked()), this,
          SLOT(clickedReady()));
  connect(phase_button[tomato_msgs::Phase::CIRCLE], SIGNAL(clicked()), this,
          SLOT(clickedCircle()));
  connect(phase_button[tomato_msgs::Phase::APPROACH], SIGNAL(clicked()), this,
          SLOT(clickedApproach()));
  connect(phase_button[tomato_msgs::Phase::HARVEST], SIGNAL(clicked()), this,
          SLOT(clickedHarvest()));
  connect(phase_button[tomato_msgs::Phase::REMOTE], SIGNAL(clicked()), this,
          SLOT(clickedRemote()));
  connect(phase_button[tomato_msgs::Phase::REMOTE_KOBUKI], SIGNAL(clicked()),
          this, SLOT(clickedKobuki()));
  connect(phase_button[tomato_msgs::Phase::REMOTE_MANIP_AXIS],
          SIGNAL(clicked()), this, SLOT(clickedManipAxis()));
  connect(phase_button[tomato_msgs::Phase::REMOTE_MANIP_IK], SIGNAL(clicked()),
          this, SLOT(clickedManipIK()));

  // connect(device_button[tomato_msgs::Device::JOY], SIGNAL(clicked()), this,
  //         SLOT(clickedDeviceJoy()));
  // connect(device_button[tomato_msgs::Device::MARKER], SIGNAL(clicked()),
  // this,
  //         SLOT(clickedDeviceMarker()));

  buttonEnabled(phase_button[tomato_msgs::Phase::MAPPING], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::READY], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::CIRCLE], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::APPROACH], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::HARVEST], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::REMOTE_KOBUKI], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::REMOTE_MANIP_AXIS], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::REMOTE_MANIP_IK], false);
  // buttonEnabled(device_button[tomato_msgs::Device::JOY], false);
  // buttonEnabled(device_button[tomato_msgs::Device::MARKER], false);

  pub_phase = nh.advertise<std_msgs::UInt8>("/control/phase", 1);
  // pub_device = nh.advertise<std_msgs::UInt8>("/control/device", 1);
  pub_time  = nh.advertise<std_msgs::Empty>("/timer/reset", 1);
}

ControlPanel::~ControlPanel() {}

void ControlPanel::clickedWait() {
  phase_label->setText(tr("Wait"));
  buttonSelected(phase_button[tomato_msgs::Phase::REMOTE], false);
  buttonSelected(phase_button[tomato_msgs::Phase::REMOTE_KOBUKI], false);
  buttonSelected(phase_button[tomato_msgs::Phase::REMOTE_MANIP_AXIS], false);
  buttonSelected(phase_button[tomato_msgs::Phase::REMOTE_MANIP_IK], false);

  buttonEnabled(phase_button[tomato_msgs::Phase::MAPPING], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::READY], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::CIRCLE], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::APPROACH], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::HARVEST], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::REMOTE], true);
  buttonEnabled(phase_button[tomato_msgs::Phase::REMOTE_KOBUKI], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::REMOTE_MANIP_AXIS], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::REMOTE_MANIP_IK], false);

  std_msgs::Empty empty;
  pub_time.publish(empty);

  // buttonSelected(device_button[tomato_msgs::Device::JOY], false);
  // buttonSelected(device_button[tomato_msgs::Device::MARKER], false);
  //
  // buttonEnabled(device_button[tomato_msgs::Device::JOY], false);
  // buttonEnabled(device_button[tomato_msgs::Device::MARKER], false);
}

void ControlPanel::clickedMapping() { phase_label->setText(tr("Mapping")); }

void ControlPanel::clickedReady() { phase_label->setText(tr("Ready")); }

void ControlPanel::clickedCircle() { phase_label->setText(tr("Circle")); }

void ControlPanel::clickedApproach() { phase_label->setText(tr("Approach")); }

void ControlPanel::clickedHarvest() { phase_label->setText(tr("Harvest")); }

void ControlPanel::clickedRemote() {
  phase_label->setText(tr("Remote"));
  buttonSelected(phase_button[tomato_msgs::Phase::REMOTE], true);

  buttonEnabled(phase_button[tomato_msgs::Phase::MAPPING], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::READY], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::CIRCLE], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::APPROACH], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::HARVEST], false);
  buttonEnabled(phase_button[tomato_msgs::Phase::REMOTE_KOBUKI], true);
  buttonEnabled(phase_button[tomato_msgs::Phase::REMOTE_MANIP_AXIS], true);
  buttonEnabled(phase_button[tomato_msgs::Phase::REMOTE_MANIP_IK], true);
  // buttonEnabled(device_button[tomato_msgs::Device::JOY], true);
  // buttonEnabled(device_button[tomato_msgs::Device::MARKER], true);

  buttonSelected(phase_button[tomato_msgs::Phase::REMOTE_KOBUKI], false);
  buttonSelected(phase_button[tomato_msgs::Phase::REMOTE_MANIP_AXIS], false);
  buttonSelected(phase_button[tomato_msgs::Phase::REMOTE_MANIP_IK], false);
  // buttonSelected(device_button[tomato_msgs::Device::JOY], false);
  // buttonSelected(device_button[tomato_msgs::Device::MARKER], false);

  std_msgs::UInt8 msg;
  msg.data = tomato_msgs::Phase::REMOTE;
  pub_phase.publish(msg);
}

void ControlPanel::clickedKobuki() {
  phase_label->setText(tr("Kobuki"));
  buttonSelected(phase_button[tomato_msgs::Phase::REMOTE_KOBUKI], true);
  buttonSelected(phase_button[tomato_msgs::Phase::REMOTE_MANIP_AXIS], false);
  buttonSelected(phase_button[tomato_msgs::Phase::REMOTE_MANIP_IK], false);

  std_msgs::UInt8 msg;
  msg.data = tomato_msgs::Phase::REMOTE_KOBUKI;
  pub_phase.publish(msg);
}

void ControlPanel::clickedManipAxis() {
  phase_label->setText(tr("ManipAxis"));
  buttonSelected(phase_button[tomato_msgs::Phase::REMOTE_KOBUKI], false);
  buttonSelected(phase_button[tomato_msgs::Phase::REMOTE_MANIP_AXIS], true);
  buttonSelected(phase_button[tomato_msgs::Phase::REMOTE_MANIP_IK], false);

  std_msgs::UInt8 msg;
  msg.data = tomato_msgs::Phase::REMOTE_MANIP_AXIS;
  pub_phase.publish(msg);
}

void ControlPanel::clickedManipIK() {
  phase_label->setText(tr("ManipIK"));
  buttonSelected(phase_button[tomato_msgs::Phase::REMOTE_KOBUKI], false);
  buttonSelected(phase_button[tomato_msgs::Phase::REMOTE_MANIP_AXIS], false);
  buttonSelected(phase_button[tomato_msgs::Phase::REMOTE_MANIP_IK], true);

  std_msgs::UInt8 msg;
  msg.data = tomato_msgs::Phase::REMOTE_MANIP_IK;
  pub_phase.publish(msg);
}

void ControlPanel::clickedDeviceJoy() {
  buttonSelected(device_button[tomato_msgs::Device::JOY], true, "blue");
  buttonSelected(device_button[tomato_msgs::Device::MARKER], false);

  std_msgs::UInt8 msg;
  msg.data = tomato_msgs::Device::JOY;
  pub_device.publish(msg);
}

void ControlPanel::clickedDeviceMarker() {
  buttonSelected(device_button[tomato_msgs::Device::MARKER], true, "blue");
  buttonSelected(device_button[tomato_msgs::Device::JOY], false);

  std_msgs::UInt8 msg;
  msg.data = tomato_msgs::Device::MARKER;
  pub_device.publish(msg);
}

void ControlPanel::buttonEnabled(QToolButton *button, bool flag) {
  button->setEnabled(flag);
  if (!flag) {
    buttonSelected(button, false);
  }
}

void ControlPanel::buttonSelected(QToolButton *button, bool flag) {
  if (flag) {
    button->setStyleSheet("background-color: rgba(224,50,83,100);");
  } else {
    button->setStyleSheet("background-color: rgba(239,235,231,255);");
  }
}

void ControlPanel::buttonSelected(QToolButton *button, bool flag,
                                  std::string color) {
  if (flag) {
    button->setStyleSheet("background-color: rgba(224,50,83,100);");
  } else {
    button->setStyleSheet("background-color: rgba(239,235,231,255);");
  }

  if ((color == "b") || (color == "blue"))
    button->setStyleSheet("background-color: rgba(0,113,188,100);");
}

void ControlPanel::save(rviz::Config config) const {
  rviz::Panel::save(config);
}

void ControlPanel::load(const rviz::Config &config) {
  rviz::Panel::load(config);
}

} // control_plugin

PLUGINLIB_EXPORT_CLASS(control_plugin::ControlPanel, rviz::Panel)