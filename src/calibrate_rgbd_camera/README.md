# About this package

Xtionのキャリブレーションを行う  
キャリブレーションデータの読み込みもできるようにする予定

# 前提

`/scripts/install_calibration_package.sh` を使って必要なパッケージをインストール


# RGBカメラのキャリブレーション

    $ roslaunch openni2_launch openni2.launch
以降RGBカメラのキャリブレーションが終わるまで立ち上げたままにしておく  

    $ rostopic list
        /camera/rgb/camera_info
        /camera/rgb/image_raw
        /camera/depth/camera_info
        /camera/depth/image_raw
を確認  

    $ rosrun camera_calibration cameracalibrator.py --size 8x6 --square 0.0245 image:=/camera/rgb/image_raw
（size,squareは用意したチェックボードに合わせる）

カメラの前でチェックボードを動かす  
近づく/遠ざかる，ひねる，上下左右に平行移動など  

CALIBRATEボタンが押せるようになったら押してしばらく待つ  
（フリーズしたっぽい挙動になるが気にしない）  

ターミナルにキャリブレーションデータが表示されてCOMMITボタンが押せるようになったら押す  


`~/.ros/camera_info` に `rgb_PS1080_PrimeSense.yaml` が（おそらく）自動生成される  
自動生成されなかった場合は `/tmp/calibrationdata.tar.gz` を解凍して使う  


# Depthカメラのキャリブレーション

    $ roslaunch jsk_pcl_ros openni2_local.launch  
    $ roslaunch jsk_pcl_ros openni2_remote.launch  
`openni2_local.launch` の側でRGBキャリブレーションデータが読み込まれて  

    [INFO] [1563133962.016375462]: camera calibration URL: file:///home/name/.ros/camera_info/rgb_PS1080_PrimeSense.yaml  
という感じになるはず  

    $ roscd jsk_pcl_ros/launch/  
    $ sudo gedit depth_error.launch  
でエディタを開いて  

        <arg name="GRID_SIZE_X" default="8" />  
        <arg name="GRID_SIZE_Y" default="6" />  
        <param name="rect0_size_x" type="double" value="0.0245"/>  
        <param name="rect0_size_y" type="double" value="0.0245"/>  
にパラメータ変更（用意したチェックボードに合わせる），  
最後の辺にrosparamで `approximate_sync: true` を追記  
```
    PointCloud Processing.
    <node pkg="jsk_pcl_ros" type="depth_image_error"
          name="depth_image_error"
          output="screen">
        <remap from="~image" to="$(arg DEPTH_IMAGE)" />
        <remap from="~point" to="/checkerdetector/corner_point" />
        <remap from="~camera_info" to="$(arg CAMERA_INFO_TOPIC)" />
        <rosparam>
            approximate_sync: true
        </rosparam>
    </node>
```

    $ roslaunch jsk_pcl_ros depth_error.launch  
RGB画像が表示される  

    $ rosrun rviz rviz  

`Fixed Frame` を `camera_depth_frame` に変更  

`Pose`, `Pointcloud2`, `Pointcloud2` をAddして  
`/checkerdetector/objectdetection_pose`,  
`/camera_remote_uncalibrated/depth_registered/points`,  
`/camera_remote/depth_registered/points` をsubscribe  

    $ roscd calibrate_rgbd_camera/scripts/  
    $ ls -l  
でパーミッションを確認  

    $ sudo chmod a+w calibration.csv calibration_parameter.yaml
後述のキャリブレーションデータ保存失敗を回避するために事前に用意したファイルに書き込み権限を与えておく（とても無理矢理なのでもっとよい解決策があれば教えてください）

    $ rosrun calibrate_rgbd_camera depth_error_calibration_chmod.py --model quadratic-uv-quadratic-abs
グラフが出てくるのでカメラの前でチェックボードを動かす  
（公式パッケージの `$ rosrun jsk_pcl_ros depth_error_calibration.py --model quadratic-uv-quadratic-abs` だとPermission deniedでデータ保存失敗した，一応最後にエラーメッセージを載せておく）

    $ rosrun image_view image_view image:=/depth_error_logistic_regression/frequency_image  
何かが表示される

`Ctrl+c` で `depth_error_calibration_chmod.py` を落とすと `calibration.csv` と `calibration_parameter.yaml` にデータが保存されるのでrenameして利用


# キャリブレーションデータの読み込み

hoge


# 参考
- [camera_calibration - ROS Wiki](http://wiki.ros.org/camera_calibration)
- [ROS講座66 カメラのキャリブレーションを行う](https://qiita.com/srs/items/416aa78f2c679ddb7c52#%E6%A6%82%E8%A6%81)  
- [Depth Camera Calibration(Kinect,Xtion,Primesense)](https://jsk-recognition.readthedocs.io/en/latest/jsk_pcl_ros/calibration.html)  
-  [permission deniedで悩まされたので勉強してみた](https://qiita.com/takuyanin/items/18590600d077df707923)  

### `depth_error_calibration.py` 終了時のエラー
```
^CSave calibration parameters to calibration-2019-07-15-05-54-46.csv
Traceback (most recent call last):
  File "/opt/ros/kinetic/share/jsk_pcl_ros/scripts/check_depth_error/depth_error_calibration.py", line 434, in <module>
    main()
  File "/opt/ros/kinetic/share/jsk_pcl_ros/scripts/check_depth_error/depth_error_calibration.py", line 408, in main
    with open(csv_filename, "w") as f:
IOError: [Errno 13] Permission denied: 'calibration-2019-07-15-05-54-46.csv'
```