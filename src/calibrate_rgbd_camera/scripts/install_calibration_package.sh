#!/bin/sh

# update of apt
yes | sudo apt update
yes | sudo apt upgrade

# RGB
yes | sudo apt install ros-kinetic-camera-calibration
yes | sudo apt install ros-kinetic-image-proc
yes | sudo apt install ros-kinetic-openni2-launch

# Depth
yes | sudo apt install ros-kinetic-jsk-pcl-ros
