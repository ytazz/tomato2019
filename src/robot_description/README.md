# About this package

rvizでロボットを可視化  
ロボットモデルやシミュレーション用プログラム

## 構成

>robot_description/  
>　├ include/  
>　├ launch/  
>　│　└ \*.launch  
>　├ meshes/  
>　│　├ \*.stl  
>　│　└ \*.dae  
>　├ rviz/  
>　│　└ \*.rviz  
>　├ urdf/  
>　│　├ \*.urdf  
>　│　└ \*.xacro  
>　├ CMakeLists.txt  
>　├ package.xml   
>　└ README.md  

## How to use

### 鉢とトマトのモデル

軌道計画の際にあったほうが便利かなと思ってかんたんのなモデルを作りました．  
実際の鉢の寸法通りでないので,暇があれば修正します．  
以下コマンドで鉢withトマトモデルをRviz上で見ることができます．  

    $ roscd robot_description/urdf/
    $ roslaunch urdf_tutorial display.launch model:=planter.xacro

または

    $ roslaunch robot_description planter.launch