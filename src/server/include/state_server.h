/*
    author: H. Kuribayashi

    状態サーバ
*/

#ifndef __STATE_SERVER_H__
#define __STATE_SERVER_H__

// ヘッダファイルの読み込み
// C言語系
#include <cmath>
// ROS系
#include <geometry_msgs/Twist.h> // Kobuki軌道計画用
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64.h>
#include <std_srvs/Empty.h> // empty service
#include <string.h>
#include <tf/transform_broadcaster.h>
// 自分で定義したメッセージ型
#include "map_param.h"
#include <tomato_msgs/KobukiState.h>  // Kobuki軌道計画用
#include <tomato_msgs/KobukiValue.h>  // state service
#include <tomato_msgs/ManipState.h>   // マニピュレータ軌道計画用
#include <tomato_msgs/ManipValue.h>   // state service
#include <tomato_msgs/PlanterState.h> // 鉢の状態
#include <tomato_msgs/PlanterValue.h> // state service
#include <tomato_msgs/RvCircle.h>     // 円形描画用
#include <tomato_msgs/RvLine.h>       // 線描画用
#include <tomato_msgs/RvPoint.h>      // 点描画用
#include <tomato_msgs/RvSquare.h>     // 四角形描画用
#include <tomato_msgs/RvStl.h>        // stlモデル描画用

class StateServer {
public:
  StateServer();
  ~StateServer();

  void setKobukiRefCallback(const tomato_msgs::KobukiState::ConstPtr &ref);
  void setKobukiCurCallback(const tomato_msgs::KobukiState::ConstPtr &cur);
  void broadcastKobukiCur();
  void broadcastKobukiRef();

  void setManipRefCallback(const tomato_msgs::ManipState::ConstPtr &ref);
  void setManipCurCallback(const tomato_msgs::ManipState::ConstPtr &cur);
  void pubJointsRef();
  void pubJointsCur();

  void setPlanterCurCallback(const tomato_msgs::PlanterState::ConstPtr &cur);
  void broadcastPlanterCur();

  bool srvKobukiValue(tomato_msgs::KobukiValue::Request &req,
                      tomato_msgs::KobukiValue::Response &res);
  bool srvPlanterValue(tomato_msgs::PlanterValue::Request &req,
                       tomato_msgs::PlanterValue::Response &res);
  bool srvManipValue(tomato_msgs::ManipValue::Request &req,
                     tomato_msgs::ManipValue::Response &res);

private:
  void init();
  void init(tomato_msgs::ManipState *state);
  void init(geometry_msgs::Twist *twist);
  void init(tomato_msgs::PlanterState *state);
  void init(sensor_msgs::JointState *state, std::string prefix);

  // error = reference - current
  void updateKobukiError();
  void updateManipError();

  // ROSシステムの変数
  ros::NodeHandle nh;

  // 状態更新Subscriber
  ros::Subscriber sub_kobuki_ref, sub_kobuki_cur;
  ros::Subscriber sub_manip_ref, sub_manip_cur;
  ros::Subscriber sub_planter_cur;

  // マニピュレータの現在状態と目標状態をRvizに反映させるためのpublisher
  ros::Publisher pub_joint_ref, pub_joint_cur;

  // jointの数
  const int num_joint;

  // 状態保存変数
  tomato_msgs::KobukiState kobuki_ref, kobuki_cur, kobuki_err;
  tomato_msgs::ManipState manip_ref, manip_cur, manip_err;
  tomato_msgs::PlanterState planter_cur;
  sensor_msgs::JointState joint_ref, joint_cur;
};

#endif // __STATE_SERVER_H____