/*
    author: H. Kuribayashi

    時間サーバ
*/

#ifndef __TIME_SERVER_H__
#define __TIME_SERVER_H__

// ヘッダファイルの読み込み
// C言語系
#include <cmath>
#include <string>
// ROS系
#include "std_msgs/Empty.h"
#include <jsk_rviz_plugins/OverlayText.h>
#include <ros/ros.h>
// 自分で定義したメッセージ型
#include "map_param.h"
#include <tomato_msgs/Time.h>

class TimeServer {
public:
  TimeServer();
  ~TimeServer();

  void resetCallback(const std_msgs::Empty::ConstPtr &);
  void timerCallback(const ros::TimerEvent &);

  bool srvTime(tomato_msgs::Time::Request &req,
               tomato_msgs::Time::Response &res);

private:
  // ROSシステムの変数
  ros::NodeHandle nh;
  ros::Timer timer;
  ros::Subscriber sub;
  ros::Publisher pub;
  ros::ServiceClient client;

  const double dt;

  int start_time;
};

#endif // __TIME_SERVER_H____
