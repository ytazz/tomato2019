/*
    author: H. Kuribayashi

    地図サーバ
*/

#ifndef __MAP_SERVER_H__
#define __MAP_SERVER_H__

// ヘッダファイルの読み込み
// C言語系
#include <cmath>
// ROS系
#include <ros/ros.h>
// 自分で定義したメッセージ型
#include "map_param.h"
#include <geometry_msgs/Twist.h>     // Kobuki軌道計画用
#include <std_srvs/Empty.h>          // empty service
#include <tomato_msgs/KobukiValue.h> // state service
#include <tomato_msgs/PlanterValue.h>    // state service
#include <tomato_msgs/RvCircle.h>    // 円形描画用
#include <tomato_msgs/RvLine.h>      // 線描画用
#include <tomato_msgs/RvPoint.h>     // 点描画用
#include <tomato_msgs/RvSquare.h>    // 四角形描画用

class MapServer {
public:
  MapServer();
  ~MapServer();

  void timerCallback(const ros::TimerEvent &);

private:
  void pubField();
  void pubStart();
  void pubTomato();
  void pubCircle();

  // ROSシステムの変数
  ros::NodeHandle nh;
  ros::Timer timer;
  ros::Subscriber sub_planter;
  ros::Publisher pub_field, pub_tomato, pub_start, pub_circle;
  ros::ServiceClient client;

  geometry_msgs::Point planter_cur;

  const float dt;
};

#endif // __MAP_SERVER_H__