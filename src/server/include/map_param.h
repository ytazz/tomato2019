/*
    author: H. Kuribayashi

    トマト競技会場のパラメータ

    unit:
      position [m]
      time [min]
      color [-] (from 0 to 1)

*/

#ifndef __MAP_PARAM_H__
#define __MAP_PARAM_H__

#define FIELD_POSITION_X 1.5
#define FIELD_POSITION_Y 0.0
#define FIELD_HEIGHT 3.5
#define FIELD_WIDTH 3.5
#define FIELD_COLOR_R 1.0
#define FIELD_COLOR_G 1.0
#define FIELD_COLOR_B 1.0
#define FIELD_COLOR_A 1.0

#define START_POSITION_X 0
#define START_POSITION_Y 0
#define START_HEIGHT 0.5
#define START_WIDTH 0.5
#define START_COLOR_R 1.0
#define START_COLOR_G 1.0
#define START_COLOR_B 1.0
#define START_COLOR_A 1.0

#define TOMATO_POSITION_X 1.5
#define TOMATO_POSITION_Y 0.0
#define TOMATO_RADIUS 0.75
#define TOMATO_COLOR_R 1.0
#define TOMATO_COLOR_G 1.0
#define TOMATO_COLOR_B 1.0
#define TOMATO_COLOR_A 1.0

#define POT_RADIUS 0.4

#define CIRCLE_RADIUS_DEFAULT 0.0
#define CIRCLE_COLOR_R 0.0
#define CIRCLE_COLOR_G 1.0
#define CIRCLE_COLOR_B 1.0
#define CIRCLE_COLOR_A 1.0

#define COMPETITION_TIME 5 // [min]

#endif // __MAP_PARAM_H__