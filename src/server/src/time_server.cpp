/*
    author: H. Kuribayashi

    時間サーバ
*/

#include "time_server.h"

TimeServer::TimeServer() : dt(1) {
  sub = nh.subscribe<std_msgs::Empty>("/timer/reset", 1,
                                      &TimeServer::resetCallback, this);
  pub = nh.advertise<jsk_rviz_plugins::OverlayText>("/timer/text", 1);

  timer = nh.createTimer(ros::Duration(dt), &TimeServer::timerCallback, this);

  start_time = ros::Time::now().sec;
}

TimeServer::~TimeServer() {}

void TimeServer::resetCallback(const std_msgs::Empty::ConstPtr &) {
  start_time = ros::Time::now().sec;
}

void TimeServer::timerCallback(const ros::TimerEvent &) {
  jsk_rviz_plugins::OverlayText text;
  text.action = jsk_rviz_plugins::OverlayText::ADD;
  text.width = 200;
  text.height = 100;
  text.left = 0;
  text.top = 0;

  std_msgs::ColorRGBA bg_color, fg_color;
  bg_color.r = 0;
  bg_color.g = 0;
  bg_color.b = 0;
  bg_color.a = 0.4;
  text.bg_color = bg_color;

  text.line_width = 1;
  text.text_size = 50;
  text.font = "Ubuntu";

  int remaining_time =
      COMPETITION_TIME * 60 - (ros::Time::now().sec - start_time);
  int minute = remaining_time / 60;
  int second = remaining_time % 60;

  std::string str = "";

  if (remaining_time > 1 * 60) {
    fg_color.r = 25.0 / 255;
    fg_color.g = 255.0 / 255;
    fg_color.b = 240.0 / 255;
    fg_color.a = 0.8;
    str += std::to_string(minute);
    str += " : ";
    if (second < 10) // 秒が１桁の時
      str += "0";
    str += std::to_string(second);
  } else if (remaining_time > 0) {
    fg_color.r = 1.0;
    fg_color.g = 1.0;
    fg_color.b = 0.0;
    fg_color.a = 0.8;
    str += std::to_string(minute);
    str += " : ";
    if (second < 10) // 秒が１桁の時
      str += "0";
    str += std::to_string(second);
  } else {
    fg_color.r = 1.0;
    fg_color.g = 0.0;
    fg_color.b = 0.0;
    fg_color.a = 0.8;
    str += "0 : 00";
  }

  text.fg_color = fg_color;
  text.text = str;

  pub.publish(text);
}

bool TimeServer::srvTime(tomato_msgs::Time::Request &req,
                         tomato_msgs::Time::Response &res) {
  double sec = ros::Time::now().sec - start_time;
  double nsec = ros::Time::now().nsec;
  res.current = sec + nsec / (1000 * 1000 * 1000);
  return true;
}

// main関数
// 定義したクラスのオブジェクト（ノード）を生成
int main(int argc, char **argv) {
  ros::init(argc, argv, "time_server");
  ros::NodeHandle nh;

  TimeServer time_server;
  ros::ServiceServer srv_time =
      nh.advertiseService("/timer/current", &TimeServer::srvTime, &time_server);

  ros::spin();
}