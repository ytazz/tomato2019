/*
    author: H. Kuribayashi

    地図サーバ
*/

#include "map_server.h"

MapServer::MapServer() : dt(0.1) {
  timer = nh.createTimer(ros::Duration(dt), &MapServer::timerCallback, this);

  pub_tomato = nh.advertise<tomato_msgs::RvCircle>("rviz_plot/circle", 10);
  pub_start = nh.advertise<tomato_msgs::RvCircle>("rviz_plot/circle", 10);
  pub_field = nh.advertise<tomato_msgs::RvSquare>("rviz_plot/square", 10);
  pub_start = nh.advertise<tomato_msgs::RvSquare>("rviz_plot/square", 10);

  client = nh.serviceClient<tomato_msgs::PlanterValue>("/planter/state");
}

MapServer::~MapServer() {}

void MapServer::pubField() {
  tomato_msgs::RvSquare rv_square;

  rv_square.name = "field";
  rv_square.action = "add";
  rv_square.size = 0.02;
  rv_square.color.r = FIELD_COLOR_R;
  rv_square.color.g = FIELD_COLOR_G;
  rv_square.color.b = FIELD_COLOR_B;
  rv_square.color.a = FIELD_COLOR_A;

  geometry_msgs::Point square;
  square.x = FIELD_POSITION_X;
  square.y = FIELD_POSITION_Y;
  square.z = 0.0;
  rv_square.position = square;

  rv_square.width = FIELD_WIDTH;
  rv_square.height = FIELD_HEIGHT;

  pub_start.publish(rv_square);
}

void MapServer::pubStart() {
  tomato_msgs::RvSquare rv_square;

  rv_square.name = "start_area";
  rv_square.action = "add";
  rv_square.size = 0.02;
  rv_square.color.r = START_COLOR_R;
  rv_square.color.g = START_COLOR_G;
  rv_square.color.b = START_COLOR_B;
  rv_square.color.a = START_COLOR_A;

  geometry_msgs::Point square;
  square.x = START_POSITION_X;
  square.y = START_POSITION_Y;
  square.z = 0.0;
  rv_square.position = square;

  rv_square.width = START_WIDTH;
  rv_square.height = START_HEIGHT;

  pub_start.publish(rv_square);
}

void MapServer::pubTomato() {
  tomato_msgs::RvCircle rv_circle;

  rv_circle.name = "tomato_area";
  rv_circle.action = "add";
  rv_circle.size = 0.02;
  rv_circle.color.r = TOMATO_COLOR_R;
  rv_circle.color.g = TOMATO_COLOR_G;
  rv_circle.color.b = TOMATO_COLOR_B;
  rv_circle.color.a = TOMATO_COLOR_A;

  geometry_msgs::Point circle;
  circle.x = TOMATO_POSITION_X;
  circle.y = TOMATO_POSITION_Y;
  circle.z = 0.0;
  rv_circle.position = circle;

  rv_circle.radius = TOMATO_RADIUS;

  pub_tomato.publish(rv_circle);
}

void MapServer::pubCircle() {
  tomato_msgs::RvCircle rv_circle;

  rv_circle.name = "circle_area";
  rv_circle.action = "add";
  rv_circle.size = 0.02;
  rv_circle.color.r = CIRCLE_COLOR_R;
  rv_circle.color.g = CIRCLE_COLOR_G;
  rv_circle.color.b = CIRCLE_COLOR_B;
  rv_circle.color.a = CIRCLE_COLOR_A;

  geometry_msgs::Point circle;
  tomato_msgs::PlanterValue srv;
  client.call(srv); // planterの現在位置を取得
  circle.x = srv.response.current.position.x;
  circle.y = srv.response.current.position.y;
  circle.z = 0.0;
  rv_circle.position = circle;

  rv_circle.radius = srv.response.current.circle_radius;
  rv_circle.radius = 0.0;

  pub_tomato.publish(rv_circle);
}

void MapServer::timerCallback(const ros::TimerEvent &) {
  pubField();
  pubStart();
  pubTomato();
  pubCircle();
}

// main関数
// 定義したクラスのオブジェクト（ノード）を生成
int main(int argc, char **argv) {
  ros::init(argc, argv, "map_server");
  MapServer map_server;
  ros::spin();
}