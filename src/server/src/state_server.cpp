/*
    author: H. Kuribayashi

    状態サーバ
*/

#include "state_server.h"

StateServer::StateServer() : num_joint(8) {
  sub_kobuki_ref = nh.subscribe<tomato_msgs::KobukiState>(
      "/kobuki/pose/reference", 10, &StateServer::setKobukiRefCallback, this);
  sub_kobuki_cur = nh.subscribe<tomato_msgs::KobukiState>(
      "/kobuki/pose/current", 10, &StateServer::setKobukiCurCallback, this);

  sub_manip_ref = nh.subscribe<tomato_msgs::ManipState>(
      "/manip/pose/reference", 10, &StateServer::setManipRefCallback, this);
  sub_manip_cur = nh.subscribe<tomato_msgs::ManipState>(
      "/manip/pose/current", 10, &StateServer::setManipCurCallback, this);

  sub_planter_cur = nh.subscribe<tomato_msgs::PlanterState>(
      "/planter/pose/current", 10, &StateServer::setPlanterCurCallback, this);

  // pub_joint_ref =
  //     nh.advertise<sensor_msgs::JointState>("/joint_states_ref", 10);
  pub_joint_cur = nh.advertise<sensor_msgs::JointState>("/joint_states", 10);

  init();
}

StateServer::~StateServer() {}

void StateServer::init() {
  init(&kobuki_ref.position);
  init(&kobuki_ref.velocity);
  init(&kobuki_cur.position);
  init(&kobuki_cur.velocity);
  init(&kobuki_err.position);
  init(&kobuki_err.velocity);

  init(&planter_cur);

  init(&joint_ref, "ref");
  init(&joint_cur, "cur");

  init(&manip_ref);
  init(&manip_cur);
  init(&manip_err);
}

void StateServer::init(tomato_msgs::ManipState *state) {
  state->current_position.x = 0.0;
  state->current_position.y = 0.0;
  state->current_position.z = 0.0;
  state->current_orientation.x = 0.0;
  state->current_orientation.y = 0.0;
  state->current_orientation.z = 0.0;
  state->target_position.x = 0.0;
  state->target_position.y = 0.0;
  state->target_position.z = 0.0;
  state->target_position.x = 0.0;
  state->target_position.y = 0.0;
  state->target_position.z = 0.0;
  state->joint_position.clear();
  state->joint_position.resize(num_joint + 4);
  state->joint_position[0] = 0.0;
  state->joint_position[1] = 0.0;
  state->joint_position[2] = 0.0;
  state->joint_position[3] = 0.0;
  state->joint_position[4] = 0.0;
  state->joint_position[5] = 0.0;
  state->joint_position[6] = 0.0;
  state->joint_position[7] = 0.0;
  state->joint_position[8] = 0.0;
  state->joint_position[9] = 0.0;
  state->joint_position[10] = 0.0;
  state->joint_position[11] = 0.0;
}

void StateServer::init(geometry_msgs::Twist *twist) {
  twist->linear.x = 0.0;
  twist->linear.y = 0.0;
  twist->linear.z = 0.0;
  twist->angular.x = 0.0;
  twist->angular.y = 0.0;
  twist->angular.z = 0.0;
}

void StateServer::init(tomato_msgs::PlanterState *state) {
  state->position.x = FIELD_POSITION_X;
  state->position.y = 0.0;
  state->position.z = 0.0;
  state->circle_radius = CIRCLE_RADIUS_DEFAULT;
}

void StateServer::init(sensor_msgs::JointState *state, std::string prefix) {
  if (prefix == std::string("ref")) {
    state->name.clear();
    state->name.push_back("joint0_ref");
    state->name.push_back("joint1_ref");
    state->name.push_back("joint2_ref");
    state->name.push_back("joint3_ref");
    state->name.push_back("joint4_ref");
    state->name.push_back("joint5_ref");
    state->name.push_back("joint6_ref");
    state->name.push_back("joint7_ref");
  } else if (prefix == std::string("cur")) {
    state->name.clear();
    state->name.resize(num_joint + 4);
    state->name[0] = "joint0";
    state->name[1] = "joint1";
    state->name[2] = "joint2";
    state->name[3] = "joint3";
    state->name[4] = "joint4";
    state->name[5] = "joint5";
    state->name[6] = "joint6";
    state->name[7] = "joint6r";
    state->name[8] = "joint6l";
    state->name[9] = "joint7";
    state->name[10] = "joint7r";
    state->name[11] = "joint7l";
  } else {
    printf("WANING: \n");
    printf("joint's initialization failed\n");
  }
}

void StateServer::setKobukiRefCallback(
    const tomato_msgs::KobukiState::ConstPtr &ref) {
  this->kobuki_ref = *ref;
  updateKobukiError();
}

void StateServer::setKobukiCurCallback(
    const tomato_msgs::KobukiState::ConstPtr &cur) {
  this->kobuki_cur = *cur;
  updateKobukiError();
}

void StateServer::updateKobukiError() {
  kobuki_err.position.linear.x =
      kobuki_ref.position.linear.x - kobuki_cur.position.linear.x;
  kobuki_err.position.linear.y =
      kobuki_ref.position.linear.y - kobuki_cur.position.linear.y;
  kobuki_err.position.linear.z =
      kobuki_ref.position.linear.z - kobuki_cur.position.linear.z;
  kobuki_err.position.angular.x =
      kobuki_ref.position.angular.x - kobuki_cur.position.angular.x;
  kobuki_err.position.angular.y =
      kobuki_ref.position.angular.y - kobuki_cur.position.angular.y;
  kobuki_err.position.angular.z =
      kobuki_ref.position.angular.z - kobuki_cur.position.angular.z;

  kobuki_err.velocity.linear.x =
      kobuki_ref.velocity.linear.x - kobuki_cur.velocity.linear.x;
  kobuki_err.velocity.linear.y =
      kobuki_ref.velocity.linear.y - kobuki_cur.velocity.linear.y;
  kobuki_err.velocity.linear.z =
      kobuki_ref.velocity.linear.z - kobuki_cur.velocity.linear.z;
  kobuki_err.velocity.angular.x =
      kobuki_ref.velocity.angular.x - kobuki_cur.velocity.angular.x;
  kobuki_err.velocity.angular.y =
      kobuki_ref.velocity.angular.y - kobuki_cur.velocity.angular.y;
  kobuki_err.velocity.angular.z =
      kobuki_ref.velocity.angular.z - kobuki_cur.velocity.angular.z;
}

void StateServer::broadcastKobukiCur() {
  tf::Transform transform;
  transform.setOrigin(tf::Vector3(this->kobuki_cur.position.linear.x,
                                  this->kobuki_cur.position.linear.y,
                                  this->kobuki_cur.position.linear.z));
  tf::Quaternion q;
  q.setRPY(this->kobuki_cur.position.angular.x,
           this->kobuki_cur.position.angular.y,
           this->kobuki_cur.position.angular.z);
  transform.setRotation(q);

  static tf::TransformBroadcaster br;
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world",
                                        "kobuki_base"));

  pubJointsCur();
}

void StateServer::broadcastKobukiRef() {
  tf::Transform transform;
  transform.setOrigin(tf::Vector3(this->kobuki_ref.position.linear.x,
                                  this->kobuki_ref.position.linear.y,
                                  this->kobuki_ref.position.linear.z));
  tf::Quaternion q;
  q.setRPY(this->kobuki_ref.position.angular.x,
           this->kobuki_ref.position.angular.y,
           this->kobuki_ref.position.angular.z);
  transform.setRotation(q);

  static tf::TransformBroadcaster br;
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world",
                                        "kobuki_base_ref"));
}

void StateServer::setManipRefCallback(
    const tomato_msgs::ManipState::ConstPtr &ref) {
  this->manip_ref = *ref;
  updateManipError();
  pubJointsRef();
}

void StateServer::setManipCurCallback(
    const tomato_msgs::ManipState::ConstPtr &cur) {
  this->manip_cur = *cur;
  updateManipError();
  pubJointsCur();
}

void StateServer::updateManipError() {
  manip_err.current_position.x =
      manip_ref.target_position.x - manip_cur.current_position.x;
  manip_err.current_position.y =
      manip_ref.target_position.y - manip_cur.current_position.y;
  manip_err.current_position.z =
      manip_ref.target_position.z - manip_cur.current_position.z;
  manip_err.current_orientation.x =
      manip_ref.target_orientation.x - manip_cur.current_orientation.x;
  manip_err.current_orientation.y =
      manip_ref.target_orientation.y - manip_cur.current_orientation.y;
  manip_err.current_orientation.z =
      manip_ref.target_orientation.z - manip_cur.current_orientation.z;

  for (int i = 0; i < num_joint; i++) {
    manip_err.joint_position[i] =
        manip_ref.joint_position[i] - manip_cur.joint_position[i];
  }
}

void StateServer::pubJointsCur() {
  joint_cur.header.stamp = ros::Time::now();
  joint_cur.position = manip_cur.joint_position;
  // joint_cur.position.resize(12);
  // for (int i = 0; i < 8; i++) {
  //   joint_cur.position[i] = manip_cur.joint_position[i];
  // }
  // printf("%lf\n", joint_cur.position[2]);
  // joint_cur.position[8] = 0.0;
  // joint_cur.position[9] = 0.0;
  // joint_cur.position[10] = 0.0;
  // joint_cur.position[11] = 0.0;
  pub_joint_cur.publish(joint_cur);
}

void StateServer::pubJointsRef() {
  joint_ref.header.stamp = ros::Time::now();
  joint_ref.position = manip_ref.joint_position;
  // pub_joint_ref.publish(joint_ref);
}

void StateServer::setPlanterCurCallback(
    const tomato_msgs::PlanterState::ConstPtr &cur) {
  this->planter_cur = *cur;
}

void StateServer::broadcastPlanterCur() {
  tf::Transform transform;
  transform.setOrigin(tf::Vector3(this->planter_cur.position.x,
                                  this->planter_cur.position.y,
                                  this->planter_cur.position.z));
  tf::Quaternion q;
  q.setRPY(0, 0, 0);
  transform.setRotation(q);

  static tf::TransformBroadcaster br;
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world",
                                        "planter_base"));
}

bool StateServer::srvKobukiValue(tomato_msgs::KobukiValue::Request &req,
                                 tomato_msgs::KobukiValue::Response &res) {
  res.reference = kobuki_ref;
  res.current = kobuki_cur;
  res.error = kobuki_err;
  return true;
}

bool StateServer::srvPlanterValue(tomato_msgs::PlanterValue::Request &req,
                                  tomato_msgs::PlanterValue::Response &res) {
  res.current = planter_cur;
  return true;
}

bool StateServer::srvManipValue(tomato_msgs::ManipValue::Request &req,
                                tomato_msgs::ManipValue::Response &res) {
  res.reference = manip_ref;
  res.current = manip_cur;
  res.error = manip_err;
  return true;
}

// main関数
// 定義したクラスのオブジェクト（ノード）を生成
int main(int argc, char **argv) {
  ros::init(argc, argv, "state_server");
  ros::NodeHandle nh;

  StateServer state_server;
  ros::ServiceServer srv_kobuki = nh.advertiseService(
      "/kobuki/state", &StateServer::srvKobukiValue, &state_server);
  ros::ServiceServer srv_planter = nh.advertiseService(
      "/planter/state", &StateServer::srvPlanterValue, &state_server);
  ros::ServiceServer srv_manip = nh.advertiseService(
      "/manip/state", &StateServer::srvManipValue, &state_server);

  ros::Rate loop_rate(10);
  while (ros::ok()) {
    state_server.broadcastKobukiCur();
    state_server.broadcastKobukiRef();
    state_server.broadcastPlanterCur();
    ros::spinOnce();
    loop_rate.sleep();
  }
}
