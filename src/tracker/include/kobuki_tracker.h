/*
    author: H. Kuribayashi

    Trajectory Tracker for Kobuki Robot

    state variable xi consists of
      xi_x    : position x of center of the robot
      xi_y    : position y of center of the robot
      xi_theta: orientation
      xi_v    : linear velocity
      xi_omega: angular velocity

    input variable u consists of
      u_v     : linear velocity
      u_omega : angular velocity
*/

#ifndef __KOBUKI_TRACKER_H__
#define __KOBUKI_TRACKER_H__

// ヘッダファイルの読み込み
// C言語系
#include <cmath>
#include <string>
// ROS系
#include <ros/ros.h>

class KobukiTracker {
public:
  KobukiTracker();
  ~KobukiTracker();

  void get();

private:
};

#endif // __KOBUKI_TRACKER_H__