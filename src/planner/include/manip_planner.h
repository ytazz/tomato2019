/*shunji*/
#include <fstream>
#include <iostream>
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <tomato_msgs/ManipState.h>
#include <tomato_msgs/ManipValue.h>
#define pi 3.141592

// ROSシステムの変数
ros::NodeHandle nh;
ros::Subscriber sub;
ros::Publisher pub_cur, pub_ref;
ros::ServiceClient cli_kobuki, cli_manip, cli_planter, cli_time;
tomato_msgs::ManipValue srv_manip;

geometry_msgs::Vector3 current;
geometry_msgs::Vector3 target;