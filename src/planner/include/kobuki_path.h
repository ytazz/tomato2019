/*
    author: H. Kuribayashi

    経路生成クラス
    現在poseと目標poseから計算
*/

#ifndef __KOBUKI_PATH_H__
#define __KOBUKI_PATH_H__

// ヘッダファイルの読み込み
// C言語系
#include "interpolation.h"
#include <iostream>
// #include <math.h>
// ROS系
#include <geometry_msgs/Twist.h>
#include <ros/ros.h>
// 自分で定義したメッセージ型
#include <tomato_msgs/KobukiState.h> // Kobuki軌道計画用
#include <tomato_msgs/RvArrow.h>     // 矢印描画用
#include <tomato_msgs/RvCircle.h>    // 円描画用
#include <tomato_msgs/RvLine.h>      // 線描画用
#include <tomato_msgs/RvPoint.h>     // 点描画用

// 経路生成クラス
class KobukiPath {
public:
  KobukiPath();
  ~KobukiPath();

  void clear();
  bool exist();

  void set(geometry_msgs::Twist start, geometry_msgs::Twist goal);

  tomato_msgs::KobukiState get(double t);
  double getPathTime();

private:
  // sign function
  double sgn(double val);

  // 軌道計画されているかいないか
  bool is_planned;

  // 軌道を３段階に分割
  // 1. startからgoalに向かう方向に先頭を合わせる回転
  // 2. startからgoalに直進移動する
  // 3. goal poseに先頭を合わせる回転
  Interpolation pre_rot[3];
  Interpolation linear[3];
  Interpolation post_rot[3];
  // 各軌道の所要時間
  double t_pre_rot[3], t_pre_rot_total;
  double t_linear[3], t_linear_total;
  double t_post_rot[3], t_post_rot_total;

  // ROSシステムの変数
  ros::NodeHandle nh;
  ros::Publisher pub_point, pub_line, pub_circle, pub_arrow;

  // Kobukiのハードウェア的な速度制約
  const double v_linear_max;  // [m/s]
  const double v_angular_max; // [rad/s]

  // 直進移動方向
  double direction;

  // 加速時間・減速時間
  const double t_accel; // [s]
  const double t_decel; // [s]

  FILE *fp;
};

#endif // __KOBUKI_PATH_H____