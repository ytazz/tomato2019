/*
    author: H. Kuribayashi

    多項式補間クラス
    0次から・３次多項式のみ対応
*/

#ifndef __INTERPOLATION_H__
#define __INTERPOLATION_H__

// ヘッダファイルの読み込み
#include <iostream>

// ２次多項式補間クラス
class Interpolation {
public:
  Interpolation();
  ~Interpolation();

  // 境界条件が１つのとき
  // １. 初期位置 initial position
  void set(double init_pos, double term_time);

  // 境界条件が２つのとき
  // １. 初期位置 initial position
  // ２. 初期速度 initial velocity
  // あと、終端時間 terminal time
  void set(double init_pos, double init_vel, double term_time);

  // 境界条件が３つのとき
  // １. 初期位置 initial position
  // ２. 初期速度 initial velocity
  // ３. 終端速度 terminal velosity
  // あと、終端時間 terminal time
  void set(double init_pos, double init_vel, double term_vel, double term_time);

  // 境界条件が４つのとき
  // １. 初期位置 initial position
  // ２. 初期速度 initial velocity
  // ３. 終端位置 terminal position
  // ４. 終端速度 terminal velosity
  // あと、終端時間 terminal time
  void set(double init_pos, double init_vel, double term_pos, double term_vel,
           double term_time);

  // 時刻tにおける目標位置を取得
  double getPos(double t);
  // 時刻tにおける目標速度を取得
  double getVel(double t);

  // 移動時間を取得
  double time();

  // パラメータを表示
  void print();

private:
  // 多項式の係数
  double c[4];
  // 計画時間
  double t_plan;
};

#endif // __INTERPOLATION_H__