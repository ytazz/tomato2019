/*
    author: H. Kuribayashi

    Kobuki用軌道計画器
*/

#ifndef __KOBUKI_PLANNER_H__
#define __KOBUKI_PLANNER_H__

// ヘッダファイルの読み込み
// C言語系
#include "kobuki_path.h"
#include <cmath>
// ROS系
#include <ros/ros.h>
// 自分で定義したメッセージ型
#include <tomato_msgs/KobukiPhase.h> // Kobuki軌道計画用
#include <tomato_msgs/KobukiValue.h>
#include <tomato_msgs/Phase.h> // phase
#include <tomato_msgs/Time.h>
// #include <tomato_msgs/ManipState.h>
#include <tomato_msgs/PlanterValue.h>
#include <tomato_msgs/RvCircle.h> // 円形描画用
#include <tomato_msgs/RvLine.h>   // 線描画用
#include <tomato_msgs/RvPoint.h>  // 点描画用

// Kobuki用軌道計画クラス
class KobukiPlanner {
public:
  KobukiPlanner();
  ~KobukiPlanner();

  // Phaseが設定された時に呼ばれる関数
  bool srvKobukiPhase(tomato_msgs::KobukiPhase::Request &req,
                      tomato_msgs::KobukiPhase::Response &res);

private:
  //
  geometry_msgs::Twist calcCircleRef(geometry_msgs::Twist kobuki_pos,
                                     geometry_msgs::Vector3 planter_pos);

  // 計算した目標位置をPD controllerにpublish
  void pubPath();

  // ROSシステムの変数
  ros::NodeHandle nh;
  ros::Subscriber sub;
  ros::Publisher pub_ref;
  ros::ServiceClient cli_kobuki, cli_planter, cli_time;

  tomato_msgs::KobukiValue srv_kobuki;
  tomato_msgs::PlanterValue srv_planter;
  tomato_msgs::Time srv_time;

  // Kobuki経路計画クラス
  KobukiPath path;
  geometry_msgs::Twist current;
  geometry_msgs::Twist target;
  tomato_msgs::KobukiState ref;

  // phase
  uint prev_phase;

  // time
  double t;
  double start_time;

  // 周回するときの周方向角速度最大値
  double omega_max;
};

#endif // __KOBUKI_PLANNER_H____