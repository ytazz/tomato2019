#include <fstream>
#include <iostream>

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 4
#define M 4
#define pi 3.141592
// ＊＊＊正確なリンク長の追記が必要＊＊＊
const double l1=0 ; // [m]
const double l2=0.1 ; // [m]
const double l3=0.1 ; // [m]
const double l4=0.15 ; // [m]
double link[5][4]= {{0, 0, 0, 0},{0, pi/2, 0, pi/2tr 65},{l2, 0, 0, 0},{l3, 0, 0, 0},{l4, -pi/2, 0, 0}};
// double link1[4] = {0, 0, 0, 0};
// double link2[4] = {0, -pi/2, 0, pi/2};
// double link3[4] = {l2, 0, 0, 0};
// double link4[4] = {l3, 0, 0, 0};
// double link5[4] = {l4, -pi/2, 0, 0};
double R[3][3];
double p[3];

// タイムステップの設定
const double timestep = 0.01; // 制御のタイムステップ[s]

double link_length[4] = {l1, l2, l3, l4};     // リンク長[m]
double theta[4];           // 関節角度[rad]

/////////////////////////////////関数4*4行列の内積/////////////////////////////
void CalDet(double x[][N], double y[][N])
{
  double z[4][4]= {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
  for ( int i = 0 ; i < N; i++ ) {
     for ( int j = 0 ; j < N; j++ ) {
       for ( int k = 0 ; k < N; k++ ) {
         z[i][j] += x[i][k] * y[k][j];
  }
      x[i][j] = z[i][j];
     }
       }
}


/////////////////////////////////関数CalT定義//////////////////////////////////
void CalT(double link[][N], double t[][N])
{
  for(int i=0; i<5; i++){
    double temp[4][4]= {{cos(link[i][3]), -sin(link[i][3]), 0, link[i][0]}, {cos(link[i][1])*sin(link[i][3]), cos(link[i][1])*cos(link[i][3]), -sin(link[i][1]), -sin(link[i][1])*link[i][2]},
    {sin(link[i][1])*sin(link[i][3]), sin(link[i][1])*cos(link[i][3]), cos(link[i][1]), cos(link[i][1])*link[i][2]}, {0, 0, 0, 1}};
    CalDet(t, temp);
  }
}



/////////////////////////////////main////////////////////////////////////
int main()
{
  double T[4][4] = {{1,0,0,0}, {0,1,0,0}, {0,0,1,0}, {0,0,0,1}};
  CalT(link, T);
  for(int i=0;i<=2;i++){
    p[i]=T[i][3];
    for(int j=0; j<=2; j++){
      R[i][j]=T[i][j];
    }
  }
  printf("%f,%f,%f",p[0],p[1],p[2]);

}
