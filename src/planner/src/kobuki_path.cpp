/*
    author: H. Kuribayashi

    経路生成クラス
    現在poseと目標poseから計算
*/

#include "kobuki_path.h"

KobukiPath::KobukiPath()
    : v_linear_max(0.3), v_angular_max(2), t_accel(1), t_decel(1) {
  pub_point = nh.advertise<tomato_msgs::RvPoint>("rviz_plot/point", 10);
  pub_line = nh.advertise<tomato_msgs::RvLine>("rviz_plot/line", 10);
  pub_circle = nh.advertise<tomato_msgs::RvCircle>("rviz_plot/circle", 10);
  pub_arrow = nh.advertise<tomato_msgs::RvArrow>("rviz_plot/arrow", 10);

  fp = fopen("/home/kuribayashi/Desktop/ros.csv", "w");
  fprintf(fp, "time, th, dth\n");

  is_planned = false;
}

KobukiPath::~KobukiPath() {}

void KobukiPath::clear() { is_planned = false; }

bool KobukiPath::exist() { return is_planned; }

void KobukiPath::set(geometry_msgs::Twist start, geometry_msgs::Twist goal) {
  is_planned = true;

  // 移動距離・方向の計算
  double d_x = goal.linear.x - start.linear.x; // [m]
  double d_y = goal.linear.y - start.linear.y; // [m]
  double d_xy = sqrt(d_x * d_x + d_y * d_y);
  direction = atan2(d_y, d_x); // [rad]

  // 最大速度を使うか使わないかの境界値
  double border_lin =
      (v_linear_max * t_accel) / 2 + (v_linear_max * t_decel) / 2;
  double border_rot =
      (v_angular_max * t_accel) / 2 + (v_angular_max * t_decel) / 2;

  // 1. startからgoalに向かう方向に先頭を合わせる回転
  double d_xi = direction - start.angular.z; // 移動量
  if (abs(d_xi) > M_PI)
    d_xi -= sgn(d_xi) * M_PI;

  if (abs(d_xi) <= border_rot) {
    double v_xi_max = 2 * d_xi / (t_accel + t_decel);
    pre_rot[0].set(start.angular.z, 0.0, v_xi_max, t_accel);
    pre_rot[1].set(pre_rot[0].getPos(t_accel), 0);
    pre_rot[2].set(pre_rot[1].getPos(0), v_xi_max, 0.0, t_decel);
  } else {
    double t_const = (abs(d_xi) - border_rot) / v_angular_max; // 等速で動く時間
    pre_rot[0].set(start.angular.z, 0.0, v_angular_max, t_accel);
    pre_rot[1].set(pre_rot[0].getPos(t_accel), v_angular_max, t_const);
    pre_rot[2].set(pre_rot[1].getPos(t_const), v_angular_max, 0.0, t_decel);
  }
  t_pre_rot_total = 0;
  for (int i = 0; i < 3; i++) {
    t_pre_rot[i] = pre_rot[i].time();
    t_pre_rot_total += t_pre_rot[i];
  }

  // 2. startからgoalに直進移動する
  if (d_xy <= border_lin) {
    double v_xi_max = 2 * d_xy / (t_accel + t_decel);
    linear[0].set(0.0, 0.0, v_xi_max, t_accel);
    linear[1].set(linear[0].getPos(t_accel), 0);
    linear[2].set(linear[1].getPos(0), v_xi_max, 0.0, t_decel);
  } else {
    double t_const = (d_xy - border_lin) / v_linear_max; // 等速で動く時間
    linear[0].set(0.0, 0.0, v_linear_max, t_accel);
    linear[1].set(linear[0].getPos(t_accel), v_linear_max, t_const);
    linear[2].set(linear[1].getPos(t_const), v_linear_max, 0.0, t_decel);
  }
  t_linear_total = 0;
  for (int i = 0; i < 3; i++) {
    t_linear[i] = linear[i].time();
    t_linear_total += t_linear[i];
  }

  // 3. goal poseに先頭を合わせる回転
  d_xi = goal.angular.z - direction; // 移動量
  if (abs(d_xi) > M_PI)
    d_xi -= sgn(d_xi) * M_PI;

  if (abs(d_xi) <= border_rot) {
    double v_xi_max = 2 * d_xi / (t_accel + t_decel);
    post_rot[0].set(direction, 0.0, v_xi_max, t_accel);
    post_rot[1].set(post_rot[0].getPos(t_accel), 0);
    post_rot[2].set(post_rot[1].getPos(0), v_xi_max, 0.0, t_decel);
  } else {
    double t_const = (abs(d_xi) - border_rot) / v_angular_max; // 等速で動く時間
    post_rot[0].set(direction, 0.0, v_angular_max, t_accel);
    post_rot[1].set(post_rot[0].getPos(t_accel), v_angular_max, t_const);
    post_rot[2].set(post_rot[1].getPos(t_const), v_angular_max, 0.0, t_decel);
  }
  t_post_rot_total = 0;
  for (int i = 0; i < 3; i++) {
    t_post_rot[i] = post_rot[i].time();
    t_post_rot_total += t_post_rot[i];
  }

  // 軌跡を描画
  double t = 0;
  tomato_msgs::RvLine rv_line;
  rv_line.name = "kobuki_path";
  rv_line.action = "add";
  rv_line.size = 0.02;
  rv_line.color.r = 0;
  rv_line.color.g = 1;
  rv_line.color.b = 0;
  rv_line.color.a = 1;

  while (t < getPathTime()) {
    geometry_msgs::Point line;
    line.x = get(t).position.linear.x;
    line.y = get(t).position.linear.y;
    line.z = get(t).position.linear.z;
    rv_line.position.push_back(line);
    t += 0.01;
  }
  pub_line.publish(rv_line);
}

double KobukiPath::getPathTime() {
  double t = t_pre_rot_total + t_linear_total + t_post_rot_total;
  return t;
}

tomato_msgs::KobukiState KobukiPath::get(double t) {
  geometry_msgs::Twist p_ref;
  geometry_msgs::Twist v_ref;
  p_ref.linear.x = 0.0;
  p_ref.linear.y = 0.0;
  p_ref.linear.z = 0.0;
  p_ref.angular.x = 0.0;
  p_ref.angular.y = 0.0;
  p_ref.angular.z = 0.0;
  v_ref.linear.x = 0.0;
  v_ref.linear.y = 0.0;
  v_ref.linear.z = 0.0;
  v_ref.angular.x = 0.0;
  v_ref.angular.y = 0.0;
  v_ref.angular.z = 0.0;

  if (t <= t_pre_rot_total) {
    if (t <= t_pre_rot[0]) {
      p_ref.angular.z = pre_rot[0].getPos(t);
      v_ref.angular.z = pre_rot[0].getVel(t);
    } else if (t <= t_pre_rot[0] + t_pre_rot[1]) {
      p_ref.angular.z = pre_rot[1].getPos(t - t_pre_rot[0]);
      v_ref.angular.z = pre_rot[1].getVel(t - t_pre_rot[0]);
    } else {
      p_ref.angular.z = pre_rot[2].getPos(t - t_pre_rot[0] - t_pre_rot[1]);
      v_ref.angular.z = pre_rot[2].getVel(t - t_pre_rot[0] - t_pre_rot[1]);
    }
  } else if (t <= t_pre_rot_total + t_linear_total) {
    double t_ = t - t_pre_rot_total;
    p_ref.angular.z = direction;
    if (t_ <= t_linear[0]) {
      p_ref.linear.x = linear[0].getPos(t_) * cos(direction);
      p_ref.linear.y = linear[0].getPos(t_) * sin(direction);
      v_ref.linear.x = linear[0].getVel(t_) * cos(direction);
      v_ref.linear.y = linear[0].getVel(t_) * sin(direction);
    } else if (t_ <= t_linear[0] + t_linear[1]) {
      p_ref.linear.x = linear[1].getPos(t_ - t_linear[0]) * cos(direction);
      p_ref.linear.y = linear[1].getPos(t_ - t_linear[0]) * sin(direction);
      v_ref.linear.x = linear[1].getVel(t_ - t_linear[0]) * cos(direction);
      v_ref.linear.y = linear[1].getVel(t_ - t_linear[0]) * sin(direction);
    } else {
      p_ref.linear.x =
          linear[2].getPos(t_ - t_linear[0] - t_linear[1]) * cos(direction);
      p_ref.linear.y =
          linear[2].getPos(t_ - t_linear[0] - t_linear[1]) * sin(direction);
      v_ref.linear.x =
          linear[2].getVel(t_ - t_linear[0] - t_linear[1]) * cos(direction);
      v_ref.linear.y =
          linear[2].getVel(t_ - t_linear[0] - t_linear[1]) * sin(direction);
    }
  } else if (t <= t_pre_rot_total + t_linear_total + t_post_rot_total) {
    double t_ = t - t_pre_rot_total - t_linear_total;
    p_ref.linear.x = linear[2].getPos(t_linear[2]) * cos(direction);
    p_ref.linear.y = linear[2].getPos(t_linear[2]) * sin(direction);
    fprintf(fp, "%lf,", t_);

    if (t_ <= t_post_rot[0]) {
      p_ref.angular.z = post_rot[0].getPos(t_);
      v_ref.angular.z = post_rot[0].getVel(t_);
    } else if (t_ <= t_post_rot[0] + t_post_rot[1]) {
      p_ref.angular.z = post_rot[1].getPos(t_ - t_post_rot[0]);
      v_ref.angular.z = post_rot[1].getVel(t_ - t_post_rot[0]);
    } else {
      p_ref.angular.z = post_rot[2].getPos(t_ - t_post_rot[0] - t_post_rot[1]);
      v_ref.angular.z = post_rot[2].getVel(t_ - t_post_rot[0] - t_post_rot[1]);
    }
    fprintf(fp, "%lf,%lf\n", p_ref.angular.z, v_ref.angular.z);
  } else {
    p_ref.linear.x = linear[2].getPos(t_linear[2]) * cos(direction);
    p_ref.linear.y = linear[2].getPos(t_linear[2]) * sin(direction);
    p_ref.angular.z = post_rot[2].getPos(t_post_rot[2]);
  }

  if (p_ref.angular.z < 0)
    p_ref.angular.z += 2 * M_PI;

  // 進行方向を描画
  // tomato_msgs::RvArrow arrow_linear, arrow_angular;
  //
  // arrow_linear.name = "arrow_linear";
  // arrow_linear.size = 0.01;
  // arrow_linear.color.r = 1;
  // arrow_linear.color.g = 0;
  // arrow_linear.color.b = 0;
  // arrow_linear.color.a = 1;
  //
  // geometry_msgs::Point start;
  // start.x = p_ref.linear.x + 0.1 * cos(p_ref.angular.z);
  // start.y = p_ref.linear.y + 0.1 * sin(p_ref.angular.z);
  // start.z = 0;
  // geometry_msgs::Point end;
  // end.x = start.x + v_ref.linear.x;
  // end.y = start.y + v_ref.linear.y;
  // end.z = 0;
  // arrow_linear.start = start;
  // arrow_linear.end = end;
  //
  // if (sqrt((end.x - start.x) * (end.x - start.x) +
  //          (end.y - start.y) * (end.y - start.y)) <= 0.001)
  //   arrow_linear.action = "delete";
  // else
  //   arrow_linear.action = "add";
  // pub_arrow.publish(arrow_linear);
  //
  // arrow_angular.name = "arrow_angular";
  //
  // arrow_angular.size = 0.01;
  // arrow_angular.color.r = 0;
  // arrow_angular.color.g = 1;
  // arrow_angular.color.b = 1;
  // arrow_angular.color.a = 1;
  //
  // end.x = start.x +
  //         0.5 * fabs(v_ref.angular.z) *
  //             cos(p_ref.angular.z + sgn(v_ref.angular.z) * M_PI_2);
  // end.y = start.y +
  //         0.5 * fabs(v_ref.angular.z) *
  //             sin(p_ref.angular.z + sgn(v_ref.angular.z) * M_PI_2);
  // end.z = 0;
  // arrow_angular.start = start;
  // arrow_angular.end = end;
  // if (sqrt((end.x - start.x) * (end.x - start.x) +
  //          (end.y - start.y) * (end.y - start.y)) <= 0.001)
  //   arrow_angular.action = "delete";
  // else
  //   arrow_angular.action = "add";
  // pub_arrow.publish(arrow_angular);

  tomato_msgs::KobukiState ref;
  ref.position = p_ref;
  ref.velocity = v_ref;

  return ref;
}

double KobukiPath::sgn(double val) {
  double sign = 0;
  if (val > 0)
    sign = +1;
  if (val < 0)
    sign = -1;
  return sign;
}
