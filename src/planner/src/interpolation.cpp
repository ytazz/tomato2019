#include "interpolation.h"

Interpolation::Interpolation() {
  c[0] = 0.0;
  c[1] = 0.0;
  c[2] = 0.0;
  c[3] = 0.0;
}

Interpolation::~Interpolation() {}

void Interpolation::set(double init_pos, double term_time) {
  c[0] = init_pos;
  c[1] = 0.0;
  c[2] = 0.0;
  c[3] = 0.0;
  t_plan = term_time;
}

void Interpolation::set(double init_pos, double init_vel, double term_time) {
  c[0] = init_pos;
  c[1] = init_vel;
  c[2] = 0.0;
  c[3] = 0.0;
  t_plan = term_time;
}

void Interpolation::set(double init_pos, double init_vel, double term_vel,
                        double term_time) {
  c[0] = init_pos;
  c[1] = init_vel;
  c[2] = (term_vel - init_vel) / (2 * term_time);
  c[3] = 0.0;
  t_plan = term_time;
}

void Interpolation::set(double init_pos, double init_vel, double term_pos,
                        double term_vel, double term_time) {
  c[0] = init_pos;
  c[1] = init_vel;
  c[2] = (1 / (term_time * term_time)) *
         (3 * (term_pos - init_pos) - (2 * init_vel + term_vel) * term_time);
  c[3] = (1 / (term_time * term_time * term_time)) *
         (-2 * (term_pos - init_pos) + (init_vel + term_vel) * term_time);
  t_plan = term_time;
}

double Interpolation::getPos(double t) {
  double pos;
  pos = c[0] + c[1] * t + c[2] * t * t + c[3] * t * t * t;
  return pos;
}

double Interpolation::getVel(double t) {
  double vel;
  vel = c[1] + 2 * c[2] * t + 3 * c[3] * t * t;
  return vel;
}

double Interpolation::time() { return t_plan; }

void Interpolation::print() {
  std::cout << "c0 = " << c[0] << '\n';
  std::cout << "c1 = " << c[1] << '\n';
  std::cout << "c2 = " << c[2] << '\n';
  std::cout << "c3 = " << c[3] << '\n';
}