/*
    author: H. Kuribayashi

    Kobuki用軌道計画器
*/

#include "kobuki_planner.h"

KobukiPlanner::KobukiPlanner() {
  pub_ref = nh.advertise<tomato_msgs::KobukiState>("/kobuki/pose/reference", 1);

  cli_kobuki = nh.serviceClient<tomato_msgs::KobukiValue>("/kobuki/state");
  cli_planter = nh.serviceClient<tomato_msgs::PlanterValue>("/planter/state");
  cli_time = nh.serviceClient<tomato_msgs::Time>("/timer/current");

  prev_phase = tomato_msgs::Phase::WAIT;
}

KobukiPlanner::~KobukiPlanner() {}

bool KobukiPlanner::srvKobukiPhase(tomato_msgs::KobukiPhase::Request &req,
                                   tomato_msgs::KobukiPhase::Response &res) {
  double vel = 0.0;

  switch (req.phase) {
  case tomato_msgs::Phase::WAIT:
    break;
  case tomato_msgs::Phase::MAPPING:
    break;
  case tomato_msgs::Phase::READY:
    if (prev_phase != tomato_msgs::Phase::READY) {
      // set current time
      cli_time.call(srv_time);
      start_time = (double)srv_time.response.current;
      std::cout << "start_time: " << start_time << '\n';

      // get current pose
      cli_kobuki.call(srv_kobuki); // kobukiの現在位置を取得
      current = srv_kobuki.response.current.position;

      // calc target pose
      cli_planter.call(srv_planter); // 鉢の現在位置を取得
      target = calcCircleRef(current, srv_planter.response.current.position);
      path.set(current, target);

      // 目標値を取得
      t = 0.0;
      ref = path.get(t);
      res.is_finished = false;
    } else if (t <= path.getPathTime()) {
      // get current time
      cli_time.call(srv_time);
      t = (double)srv_time.response.current - start_time;

      // 目標値を取得
      ref = path.get(t);
      std::cout << "current time: " << t << " :\t (total:" << path.getPathTime()
                << '\n';
      res.is_finished = false;
    } else {
      path.clear();
      std::cout << "終わった" << '\n';
      res.is_finished = true;
    }
    pub_ref.publish(ref);
    break;
  case tomato_msgs::Phase::CIRCLE:
    switch (req.direction) {
    case tomato_msgs::Phase::STOP:
      break;
    case tomato_msgs::Phase::CW:
      break;
    case tomato_msgs::Phase::CCW:
      break;
    }
    ref.position.linear.x = 0.0;
    ref.position.linear.y = 0.0;
    ref.position.linear.z = 0.0;
    ref.position.angular.x = 0.0;
    ref.position.angular.y = 0.0;
    ref.position.angular.z = 0.0;

    ref.velocity.linear.x = 0.0;
    ref.velocity.linear.y = 0.0;
    ref.velocity.linear.z = 0.0;
    ref.velocity.angular.x = 0.0;
    ref.velocity.angular.y = 0.0;
    ref.velocity.angular.z = 0.0;
    break;
  case tomato_msgs::Phase::APPROACH:
    break;
  case tomato_msgs::Phase::HARVEST:
    break;
  case tomato_msgs::Phase::REMOTE:
    ref.position.linear.x = 0.0;
    ref.position.linear.y = 0.0;
    ref.position.linear.z = 0.0;
    ref.position.angular.x = 0.0;
    ref.position.angular.y = 0.0;
    ref.position.angular.z = 0.0;

    ref.velocity = req.velocity;
    pub_ref.publish(ref);
    break;
  default:
    break;
  }

  prev_phase = req.phase;
  return true;
}

geometry_msgs::Twist
KobukiPlanner::calcCircleRef(geometry_msgs::Twist kobuki_pos,
                             geometry_msgs::Vector3 planter_pos) {
  geometry_msgs::Twist planter_p_normal;
  geometry_msgs::Twist planter_p_kobuki;
  double norm = 0;

  planter_p_kobuki.linear.x = kobuki_pos.linear.x - planter_pos.x;
  planter_p_kobuki.linear.y = kobuki_pos.linear.y - planter_pos.y;
  planter_p_kobuki.linear.z = kobuki_pos.linear.z - planter_pos.z;
  norm = sqrt(planter_p_kobuki.linear.x * planter_p_kobuki.linear.x +
              planter_p_kobuki.linear.y * planter_p_kobuki.linear.y +
              planter_p_kobuki.linear.z * planter_p_kobuki.linear.z);
  planter_p_normal.linear.x = planter_p_kobuki.linear.x / norm;
  planter_p_normal.linear.y = planter_p_kobuki.linear.y / norm;
  planter_p_normal.linear.z = planter_p_kobuki.linear.z / norm;

  double theta;
  theta = atan2(planter_p_normal.linear.y, planter_p_normal.linear.x) + M_PI_2;
  if (theta < 0)
    theta += 2 * M_PI;

  cli_planter.call(srv_planter); // planterの現在位置を取得
  double radius = srv_planter.response.current.circle_radius; // 周回半径を取得

  geometry_msgs::Twist ref;
  ref.linear.x = planter_pos.x + planter_p_normal.linear.x * radius;
  ref.linear.y = planter_pos.y + planter_p_normal.linear.y * radius;
  ref.linear.z = planter_pos.z + planter_p_normal.linear.z * radius;
  ref.angular.x = 0.0;
  ref.angular.y = 0.0;
  ref.angular.z = theta;
  std::cout << theta << '\n';

  return ref;
}

void KobukiPlanner::pubPath() {}

// main関数
// 定義したクラスのオブジェクト（ノード）を生成
int main(int argc, char **argv) {
  ros::init(argc, argv, "kobuki_planner");
  ros::NodeHandle nh;

  KobukiPlanner kobuki_planner;
  ros::ServiceServer srv_phase = nh.advertiseService(
      "/kobuki/phase", &KobukiPlanner::srvKobukiPhase, &kobuki_planner);

  ros::spin();
}