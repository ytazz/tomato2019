/*
author: S.Hashimoto
*/
#include "manip_planner.h"

double current_pos[6] = {0,0,0,0,0,0};
double target_pos[6] = {0.1,0.1,0.1,0,0,0};
double theta = atan(1/sqrt(2));
double phai = pi/4;

// sub = nh.subscribe<tomato_msgs::KobukiPhase>(
//     "kobuki/phase", 1, &KobukiPlanner::setPhaseCallback, this);
// pub_ref = nh.advertise<tomato_msgs::ManipState>("/manip/pose/reference", 10);
// pub_cur = nh.advertise<tomato_msgs::ManipState>("/manip/pose/current", 10);
// cli_manip = nh.serviceClient<tomato_msgs::ManipValue>("/manip/state");

//直線の軌道求める関数---------------------------------------------
//double c[]で参照渡しになっている
double linear_trajectory(double c[], double t[])
{
  double dist,Dist;
  dist = sqrt((t[0]-c[0])*(t[0]-c[0]) + (t[1]-c[1])*(t[1]-c[1]) + (t[2]-c[2])*(t[2]-c[2]));
  Dist = dist/10;
  return(Dist);
}
//----------------------------------------------------------

int main(int argc, char **argv){
  double next_target_pos[6] = {0};
  double distance;
  distance = linear_trajectory(current_pos,target_pos);

  for(int i=1;i<10;i++){
    next_target_pos[0] = current_pos[0] + distance*cos(theta)*cos(phai);
    next_target_pos[1] = current_pos[1] + distance*cos(theta)*sin(phai);
    next_target_pos[2] = current_pos[2] + distance*sin(theta);
    // printf("%f,%f,%f,%f,%f,%f\n",next_target_pos[0],next_target_pos[1],next_target_pos[2],next_target_pos[3],next_target_pos[4],next_target_pos[5]);
    for(int k=0;k<7;k++){
      current_pos[k] = next_target_pos[k];
    }
    //
    // geometry_msgs::Vector3 ref;
    // ref.target_position.x = next_target_pos[0];
    // ref.target_position.y = next_target_pos[1];
    // ref.target_position.z = next_target_pos[2];
    // ref.target_orientation.x = next_target_pos[3];
    // ref.target_orientation.y = next_target_pos[4];
    // ref.target_orientation.z = next_target_pos[5];
    //
    // geometry_msgs::Vector3 cur;
    // cur.current_position.x = current_pos[0];
    // cur.current_position.y = current_pos[1];
    // cur.current_position.z = current_pos[2];
    // cur.current_orientation.x = current_pos[3];
    // cur.current_orientation.y = current_pos[4];
    // cur.current_orientation.z = current_pos[5];
    //
    // pub_ref.publish(ref);
    // pub_cur.publish(cur);
    // printf("%f,%f,%f,%f,%f,%f\n",current_pos[0],current_pos[1],current_pos[2],current_pos[3],current_pos[4],current_pos[5]);
  }
}











//roscallback関数-------------------------------------------
// ros::Publisher chatter_pub;
// void timer_callback(const ros::TimerEvent&)
// {
//  //パッケージ名::型名
//  std_msgs::String msg;
//  msg.data = "hello world";
//  ROS_INFO("%s",msg.data.c_str());
//  //publishする
//  chatter_pub.publish(msg);
// }
//--------------------------------------------------------------
//
//
// //rosの初期化、basic_timer_talkerという名前のノードを宣言
// ros::init(argc,argv,"basic_timer_talker");
// //ノードの初期化
// ros::NodeHandle n;
// chatter_pub = n.advertise<std_msgs::String>("chatter" , 10);
// //タイマーを作成、ひとつ目の引数は実行間隔、ふたつ目はtimerが起動した時に起動するコールバック関数
// //つまり0.1秒ごとに無限にコールバック関数が呼ばれる。
// ros::Timer timer = n.createTimer(ros::Duration(0.1), timer_callback);
// ros::spin();
// return 0;


// class MotionPlanner
// {
// protected:
// ros::NodeHandle nh_;
//
// public:
// int spin()
// {
//   ros::spin();
//   return 0;
// }
//
// };
//
// int main(int argc, char**argv)
// {
//   ros::init(argc, argv, "motion_planner");
//
//   MotionPlanner node;
//   return node.spin();
// }
//
